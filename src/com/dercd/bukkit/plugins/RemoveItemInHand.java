package com.dercd.bukkit.plugins;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerNotFoundException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.events.CommandEvent;

public class RemoveItemInHand extends CDPlugin
{
	Log clog;

	public RemoveItemInHand(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}

	@Override
	public Permission[] getPermissions()
	{
		return new Permission[] { new Permission("cd.riih.others", PermissionDefault.OP), new Permission("cd.riih", PermissionDefault.OP) };
	}
	
	@Override
	public String getVersion()
	{
		return "0.1";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@CDPluginCommand(commands = { "riih cd.riih 1" })
	public void onCommand(CommandEvent e) throws CDCException
	{
		removeItem(e.getSender(), e.getArgs());
	}
	
	private void removeItem(CommandSender sender, String[] args) throws CDCException
	{
		Player p;
		if (args.length == 1)
			if (!sender.hasPermission("cd.riih.others"))
				throw new CDNoPermissionException(true);
			else if ((p = Bukkit.getPlayer(args[0])) == null)
				throw new CDPlayerNotFoundException(args[0]);
			else
				removeItem(p, true);
		else if (args.length == 0)
		{
			if (!sender.hasPermission("cd.riih"))
				throw new CDNoPermissionException(true);
			removeItem((Player) sender, true);
		}
		else throw new CDInvalidArgsException("riih");
	}

	public void removeItem(Player p, boolean mainHand)
	{
		ItemStack i = mainHand ? p.getInventory().getItemInMainHand() : p.getInventory().getItemInOffHand();
		if (i == null || i.getType() == Material.AIR)
			return;
		this.clog.log("Removing Item named \"" + i.getItemMeta().getDisplayName() + "\" from the " + (mainHand ? "main" : "off") + " hand from " + p.getName(), this);
		if (i.getAmount() == 0)
			if(mainHand)
				p.getInventory().setItemInMainHand(null);
			else
				p.getInventory().setItemInOffHand(null);
		else
		{
			i.setAmount(i.getAmount() - 1);
			if(mainHand)
				p.getInventory().setItemInMainHand(i);
			else
				p.getInventory().setItemInOffHand(i);
		}
	}
	
	public void removeItem(Player p, PlayerInteractEvent e)
	{
		ItemStack i = e.getItem();
		if(i == null)
			return;
		PlayerInventory pi = p.getInventory();
		if(i.equals(pi.getItemInMainHand()))
			removeItem(p, true);
		else if (i.equals(pi.getItemInOffHand()))
			removeItem(p, false);
		else
			this.clog.log("Neither item in main hand nor item in off hand matches item used in PlayerInteractionEvent", this);
	}
}
