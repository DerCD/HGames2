package com.dercd.bukkit.plugins.hgames;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.events.CDPluginLoadEvent;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.onarandombox.MultiverseCore.MultiverseCore;

@CDPluginDepend(depends = { MultiverseCore.class }, softdepends = { })
public class HGames2 extends CDPlugin
{
	public Game game;
	protected PluginDependencies pluginDependencies;
	protected Log clog;
	
	public HGames2(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}

	public void init()
	{
		try
		{
			PluginDependencies pluginDependencies = new PluginDependencies();
			MultiverseCore multiverse = this.getDepend(MultiverseCore.class);
			pluginDependencies.put(MultiverseCore.class, multiverse);
			
			this.pluginDependencies = pluginDependencies;
		}
		catch(CDPluginNotFoundException x)
		{
			//Won't happen
			//(Haha, yeah... sure...)
		}
	}
	
	public String[] getDirectorys()
	{
		return new String[]
		{
			"/hgames2"
		};
	}
	
	@CDPluginEvent
	public void onLoad(CDPluginLoadEvent e)
	{
		this.clog.log("Initializing HGames2");
		init();
		this.clog.log("Creating Game-instance", this);
		Game game = Game.getInstance();
		game.hgames2 = this;
		game.init();
		this.game = game;
	}
	@CDPluginEvent
	public void onEnable(CDPluginEnableEvent e)
	{
		this.clog.log("Loading Game");
		Game game = this.game;
		game.load();
	}

	@CDPluginEvent
	public void onPlayerDamage(EntityDamageByEntityEvent e)
	{
		this.game.onPlayerDamage(e);
		onEntityDamage(e);
	}
	@CDPluginEvent
	public void onPlayerDamage(EntityDamageByBlockEvent e)
	{
		this.game.onPlayerDamage(e);
		onEntityDamage(e);
	}
	@CDPluginEvent
	public void onLogin(PlayerLoginEvent e)
	{
		this.game.onLogin(e);
	}
	@CDPluginEvent
	public void onJoin(PlayerJoinEvent e)
	{
		this.game.onJoin(e);
	}
	@CDPluginEvent
	public void onLeave(PlayerQuitEvent e)
	{
		this.game.onLeave(e);
	}
	@CDPluginEvent
	public void onInteract(PlayerInteractEvent e)
	{
		this.game.onInteract(e);
	}
	@CDPluginEvent
	public void onBlockBreak(BlockBreakEvent e)
	{
		this.game.onBlockBreak(e);
	}
	@CDPluginEvent
	public void onFade(BlockFadeEvent e)
	{
		this.game.onFade(e);
	}
	@CDPluginCommand(commands = {"dm cd.hg2.dm", "v cd.hg2.vote", "vote cd.hg2.vote"})
	public void onCommand(CommandEvent e) throws CDCException
	{
		this.game.onCommand(e);
	}
	@CDPluginEvent
	public void onMove(PlayerMoveEvent e)
	{
		this.game.onMove(e);
	}
	@CDPluginEvent
	public void onDeath(PlayerDeathEvent e)
	{
		this.game.onDeath(e);
	}
	@CDPluginEvent(lateRegister = true, priority = 1000)
	public void onRespawn(PlayerRespawnEvent e)
	{
		this.game.onRespawn(e);
	}
	public void onEntityDamage(EntityDamageEvent e)
	{
		this.game.onEntityDamage(e);
	}
	@CDPluginEvent
	public void onChat(AsyncPlayerChatEvent e)
	{
		this.game.onChat(e);
	}
	
	public PluginDependencies getPluginDependencies()
	{
		return this.pluginDependencies;
	}

	public void setPluginDependencies(PluginDependencies pluginDependencies)
	{
		this.pluginDependencies = pluginDependencies;
	}
}
