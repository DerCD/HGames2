package com.dercd.bukkit.plugins.hgames;

import java.io.IOException;

import com.dercd.bukkit.plugins.hgames.map.MapPool;
import com.dercd.bukkit.plugins.hgames.map.SpawnPool;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestPool;
import com.dercd.bukkit.plugins.hgames.map.dm.DMap;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapInfo;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapTimes;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapInfo;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapTimes;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public interface IIOManager
{
	//Settings
	ISettings loadSettings();
	void saveSettings(ISettings settings);
	
	//Pools
	MapPool loadHGMapPool();
	MapPool loadDMapPool();
	TextPool loadTextPool();
	
	//Map
	void deleteTmpMapFolders() throws IOException;
	void deletePlayerData() throws IOException;
	//Load HG
	HGMap loadHGMap(HGMapInfo mapInfo, boolean includeFolderCopy);
	void copyHGMapFolder(HGMapInfo mapInfo);
	void loadHGWorld();
	SpawnPool loadHGSpawnPool();
	ItemPool loadHGItemPool();
	ItemPool loadHGSpecialItemPool();
	HGMapTimes loadHGMapTimes();
	ChestPool loadHGChestPool();
	void unloadHGWorld();
	//Load DM
	DMap loadDMap(DMapInfo mapInfo, boolean includeFolderCopy);
	void copyDMapFolder(DMapInfo mapInfo);
	void loadDMWorld();
	SpawnPool loadDMSpawnPool();
	ItemPool loadDMItemPool();
	DMapTimes loadDMapTimes();
	void unloadDMWorld();
	//Save HG
	void saveHGMap(HGMap map) throws IOException;
	void copyHGMapFolderBack(HGMapInfo mapInfo) throws IOException;
	void saveHGMapInfo(HGMapInfo mapInfo) throws IOException;
	void saveHGSpawnPool(SpawnPool spawnPool) throws IOException;
	void saveHGItemPool(ItemPool itemPool) throws IOException;
	void saveHGSpecialItemPool(ItemPool itemPool) throws IOException;
	void saveHGMapTimes(HGMapTimes mapTimes) throws IOException;
	void saveHGChestPool(ChestPool chestPool) throws IOException;
	//Save DM
	void saveDMap(DMap map) throws IOException;
	void copyDMapFolderBack(DMapInfo mapInfo) throws IOException;
	void saveDMapInfo(DMapInfo mapInfo) throws IOException;
	void saveDMSpawnPool(SpawnPool spawnPool) throws IOException;
	void saveDMItemPool(ItemPool itemPool) throws IOException;
	void saveDMapTimes(DMapTimes mapTimes) throws IOException;
}
