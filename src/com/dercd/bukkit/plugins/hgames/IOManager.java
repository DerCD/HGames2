package com.dercd.bukkit.plugins.hgames;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.commons.io.IOUtils;
import org.apache.commons.multiverse.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.BlockPosition;
import com.dercd.bukkit.plugins.hgames.TextPool.TextTemplate;
import com.dercd.bukkit.plugins.hgames.map.HGMapRefillTimes;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;
import com.dercd.bukkit.plugins.hgames.map.MapPool;
import com.dercd.bukkit.plugins.hgames.map.SpawnPool;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestCollection;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestManager;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestPool;
import com.dercd.bukkit.plugins.hgames.map.dm.DMap;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapInfo;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapTimes;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapInfo;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapTimes;
import com.dercd.bukkit.plugins.hgames.map.items.ItemOccurrence;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.onarandombox.MultiverseCore.MultiverseCore;
import com.onarandombox.MultiverseCore.api.MVWorldManager;

import net.minecraft.server.v1_11_R1.NBTTagCompound;
import net.minecraft.server.v1_11_R1.NBTTagList;

public class IOManager implements IIOManager
{
	public static final String SETTINGS = "hgames2/settings.json";
	public static final String TEXTPOOL = "hgames2/lang.json";
	
	public static final String MAPS = "hgames2/maps";
	public static final String MAPINFO = "mapInfo/mapinfo.json";
	
	public static final String LOBBY_DESTINATION = "lobby";
	public static final String LOBBY_PLAYERDATA = "playerdata";
	
	public static final String HGMAP_DESTINATION = "hg";
	public static final String HG_MAPDATA = "mapInfo/mapdata.json";
	public static final String HG_SPAWNPOOL = "mapInfo/spawnpool.json";
	public static final String HG_ITEMPOOL = "mapInfo/itempool.dat";
	public static final String HG_SPECIAL_ITEMPOOL = "mapInfo/specialItempool.dat";
	public static final String HG_MAPTIMES = "mapInfo/maptimes.json";
	public static final String HG_CHESTPOOL = "mapInfo/chests.json";
	
	public static final String DMAP_DESTINATION = "dm";
	public static final String DM_MAPDATA = "mapInfo/mapdata.json";
	public static final String DM_SPAWNPOOL = "mapInfo/spawnpool.json";
	public static final String DM_ITEMPOOL = "mapInfo/itempool.dat";
	public static final String DM_MAPTIMES = "mapInfo/maptimes.json";
	
	protected Log clog;
	protected ArrayList<MapInfo> mapInfos;
	
	public IOManager()
	{
		this.clog = CDAPI.getInstance().getHandler().getCLog();
	}
	
	@Override
	public ISettings loadSettings()
	{
		JsonObject json = null;
		try
		{
			File f = new File(SETTINGS);
			if(!f.exists())
				extractResource("/defaultSettings.json", SETTINGS);
		
			json = (JsonObject)Tools.readJson(SETTINGS);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		
		GameSettings settings = new GameSettings();
		settings.setServerName(json.get("serverName").getAsString());
		Location lobbySpawn = Tools.readLocationFromJSON(json.getAsJsonObject("lobbySpawn"));
		settings.setLobbySpawn(lobbySpawn);
		settings.setTeamsAllowed(json.get("teamsAllowed").getAsBoolean());
		settings.setMaxMapsToVote(json.get("maxMapsToVote").getAsInt());
		settings.setVotingTime(json.get("votingTime").getAsInt());
		settings.setWaitingTime(json.get("waitingTime").getAsInt());
		settings.setEndTime(json.get("endTime").getAsInt());
		return settings;
	}
	public void saveSettings(ISettings settings)
	{
		JsonObject json = new JsonObject();
		json.addProperty("serverName", settings.getServerName());
		JsonObject lobbySpawn = Tools.writeLocationToJSON(settings.getLobbySpawn());
		json.add("lobbySpawn", lobbySpawn);
		json.addProperty("teamsAllowed", settings.getTeamsAllowed());
		json.addProperty("maxMapsToVote", settings.getMaxMapsToVote());
		json.addProperty("votingTime", settings.getVotingTime());
		json.addProperty("waitingTime", settings.getWaitingTime());
		json.addProperty("endTime", settings.getEndTime());
		try
		{
			createFolderStructureToFile(SETTINGS);
			Tools.writeJson(SETTINGS, json);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
		}
	}
	
	protected void loadMapInfos()
	{
		this.clog.log("Loading MapInfos", this);
		File mapFolder = new File(MAPS);
		if(!mapFolder.exists())
			mapFolder.mkdirs();
		
		this.mapInfos = new ArrayList<MapInfo>();
		String[] childs = mapFolder.list();
		for(String child : childs)
		{
			this.clog.log("Processing '" + child + "'", this);
			File f = Paths.get(mapFolder.getAbsolutePath(), child).toFile();
			if(!f.isDirectory())
				continue;
			this.clog.log("Searching directory '" + f.getAbsolutePath() + "'", this);
			File fileMapInfo = Paths.get(f.getAbsolutePath(), MAPINFO).toFile();
			if(!fileMapInfo.exists())
				continue;
			this.clog.log("MapInfo found", this);
			MapInfo mapInfo = loadMapInfo(fileMapInfo.getPath());
			this.clog.log("MapInfo loaded", this);
			this.mapInfos.add(mapInfo);
		}
	}
	@Override
	public MapPool loadHGMapPool()
	{
		if(this.mapInfos == null)
			loadMapInfos();
		MapPool mapPool = new MapPool();
		for(MapInfo mapInfo : this.mapInfos)
			if(mapInfo instanceof HGMapInfo)
				mapPool.addMapInfo(mapInfo);
		return mapPool;
	}
	@Override
	public MapPool loadDMapPool()
	{
		if(this.mapInfos == null)
			loadMapInfos();
		MapPool mapPool = new MapPool();
		for(MapInfo mapInfo : this.mapInfos)
			if(mapInfo instanceof DMapInfo)
				mapPool.addMapInfo(mapInfo);
		return mapPool;
	}
	@Override
	public TextPool loadTextPool()
	{
		try
		{
			File f = new File(TEXTPOOL);
			if(!f.exists())
				extractResource("/lang.json", TEXTPOOL);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		return loadTextPool(TEXTPOOL);
	}
	
	@Override
	public void deleteTmpMapFolders() throws IOException
	{
		FileUtils.deleteDirectory(new File(HGMAP_DESTINATION));
		FileUtils.deleteDirectory(new File(DMAP_DESTINATION));
	}
	public void deletePlayerData() throws IOException
	{
		FileUtils.deleteDirectory(Paths.get(LOBBY_DESTINATION, LOBBY_PLAYERDATA).toFile());
	}
	
	public MapInfo loadMapInfo(String path)
	{
		this.clog.log("Loading MapInfo from file '" + path + "'");
		JsonObject json = null;
		try
		{
			json = (JsonObject)Tools.readJson(path);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		
		MapInfo mapInfo;
		String type = json.get("type").getAsString();
		switch(type)
		{
			case "hg":
				mapInfo = loadHGMapInfo(json);
				break;
			case "dm":
				mapInfo = loadDMapInfo(json);
				break;
			default:
				return null;
		}
		
		//Getting parent-parent-folder
		//File: ./MAP/mapInfo/mapinfo.json
		//Parent: ./MAP/mapInfo
		//Parent-parent: ./MAP
		File folder = new File(path).getParentFile().getParentFile();
		mapInfo.setPath(folder.getPath());
		this.clog.log("Path of Map: '" + folder.getPath() + "'");
		mapInfo.setName(json.get("name").getAsString());
		mapInfo.setMinPlayers(json.get("minPlayers").getAsInt());
		mapInfo.setMaxPlayers(json.get("maxPlayers").getAsInt());
		mapInfo.setCreator(json.get("creator").getAsString());
		
		return mapInfo;
	}
	public HGMapInfo loadHGMapInfo(JsonObject json)
	{
		HGMapInfo mapInfo = new HGMapInfo();
		//HGMapInfo has no own attributes
		return mapInfo;
	}
	public DMapInfo loadDMapInfo(JsonObject json)
	{
		DMapInfo mapInfo = new DMapInfo();
		//DMapInfo has no own attributes
		return mapInfo;
	}
	
	@Override
	public HGMap loadHGMap(HGMapInfo mapInfo, boolean includeFolderCopy)
	{
		return loadHGMap(mapInfo, Paths.get(HGMAP_DESTINATION, HG_MAPDATA).toString(), HGMAP_DESTINATION, includeFolderCopy);
	}
	@Override
	public void copyHGMapFolder(HGMapInfo mapInfo)
	{
		copyFolderSafely(mapInfo.getPath(), HGMAP_DESTINATION);
	}
	public void loadHGWorld()
	{
		loadWorld(HGMAP_DESTINATION);
	}
	@Override
	public SpawnPool loadHGSpawnPool()
	{
		return loadSpawnPool(Paths.get(HGMAP_DESTINATION, HG_SPAWNPOOL).toString());
	}
	@Override
	public ItemPool loadHGItemPool()
	{
		return loadItemPool(Paths.get(HGMAP_DESTINATION, HG_ITEMPOOL).toString());
	}
	@Override
	public ItemPool loadHGSpecialItemPool()
	{
		return loadItemPool(Paths.get(HGMAP_DESTINATION, HG_SPECIAL_ITEMPOOL).toString());
	}
	@Override
	public HGMapTimes loadHGMapTimes()
	{
		return loadHGMapTimes(Paths.get(HGMAP_DESTINATION, HG_MAPTIMES).toString());
	}
	@Override
	public ChestPool loadHGChestPool()
	{
		return loadChestPool(Paths.get(HGMAP_DESTINATION, HG_CHESTPOOL).toString());
	}
	public void unloadHGWorld()
	{
		unloadWorld(HGMAP_DESTINATION);
	}

	@Override
	public DMap loadDMap(DMapInfo mapInfo, boolean includeFolderCopy)
	{
		return loadDMap(mapInfo, Paths.get(DMAP_DESTINATION, DM_MAPDATA).toString(), DMAP_DESTINATION, includeFolderCopy);
	}
	@Override
	public void copyDMapFolder(DMapInfo mapInfo)
	{
		copyFolderSafely(mapInfo.getPath(), DMAP_DESTINATION);
	}
	public void loadDMWorld()
	{
		loadWorld(DMAP_DESTINATION);
	}
	@Override
	public SpawnPool loadDMSpawnPool()
	{
		return loadSpawnPool(Paths.get(DMAP_DESTINATION, DM_SPAWNPOOL).toString());
	}
	@Override	
	public ItemPool loadDMItemPool()
	{
		return loadItemPool(Paths.get(DMAP_DESTINATION, DM_ITEMPOOL).toString());
	}
	public DMapTimes loadDMapTimes()
	{
		return loadDMapTimes(Paths.get(DMAP_DESTINATION, DM_MAPTIMES).toString());
	}
	public void unloadDMWorld()
	{
		unloadWorld(DMAP_DESTINATION);
	}

	@Override
	public void saveHGMap(HGMap map) throws IOException
	{
		saveHGMap(map, HG_MAPDATA);
	}
	public void copyHGMapFolderBack(HGMapInfo mapInfo) throws IOException
	{
		copyFolder(HGMAP_DESTINATION, mapInfo.getPath());
	}
	public void saveHGMapInfo(HGMapInfo mapInfo) throws IOException
	{
		saveMapInfo(mapInfo, Paths.get(HGMAP_DESTINATION, MAPINFO).toString());
	}
	public void saveHGSpawnPool(SpawnPool spawnPool) throws IOException
	{
		saveSpawnPool(spawnPool, Paths.get(HGMAP_DESTINATION, HG_SPAWNPOOL).toString());
	}
	@Override
	public void saveHGItemPool(ItemPool itemPool) throws IOException
	{
		saveItemPool(itemPool, Paths.get(HGMAP_DESTINATION, HG_ITEMPOOL).toString());
	}
	@Override
	public void saveHGSpecialItemPool(ItemPool itemPool) throws IOException
	{
		saveItemPool(itemPool, Paths.get(HGMAP_DESTINATION, HG_SPECIAL_ITEMPOOL).toString());
	}
	@Override
	public void saveHGMapTimes(HGMapTimes mapTimes) throws IOException
	{
		saveHGMapTimes(mapTimes, Paths.get(HGMAP_DESTINATION, HG_MAPTIMES).toString());
	}
	@Override
	public void saveHGChestPool(ChestPool chestPool) throws IOException
	{
		saveChestPool(chestPool, Paths.get(HGMAP_DESTINATION, HG_CHESTPOOL).toString());
	}

	@Override
	public void saveDMap(DMap map) throws IOException
	{
		saveDMap(map, DM_MAPDATA);
	}
	public void copyDMapFolderBack(DMapInfo mapInfo) throws IOException
	{
		copyFolder(DMAP_DESTINATION, mapInfo.getPath());
	}
	public void saveDMapInfo(DMapInfo mapInfo) throws IOException
	{
		saveMapInfo(mapInfo, Paths.get(DMAP_DESTINATION, MAPINFO).toString());
	}
	public void saveDMSpawnPool(SpawnPool spawnPool) throws IOException
	{
		saveSpawnPool(spawnPool, Paths.get(DMAP_DESTINATION, DM_SPAWNPOOL).toString());
	}
	@Override
	public void saveDMItemPool(ItemPool itemPool) throws IOException
	{
		saveItemPool(itemPool, Paths.get(DMAP_DESTINATION, DM_ITEMPOOL).toString());
	}
	public void saveDMapTimes(DMapTimes mapTimes) throws IOException
	{
		saveDMapTimes(mapTimes, Paths.get(DMAP_DESTINATION, DM_MAPTIMES).toString());
	}
	
	public void createFolderStructureToFile(String file)
	{
		File f = new File(file);
		f.getParentFile().mkdirs();
	}
	public void extractResource(String resource, String destination) throws IOException
	{
		createFolderStructureToFile(destination);
		InputStream s = getClass().getResourceAsStream(resource);
		FileOutputStream out = new FileOutputStream(destination, false);
		IOUtils.copy(s, out);
		s.close();
		out.flush();
		out.close();
	}
	public TextPool loadTextPool(String file)
	{
		JsonObject json;
		try
		{
			json = (JsonObject)Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		
		TextPool textPool = new TextPool();
		for(Entry<String, JsonElement> textEntry : json.entrySet())
		{
			String textKey = textEntry.getKey();
			JsonElement textValue = textEntry.getValue();
			
			TextTemplate textTemplate = textPool.new TextTemplate(textKey);
			if(textValue.isJsonPrimitive())
			{
				textTemplate.addText(textValue.getAsString());
			}
			else if(textValue.isJsonObject())
			{
				JsonObject textObj = textValue.getAsJsonObject();
				if(textObj.has("usePrefix") && textObj.get("usePrefix").getAsBoolean())
					textTemplate.setUsingPrefix(true);
				
				JsonElement innerTextValue = textObj.get("text");
				if(innerTextValue.isJsonPrimitive())
				{
					textTemplate.addText(innerTextValue.getAsString());
				}
				else if(innerTextValue.isJsonArray())
				{
					JsonArray innerTextArray = innerTextValue.getAsJsonArray();
					for(JsonElement innerTextArrayValue : innerTextArray)
						textTemplate.addText(innerTextArrayValue.getAsString());
				}
				else
				{
					continue;
				}
			}
			else
			{
				continue;
			}
			
			textPool.addText(textTemplate);
		}
		return textPool;
	}
	public HGMap loadHGMap(HGMapInfo mapInfo, String mapData, String world, boolean includeFolderCopy)
	{
		if(includeFolderCopy)
		{
			copyHGMapFolder(mapInfo);
			loadHGWorld();
		}
		HGMap map = loadBareHGMap(mapData);
		map.setMapInfo(mapInfo);
		map.setWorld(Bukkit.getWorld(world));
		map.setSpawnPool(loadHGSpawnPool());
		map.setItemPool(loadHGItemPool());
		ChestManager chestManager = new ChestManager(map);
		chestManager.setChestPool(loadHGChestPool());
		map.setChestManager(chestManager);
		map.setSpecialItemPool(loadHGSpecialItemPool());
		map.setMapTimes(loadHGMapTimes());
		
		return map;
	}
	public HGMap loadBareHGMap(String file)
	{
		JsonObject json;
		try
		{
			json = (JsonObject) Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		
		HGMap map = new HGMap();
		map.setChestsToPlace(json.get("chestsToPlace").getAsInt());
		map.setMinChestDistance(json.get("minChestDistance").getAsInt());
		map.setMaxDMCountdownReducePlayers(json.get("maxDMCountdownReducePlayers").getAsInt());
		
		return map;
	}
	public DMap loadDMap(DMapInfo mapInfo, String mapData, String world, boolean includeFolderCopy)
	{
		if(includeFolderCopy)
		{
			copyDMapFolder(mapInfo);
			loadDMWorld();
		}
		DMap map = loadBareDMap(mapData);
		map.setMapInfo(mapInfo);
		map.setWorld(Bukkit.getWorld(world));
		map.setSpawnPool(loadDMSpawnPool());
		map.setItemPool(loadDMItemPool());
		map.setChestManager(new ChestManager(map));
		map.setMapTimes(loadDMapTimes());
		
		return map;
	}
	public DMap loadBareDMap(String file)
	{
		@SuppressWarnings("unused")
		JsonObject json;
		try
		{
			json = (JsonObject) Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		
		DMap map = new DMap();
		//DMap has no mapdata to load
		
		return map;
	}
	public void copyFolderSafely(String source, String destination)
	{
		try
		{
			copyFolder(source, destination);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
		}
	}
	public void loadWorld(String world)
	{
		MVWorldManager worldManager = getMVWorldManager();
		worldManager.loadWorld(world);
	}
	public SpawnPool loadSpawnPool(String file)
	{
		JsonArray json = null;
		try
		{
			json = (JsonArray)Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		SpawnPool spawnPool = new SpawnPool();
		for(JsonElement child : json)
		{
			JsonObject jsonLoc = child.getAsJsonObject();
			Location loc = Tools.readLocationFromJSON(jsonLoc);
			spawnPool.addSpawn(loc);
		}
		return spawnPool;
	}
	public ItemPool loadItemPool(String file)
	{
		this.clog.log("Loading ItemPool from '" + file + "'", this);
		NBTTagCompound nbt;
		try
		{
			nbt = Tools.load(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		ItemPool itemPool = new ItemPool();
		NBTTagList nbtItemPool = (NBTTagList)nbt.get("itemPool");
		int len = nbtItemPool.size();
		if(len == 0)
		{
			this.clog.log("File doesn't contain any items");
			return null;
		}
		for(int i = 0; i < len; ++i)
		{
			NBTTagCompound entry = nbtItemPool.get(i);
			ItemOccurrence itemOccurrence = new ItemOccurrence();
			
			NBTTagCompound nbtItem = entry.getCompound("item");
			itemOccurrence.setItem(Tools.readItem(nbtItem));
			
			int occurrence = entry.getInt("occurrence");
			itemOccurrence.setOccurrence(occurrence);
			
			itemPool.addItemOccurrence(itemOccurrence);
		}
		this.clog.log("Loaded " + itemPool.getItemOccurrences().size() + " items", this);
		int minItemsPerChest = nbt.getInt("minItemsPerChest");
		itemPool.setMinItems(minItemsPerChest);
		int maxItemsPerChest = nbt.getInt("maxItemsPerChest");
		itemPool.setMaxItems(maxItemsPerChest);
		itemPool.generatePool();
		
		return itemPool;
	}
	public HGMapTimes loadHGMapTimes(String file)
	{
		JsonObject json;
		try
		{
			json = (JsonObject)Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		HGMapTimes mapTimes = new HGMapTimes();
		mapTimes.spawnTime = json.get("spawnTime").getAsInt();
		mapTimes.protectionTime = json.get("protectionTime").getAsInt();
		mapTimes.playTime = json.get("playTime").getAsInt();
		
		HGMapRefillTimes refillTimes = new HGMapRefillTimes();
		JsonArray jsonRefillTimes = json.get("refillTimes").getAsJsonArray();
		int[] intRefillTimes = new int[jsonRefillTimes.size()];
		for(int i = 0; i < jsonRefillTimes.size(); ++i)
			intRefillTimes[i] = jsonRefillTimes.get(i).getAsInt();
		refillTimes.setBaseData(intRefillTimes);
		mapTimes.refillTimes = refillTimes;
		
		return mapTimes;
	}
	public DMapTimes loadDMapTimes(String file)
	{
		JsonObject json;
		try
		{
			json = (JsonObject)Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		DMapTimes mapTimes = new DMapTimes();
		mapTimes.spawnTime = json.get("spawnTime").getAsInt();
		mapTimes.playTime = json.get("playTime").getAsInt();
		
		return mapTimes;
	}
	public ChestPool loadChestPool(String file)
	{
		JsonArray json = null;
		try
		{
			json = (JsonArray)Tools.readJson(file);
		}
		catch(Exception x)
		{
			this.clog.printException(x);
			return null;
		}
		ChestPool chestPool = new ChestPool();
		for(JsonElement child : json)
		{
			JsonObject jsonLoc = child.getAsJsonObject();
			Location loc = Tools.readLocationFromJSON(jsonLoc);
			BlockPosition blockPos = BlockPosition.from(loc);
			if(jsonLoc.has("static") && jsonLoc.get("static").getAsBoolean())
				chestPool.addStaticChest(blockPos);
			else
				chestPool.addChest(blockPos);
		}
		return chestPool;
	}
	public void unloadWorld(String world)
	{
		MVWorldManager worldManager = getMVWorldManager();
		worldManager.unloadWorld(world);
	}
	
	public void saveHGMap(HGMap map, String file) throws IOException
	{
		saveBareHGMap(map, file);
		saveHGMapInfo((HGMapInfo) map.getMapInfo());
		saveHGSpawnPool(map.getSpawnPool());
		saveHGItemPool(map.getItemPool());
		saveHGChestPool(map.getChestManager().getChestPool());
		saveHGSpecialItemPool(map.getSpecialItemPool());
		saveHGMapTimes(map.getMapTimes());
	}
	public void saveBareHGMap(HGMap map, String file) throws IOException
	{
		JsonObject json = new JsonObject();
		
		json.addProperty("chestsToPlace", map.getChestsToPlace());
		json.addProperty("minChestDistance", map.getMinChestDistance());
		json.addProperty("maxDMCountdownReducePlayers", map.getMaxDMCountdownReducePlayers());
		
		Tools.writeJson(file, json, true);
	}
	public void saveDMap(DMap map, String file) throws IOException
	{
		saveBareDMap(map, file);
		saveDMapInfo((DMapInfo) map.getMapInfo());
		saveDMSpawnPool(map.getSpawnPool());
		saveDMItemPool(map.getItemPool());
		saveDMapTimes(map.getMapTimes());
	}
	public void saveBareDMap(DMap map, String file) throws IOException
	{
		JsonObject json = new JsonObject();
		//DMap has no mapdata to save
		
		Tools.writeJson(file, json, true);
	}
	public void saveMapInfo(MapInfo mapInfo, String file) throws IOException
	{
		JsonObject json = null;
		if(mapInfo instanceof HGMapInfo)
		{
			json = saveHGMapInfoToJson((HGMapInfo)mapInfo);
			json.addProperty("type", "hg");
		}
		else if(mapInfo instanceof DMapInfo)
		{
			json = saveDMapInfoToJson((DMapInfo)mapInfo);
			json.addProperty("type", "dm");
		}
		
		json.addProperty("name", mapInfo.getName());
		json.addProperty("minPlayers", mapInfo.getMinPlayers());
		json.addProperty("maxPlayers", mapInfo.getMaxPlayers());
		json.addProperty("creator", mapInfo.getCreator());
		
		Tools.writeJson(file, json, true);
	}
	public JsonObject saveHGMapInfoToJson(HGMapInfo mapInfo)
	{
		JsonObject json = new JsonObject();
		//HGMapInfo has no own attributes
		
		return json;
	}
	public JsonObject saveDMapInfoToJson(DMapInfo mapInfo)
	{
		JsonObject json = new JsonObject();
		//DMapInfo has no own attributes
		
		return json;
	}
	public void copyFolder(String source, String destination) throws IOException
	{
		FileUtils.copyDirectory(new File(source), new File(destination));
	}
	public void saveSpawnPool(SpawnPool spawnPool, String file) throws IOException
	{
		JsonArray json = new JsonArray();
		for(Location loc : spawnPool.getAllSpawns())
		{
			JsonObject jsonSpawn = Tools.writeLocationToJSON(loc);
			json.add(jsonSpawn);
		}
		Tools.writeJson(file, json, true);
	}
	public void saveItemPool(ItemPool itemPool, String file) throws IOException
	{
		NBTTagCompound nbt = new NBTTagCompound();
		NBTTagList nbtItemPool = new NBTTagList();
		
		for(ItemOccurrence itemOccurrence : itemPool.getItemOccurrences())
		{
			NBTTagCompound nbtItemOccurrence = new NBTTagCompound();
			NBTTagCompound nbtItem = Tools.writeItem(itemOccurrence.getItem());
			nbtItemOccurrence.set("item", nbtItem);
			nbtItemOccurrence.setInt("occurrence", itemOccurrence.getOccurrence());
			nbtItemPool.add(nbtItemOccurrence);
		}
		nbt.set("itemPool", nbtItemPool);
		nbt.setInt("minItemsPerChest", itemPool.getMinItems());
		nbt.setInt("maxItemsPerChest", itemPool.getMaxItems());
		Tools.save(nbt, file);
	}
	public void saveHGMapTimes(HGMapTimes mapTimes, String file) throws IOException
	{
		JsonObject json = new JsonObject();
		json.addProperty("spawnTime", mapTimes.spawnTime);
		json.addProperty("protectionTime", mapTimes.protectionTime);
		json.addProperty("playTime", mapTimes.playTime);
		
		JsonArray jsonRefillTimes = new JsonArray();
		int[] intRefillTimes = mapTimes.refillTimes.getBaseData();
		for(int i : intRefillTimes)
			jsonRefillTimes.add(new JsonPrimitive(i));
		json.add("refillTimes", jsonRefillTimes);
		
		Tools.writeJson(file, json, true);
	}
	public void saveDMapTimes(DMapTimes mapTimes, String file) throws IOException
	{
		JsonObject json = new JsonObject();
		json.addProperty("spawnTime", mapTimes.spawnTime);
		json.addProperty("playTime", mapTimes.playTime);
		
		Tools.writeJson(file, json, true);
	}
	public void saveChestPool(ChestPool chestPool, String file) throws IOException
	{
		JsonArray json = new JsonArray();
		saveChestsToJson(chestPool.getChests(), json);
		saveChestsToJson(chestPool.getStaticChests(), json);
		Tools.writeJson(file, json, true);
	}
	protected void saveChestsToJson(ChestCollection chests, JsonArray json)
	{
		if(chests != null)
			for(BlockPosition chest : chests)
			{
				JsonObject jsonChest = Tools.writeBlockLocationToJSON(chest.toLocation());
				json.add(jsonChest);
			}
	}

	public MVWorldManager getMVWorldManager()
	{
		Game game = Game.getInstance();
		PluginDependencies pluginDependencies = game.hgames2.getPluginDependencies();
		MultiverseCore multiverse = pluginDependencies.get(MultiverseCore.class);
		MVWorldManager worldManager = multiverse.getMVWorldManager();
		
		return worldManager;
	}
}
