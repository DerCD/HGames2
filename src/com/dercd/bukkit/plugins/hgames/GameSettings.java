package com.dercd.bukkit.plugins.hgames;

import org.bukkit.Location;

public class GameSettings implements ISettings
{
	protected String serverName;
	
	protected Location lobbySpawn;
	
	protected boolean teamsAllowed;
	
	protected int maxMapsToVote;
	
	protected int votingTime;
	protected int waitingTime;
	protected int endTime;
	
	
	public String getServerName()
	{
		return this.serverName;
	}
	public Location getLobbySpawn()
	{
		return this.lobbySpawn;
	}
	public boolean getTeamsAllowed()
	{
		return this.teamsAllowed;
	}
	public int getMaxMapsToVote()
	{
		return this.maxMapsToVote;
	}
	public int getVotingTime()
	{
		return this.votingTime;
	}
	public int getWaitingTime()
	{
		return this.waitingTime;
	}
	public int getEndTime()
	{
		return this.endTime;
	}
	
	public void setServerName(String serverName)
	{
		this.serverName = serverName;
	}
	public void setLobbySpawn(Location lobbySpawn)
	{
		this.lobbySpawn = lobbySpawn;
	}
	public void setTeamsAllowed(boolean teamsAllowed)
	{
		this.teamsAllowed = teamsAllowed;
	}
	public void setMaxMapsToVote(int maxMapsToVote)
	{
		this.maxMapsToVote = maxMapsToVote;
	}
	public void setVotingTime(int votingTime)
	{
		this.votingTime = votingTime;
	}
	public void setWaitingTime(int waitingTime)
	{
		this.waitingTime = waitingTime;
	}
	public void setEndTime(int endTime)
	{
		this.endTime = endTime;
	}
}
