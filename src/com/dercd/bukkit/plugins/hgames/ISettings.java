package com.dercd.bukkit.plugins.hgames;

import org.bukkit.Location;

public interface ISettings
{
	String getServerName();
	
	Location getLobbySpawn();
	
	boolean getTeamsAllowed();
	
	int getMaxMapsToVote();
	
	int getVotingTime();
	int getWaitingTime();
	int getEndTime();
}
