package com.dercd.bukkit.plugins.hgames;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;

public class TitleTemplate
{
	protected String title;
	protected String subTitle;
	protected int[] displayTimes = Countdown.TitleDisplay.DisplayTimes.DEFAULT_SECOND;
	
	public TitleTemplate()
	{ }
	public TitleTemplate(String title, String subTitle, int[] displayTimes)
	{
		this.title = title;
		this.subTitle = subTitle;
		this.displayTimes = displayTimes;
	}

	public void display()
	{
		Tools.sendTitle(this.title, this.subTitle, this.displayTimes);
	}
	
	public String getTitle()
	{
		return this.title;
	}
	public String getSubTitle()
	{
		return this.subTitle;
	}
	public int[] getDisplayTimes()
	{
		return this.displayTimes;
	}
	
	public void setTitle(String title)
	{
		this.title = title;
	}
	public void setSubTitle(String subTitle)
	{
		this.subTitle = subTitle;
	}
	public void setDisplayTimes(int[] displayTimes)
	{
		this.displayTimes = displayTimes;
	}
}
