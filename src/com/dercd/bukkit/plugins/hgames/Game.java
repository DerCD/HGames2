package com.dercd.bukkit.plugins.hgames;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Zombie;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.scoreboard.DisplaySlot;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.ObjHolder;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard;
import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard.Line;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.CustomDisplay;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.Display;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.Option;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.RunnableOption;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.SoundDisplay;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.TitleDisplay;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.OptionType;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.XPDisplay;
import com.dercd.bukkit.cdapi.tools.minecraft.PlayerSnapshot;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.gamestate.DMCountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.EndState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;
import com.dercd.bukkit.plugins.hgames.gamestate.HGCountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.LobbyVotingState;
import com.dercd.bukkit.plugins.hgames.gamestate.LobbyWaitingState;
import com.dercd.bukkit.plugins.hgames.gamestate.PreparingState;
import com.dercd.bukkit.plugins.hgames.gamestate.RunningGameState;
import com.dercd.bukkit.plugins.hgames.gamestate.HGProtectionState;
import com.dercd.bukkit.plugins.hgames.gamestate.InitState;
import com.dercd.bukkit.plugins.hgames.gamestate.HGPlayingState;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;
import com.dercd.bukkit.plugins.hgames.map.MapPool;
import com.dercd.bukkit.plugins.hgames.map.MapVoteManager;
import com.dercd.bukkit.plugins.hgames.map.VoteEntry;
import com.dercd.bukkit.plugins.hgames.map.VoteManager;
import com.dercd.bukkit.plugins.hgames.map.chest.Chest;
import com.dercd.bukkit.plugins.hgames.map.dm.DMap;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapInfo;
import com.dercd.bukkit.plugins.hgames.map.dm.DMapTimes;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapTimes;
import com.dercd.bukkit.plugins.hgames.map.loading.DMapLoader;
import com.dercd.bukkit.plugins.hgames.scoreboard.HGScoreboard;
import com.dercd.bukkit.plugins.hgames.scoreboard.ScoreboardLineMapping;
import com.dercd.bukkit.plugins.hgames.scoreboard.ScoreboardLineType;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;
import com.dercd.bukkit.plugins.hgames.user.ReservationManager;
import com.dercd.bukkit.plugins.hgames.user.Spectator;
import com.dercd.bukkit.plugins.hgames.user.User;
import com.dercd.bukkit.plugins.hgames.user.UserGroup;
import com.google.common.collect.ImmutableList;
import net.minecraft.server.v1_11_R1.PacketPlayOutTitle.EnumTitleAction;

public class Game
{
	private static Game instance;
	
	public HGames2 hgames2;
	protected Log clog;
	
	protected ISettings settings;
	protected IIOManager ioManager;
	
	protected HashMap<UUID, User> users;
	
	public CDScoreboard currentScoreboard;
	public Countdown currentCountdown;
	public GameState state;
	
	protected TextPool textPool;
	protected MapPool hgMapPool;
	protected MapPool dMapPool;
	
	public EndState endState;
	public static final int[] WINNER_DISPLAY_TIMES = new int[] { (int) (Tools.TICKS_PER_SECOND * 0.5), Tools.TICKS_PER_SECOND * 2, (int) (Tools.TICKS_PER_SECOND * 0.5) }; //Ticks for Countdown-class
	public static final int[] MSH_DISPLAY_TIMES = new int[] { (int) (Tools.TICKS_PER_SECOND * 0.5), Tools.TICKS_PER_SECOND * 3, Tools.TICKS_PER_SECOND * 1 }; //Ticks for Countdown-class
	public static final int[] DM_COUNTDOWN_REDUCE_NOTICE_TIMES = new int[] { Tools.TICKS_PER_SECOND * 1, Tools.TICKS_PER_SECOND * 2, Tools.TICKS_PER_SECOND * 1 }; //Ticks for Countdown-class
	public static final int[] MAP_LOADING_WAITING_TIMES = new int[] { Tools.TICKS_PER_SECOND * 1, (int) (Tools.TICKS_PER_SECOND * 0.5), Tools.TICKS_PER_SECOND * 1 }; //Ticks for Countdown-class
	public static final int MAP_LOADING_WAITING_PERIOD = (int)(2.5 * Tools.TICKS_PER_SECOND);
	
	public static final int MAX_EXTERNAL_SPECTATORS = 20;
	
	private Game()
	{}
	
	public void init()
	{
		this.clog = this.hgames2.clog;
		
		IIOManager ioManager = new IOManager();
		
		try
		{
			this.clog.log("Deleting old map folders", this);
			ioManager.deleteTmpMapFolders();
			this.clog.log("Deleting old playerdata", this);
			ioManager.deletePlayerData();
		}
		catch(IOException x)
		{ }
		
		this.ioManager = ioManager;
		this.users = new HashMap<UUID, User>();
		
		GameState firstState = createStates();
		this.state = firstState;
		
	}
	public void load()
	{
		IIOManager ioManager = this.ioManager;
		
		ISettings settings = ioManager.loadSettings();
		TextPool textPool = ioManager.loadTextPool();
		MapPool hgMapPool = ioManager.loadHGMapPool();
		MapPool dMapPool = ioManager.loadDMapPool();
		
		this.settings = settings;
		this.textPool = textPool;
		this.hgMapPool = hgMapPool;
		this.dMapPool = dMapPool;
		
		GameState firstState = this.state;
		this.state = firstState.applyState();
	}
	protected GameState createStates()
	{
		int stateId = 0;
		GameState firstState;
		GameState state;
		GameState endState;
		
		firstState = state = initState(new InitState(stateId++), null);
		state = initState(new LobbyVotingState(stateId++), state);
		state = initState(new LobbyWaitingState(stateId++), state);
		state = initState(new HGCountdownState(stateId++), state);
		state = initState(new HGProtectionState(stateId++), state);
		state = initState(new HGPlayingState(stateId++), state);
		state = initState(new DMCountdownState(stateId++), state);
		state = initState(new DMPlayingState(stateId++), state);
		endState = state = initState(new EndState(stateId++), state);
		
		this.endState = (EndState)endState;
		
		return firstState;
	}
	protected GameState initState(GameState state, GameState previousState)
	{
		state.setPreviousState(previousState);
		if(previousState != null)
			previousState.setNextState(state);
		return state;
	}
	
	//State methods
	public void moveToNextState()
	{
		if(this.state.getNextState() != null)
			this.clog.log("Moving to next state " + this.state.getNextState().getName(), this);
		setStateUnsafe(this.state.applyNextState());
	}
	public void moveToNextStateFrom(Class fromState)
	{
		if(this.state.getClass() != fromState)
			return;
		moveToNextState();
	}
	public void moveToNextState(Class nextState)
	{
		if(this.state.getNextState().getClass() != nextState)
			return;
		moveToNextState();
	}
	public void moveToPreviousState()
	{
		if(this.state.getPreviousState() != null)
			this.clog.log("Moving to previous state " + this.state.getPreviousState().getName(), this);
		setStateUnsafe(this.state.applyPreviousState());
	}
	public void moveToPreviousStateFrom(Class fromState)
	{
		if(this.state.getClass() != fromState)
			return;
		moveToPreviousState();
	}
	public void moveToPreviousState(Class nextState)
	{
		if(this.state.getNextState().getClass() != nextState)
			return;
		moveToPreviousState();
	}
	public void moveToEndState()
	{
		if(this.endState == null)
			return;
		this.state.setNextState(this.endState);
		moveToNextState();
	}
	public void moveToEndStateFrom(Class fromState)
	{
		if(this.state.getClass() != fromState)
			return;
		moveToEndState();
	}
	
	//denyX -> methods called when gamestate hasn't overriden
	//onX-method and doesn't have ability X
	public void denyOnLogin(PlayerLoginEvent e)
	{
		e.disallow(Result.KICK_OTHER, "Sorry, you cannot join at the moment");
	}
	public void denyOnJoin(PlayerJoinEvent e)
	{
		e.setJoinMessage(null);
		e.getPlayer().kickPlayer("Sorry, no slot available");
	}
	public void defaultOnLeave(PlayerQuitEvent e)
	{
		Game game = this;
		TextPool textPool = game.getTextPool();
		Player p = e.getPlayer();
		
		e.setQuitMessage(null);
		String playerStr = game.formatUserName(p);
		Bukkit.broadcastMessage(textPool.getText("playerLeave", playerStr));
	}
	public void denyOnMove(PlayerMoveEvent e)
	{
		boolean moved = Tools.hasMoved(e.getFrom(), e.getTo()); 
		if(moved)
		{
			Location middle = Tools.getMiddle(e.getFrom());
			e.getPlayer().teleport(middle);
		}
	}
	public void denyOnPlayerDamageByPlayer(EntityDamageByEntityEvent e, Player causer)
	{
		e.setCancelled(true);
	}
	public void denyOnPlayerDamageByWorld(EntityDamageEvent e)
	{
		e.setCancelled(true);
	}
	public void denyOnInteract(PlayerInteractEvent e)
	{
		e.setCancelled(true);
	}
	public void denyOnBlockBreak(BlockBreakEvent e)
	{
		e.setCancelled(true);
	}
	public void defaultOnCommand(CommandEvent e)
	{
		TextPool textPool = getTextPool();
		e.getSender().sendMessage(textPool.getMultiText("denyCommand"));
	}
	public void defaultOnDeath(PlayerDeathEvent e)
	{
	}
	public void defaultOnRespawn(PlayerRespawnEvent e)
	{
	}
	public void defaultOnEntityDamage(EntityDamageEvent e)
	{ }
	public void defaultOnChat(AsyncPlayerChatEvent e)
	{
		Game game = this;
		TextPool textPool = game.getTextPool();
		Player p = e.getPlayer();
		User u = game.getUser(p);
		String message = e.getMessage();
		
		String formattedPlayer = game.formatUserName(p);
		String[] formattedMessage = textPool.getMultiText("chat", formattedPlayer, message);
		
		//Show message to all players
		if(u.isAlive())
		{
			Tools.sendMessage(formattedMessage);
		}
		//Show message only to other spectators
		else
		{
			ArrayList<Player> spectatingUsers = game.convertToPlayers(game.getSpectatingPlayers());
			Tools.sendMessage(spectatingUsers, formattedMessage);
		}
		
		e.setCancelled(true);
	}
	
	//doSomethingForX -> methods called by gamestates
	public void openChest(Player p, Chest c)
	{
		if(c.isCreepy())
		{
			Block loc = c.getLocation();
			loc.setType(Material.AIR);
			spawnCreepy(loc.getLocation());
			return;
		}
		
		Inventory inv = c.getContent().getInventory();
		p.openInventory(inv);
	}
	public void spawnCreepy(Location l)
	{
		Zombie z = l.getWorld().spawn(l, Zombie.class);
		z.setBaby(true);
		z.setCanPickupItems(false);
	}
	public void prepareSpectator(Spectator s, Location spawn)
	{
		Player p = s.getPlayer();
		Tools.resetPlayer(p);
		p.setGameMode(GameMode.SPECTATOR);
		p.teleport(spawn);
	}
	public void teleportUsers(Collection<? extends User> users, Location location, double period)
	{
		teleportUsers(users, location, period, null);
	}
	public void teleportUsers(Collection<? extends User> users, Location location, double period, Runnable afterTeleport)
	{
		ArrayList<Location> locations = new ArrayList<Location>();
		locations.add(location);
		teleportUsers(users, locations, period, afterTeleport);
	}
	public void teleportUsers(Collection<? extends User> users, Collection<Location> locations, double period)
	{
		teleportUsers(users, locations, period, null);
	}
	public void teleportUsers(Collection<? extends User> users, Collection<Location> locations, double period, Runnable afterTeleport)
	{
		if(users == null)
			users = getUsers();
		Iterator<? extends User> userIterator = users.iterator();
		Iterator<Location> locationIterator = locations.iterator();
		
		final ObjHolder<BukkitTask> bukkitTask = new ObjHolder<BukkitTask>();
		bukkitTask.obj = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			if(!userIterator.hasNext())
			{
				if(afterTeleport != null)
					afterTeleport.run();
				bukkitTask.obj.cancel();
				return;
			}
			
			User currentUser = userIterator.next();
			Location currentSpawn = locationIterator.next();
			Player bukkitPlayer = currentUser.getPlayer();
			
			if(!bukkitPlayer.isOnline())
				return;
			
			bukkitPlayer.teleport(currentSpawn);
		}, 0, (long)(Tools.TICKS_PER_SECOND * period));
	}
	public MapVoteManager createVoteManager()
	{
		MapPool mapPool = getHGMapPool();
		ISettings settings = getSettings();
		int maxMaps = settings.getMaxMapsToVote();
		
		ArrayList<MapInfo> maps = mapPool.getRandomMaps(maxMaps);
		maps = Tools.shuffleList(maps);
		MapVoteManager voteManager = new MapVoteManager();
		for(int i = 0; i < maps.size(); ++i)
		{
			//Start voteId at 1 so users aren't confused
			voteManager.putEntry(i + 1, maps.get(i));
		}
		return voteManager;
	}
	public HGScoreboard createMapVotingScoreboard(MapVoteManager voteManager)
	{
		HGScoreboard s = new HGScoreboard();
		TextPool textPool = getTextPool();
		
		ArrayList<VoteEntry<MapInfo>> entries = voteManager.getEntriesSorted();
		for(VoteEntry<MapInfo> entry : entries)
		{
			int voteId = entry.getVoteId();
			MapInfo mapInfo = entry.getVoteValue();
			String line = textPool.getText("mapSelection", String.valueOf(voteId), mapInfo.getName());
			s.appendLine(line);
		}
		s.setTitle(textPool.getText("votingScoreboardTitle"));
		return s;
	}
	public HGScoreboard createHGPlayingScoreboard(MapInfo mapInfo)
	{
		HGScoreboard sb = new HGScoreboard();
		TextPool textPool = getTextPool();
		ScoreboardLineMapping lineMapping = new ScoreboardLineMapping(sb);
		Line line;
		
		line = sb.appendLine("players");
		lineMapping.put(ScoreboardLineType.PLAYER_COUNT, line);
		
		line = sb.appendEmptyLine();
		lineMapping.put(ScoreboardLineType.REQUIRED_PLAYER_COUNT, line);
		
		sb.appendEmptyLine();
		
		line = sb.appendLine("mapTitle");
		lineMapping.put(ScoreboardLineType.CURRENT_MAP, line);
		
		sb.appendEmptyLine();
		
		line = sb.appendLine("teamsAllowed");
		lineMapping.put(ScoreboardLineType.TEAMS_ALLOWED, line);
		
		sb.setLineMapping(lineMapping);
		updateScoreboardData(mapInfo, sb);
		
		sb.setDisplaySlot(DisplaySlot.SIDEBAR);
		sb.setTitle(textPool.getText("playingScoreboardTitle"));
		
		return sb;
	}
	public void updateScoreboardData(MapInfo mapInfo, HGScoreboard scoreboard)
	{
		updateScoreboardData(mapInfo, scoreboard, true);
	}
	public void updateScoreboardData(MapInfo mapInfo, HGScoreboard scoreboard, boolean displayRequired)
	{
		ScoreboardLineMapping lineMapping = scoreboard.getLineMapping();
		if(lineMapping == null)
			return;
		TextPool textPool = getTextPool();
		ISettings settings = getSettings();
		
		int playerCount = getLivingPlayerCount();
		
		for(Entry<ScoreboardLineType, Line> e : lineMapping.entrySet())
		{
			ScoreboardLineType lineType = e.getKey();
			Line line = e.getValue();
			switch(lineType)
			{
				case CURRENT_MAP:
					line.setText(textPool.getText("mapTitle", mapInfo.getName()));
					break;
				case PLAYER_COUNT:
					line.setText(textPool.getText("players", Integer.toString(playerCount)));
					break;
				case REQUIRED_PLAYER_COUNT:
					if(!displayRequired)
						break;
					
					if(playerCount < mapInfo.getMinPlayers())
						line.setText(textPool.getText("playersRequired", Integer.toString(mapInfo.getMinPlayers())));
					else
						line.setEmpty();
					break;
				case TEAMS_ALLOWED:
					if(settings.getTeamsAllowed())
						line.setText(textPool.getText("teamsAllowed"));
					else
						line.setText(textPool.getText("teamsForbidden"));
					break;
			}
		}
	}
	public Countdown createLobbyVotingCountdown(VoteManager voteManager)
	{
		Game game = this;
		ISettings settings = getSettings();
		TextPool textPool = getTextPool();
		int votingTime = settings.getVotingTime();
		int waitingTime = settings.getWaitingTime();
		int fullTime = votingTime + waitingTime;
		final int PARALLEL_VOTE_COUNTING_BUFFER = 5;
		int parallelMapVoteBufferTime = votingTime < PARALLEL_VOTE_COUNTING_BUFFER ? 0 : fullTime - (votingTime - PARALLEL_VOTE_COUNTING_BUFFER);
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), fullTime,
				new Option(OptionType.DISPLAY,
						new XPDisplay(null, fullTime, true)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_30S, null, Sound.ENTITY_ITEM_PICKUP)),
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("lobbyCountdownSubtitle"), Display.TimeTemplate.DEFAULT_30S, null, EnumTitleAction.SUBTITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new RunnableOption(() ->
				{
					voteManager.enableParallelVoteCounting();
				}, parallelMapVoteBufferTime),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(LobbyVotingState.class);
				}, fullTime - votingTime),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(LobbyWaitingState.class);
				}));
		return c;
	}
	public Countdown createLobbyWaitingCountdown()
	{
		Game game = this;
		ISettings settings = getSettings();
		TextPool textPool = getTextPool();
		int waitingTime = settings.getWaitingTime();
		int fullTime = waitingTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), fullTime,
				new Option(OptionType.DISPLAY,
						new XPDisplay(null, fullTime, true)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_30S, null, Sound.ENTITY_ITEM_PICKUP)),
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("lobbyCountdownSubtitle"), Display.TimeTemplate.DEFAULT_30S, null, EnumTitleAction.SUBTITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(LobbyWaitingState.class);
				}));
		return c;
	}
	public Countdown createHGWaitingCountdown(HGMap map)
	{
		Game game = this;
		HGMapTimes mapTimes = map.getMapTimes();
		TextPool textPool = getTextPool();
		int waitingTime = mapTimes.spawnTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), waitingTime,
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("hgWaitingCountdownTitle"), Display.TimeTemplate.EVERY_SECOND, null, EnumTitleAction.TITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_30S, null, Sound.ENTITY_ITEM_PICKUP)),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(HGCountdownState.class);
				}));
		return c;
	}
	public Countdown createHGProtectionCountdown(HGMap map, HGScoreboard scoreboard)
	{
		Game game = this;
		HGMapTimes mapTimes = map.getMapTimes();
		TextPool textPool = getTextPool();
		int protectionTime = mapTimes.protectionTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), protectionTime,
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("hgProtectionCountdownTitle"), Display.TimeTemplate.DEFAULT_60S, null, EnumTitleAction.SUBTITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_60S, null, Sound.ENTITY_ITEM_PICKUP)),
				new Option(OptionType.DISPLAY,
						new CustomDisplay(textPool.getText("countdownTime"), Display.TimeTemplate.EVERY_SECOND, (text) ->
						{
							scoreboard.setTitle(text);
						})),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(HGProtectionState.class);
				}));
		return c;
	}
	public Countdown createHGPlayingCountdown(HGMap map, HGScoreboard scoreboard)
	{
		Game game = this;
		HGMapTimes mapTimes = map.getMapTimes();
		TextPool textPool = getTextPool();
		int playingTime = mapTimes.playTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), playingTime,
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("hgPlayingCountdownTitle"), Display.TimeTemplate.DEFAULT_1H, null, EnumTitleAction.SUBTITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_1H, null, Sound.ENTITY_ITEM_PICKUP)),
				new Option(OptionType.DISPLAY,
						new CustomDisplay(textPool.getText("countdownTime"), Display.TimeTemplate.EVERY_SECOND, (text) ->
						{
							scoreboard.setTitle(text);
						})),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(HGPlayingState.class);
				}));
		return c;
	}
	public Countdown createDMWaitingCountdown(DMap map)
	{
		Game game = this;
		DMapTimes mapTimes = map.getMapTimes();
		TextPool textPool = getTextPool();
		int waitingTime = mapTimes.spawnTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), waitingTime,
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("dmWaitingCountdownTitle"), Display.TimeTemplate.DEFAULT_30S, null, EnumTitleAction.TITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_30S, null, Sound.ENTITY_ITEM_PICKUP)),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(DMCountdownState.class);
				}));
		return c;
	}
	public Countdown createDMPlayingCountdowdn(DMap map, HGScoreboard scoreboard)
	{
		Game game = this;
		DMapTimes mapTimes = map.getMapTimes();
		TextPool textPool = getTextPool();
		int playingTime = mapTimes.playTime;
		
		Countdown c = new Countdown(CDAPI.getInstance().getHandler(), playingTime,
				new Option(OptionType.DISPLAY,
						new TitleDisplay(textPool.getText("dmPlayingCountdownTitle"), Display.TimeTemplate.DEFAULT_5M, null, EnumTitleAction.SUBTITLE, TitleDisplay.DisplayTimes.DEFAULT_SECOND)),
				new Option(OptionType.DISPLAY,
						new SoundDisplay(Display.TimeTemplate.DEFAULT_5M, null, Sound.ENTITY_ITEM_PICKUP)),
				new Option(OptionType.DISPLAY,
						new CustomDisplay(textPool.getText("countdownTime"), Display.TimeTemplate.EVERY_SECOND, (text) ->
						{
							scoreboard.setTitle(text);
						})),
				new RunnableOption(() ->
				{
					game.moveToNextStateFrom(DMPlayingState.class);
				}));
		return c;
	}
	public String formatUserName(User u)
	{
		return formatUserName(u, false);
	}
	public String formatUserName(Player p)
	{
		return formatUserName(p, false);
	}
	public String formatUserName(UUID u)
	{
		return formatUserName(u, false);
	}	
	public String formatUserName(User u, boolean ignoreLivingState)
	{
		return formatUserName(u.getPlayer(), ignoreLivingState);
	}
	public String formatUserName(Player p, boolean ignoreLivingState)
	{
		if(p == null)
			return "null";
		return formatUserName(p.getUniqueId(), ignoreLivingState);
	}
	public String formatUserName(UUID u, boolean ignoreLivingState)
	{
		TextPool textPool = getTextPool();
		
		User user = getUser(u);
		if(user == null)
			return "EmptyName";
		
		String prefix;
		if(user.isAlive() || ignoreLivingState)
		{
			UserGroup userGroup = user.getUserGroup();
			prefix = textPool.getText(userGroup.getPrefixTextId());
		}
		else
		{
			prefix = textPool.getText("prefixDead");
		}
		
		return prefix + user.getName() + ChatColor.RESET;
	}
	
	public void dropInventory(Player p)
	{
		Inventory inv = p.getInventory();
		ItemStack[] items = inv.getContents();
		Location loc = p.getLocation();
		World world = loc.getWorld();
		for(ItemStack item : items)
			if(item != null)
				world.dropItemNaturally(loc, item);
		inv.clear();
	}
	public void displayDeathMessage(PlayerDeathEvent e)
	{
		Game game = this;
		Player killed = e.getEntity();
		if(killed == null)
			return;
		Player killer = killed.getKiller();
		TextPool textPool = game.getTextPool();
		
		e.setDeathMessage(null);
		String[] deathMessage;
		String killedStr = game.formatUserName(killed, true);
		String killerStr = game.formatUserName(killer, true);
		if(killer == null)
			deathMessage = textPool.getMultiText("playerDied", killedStr);
		else if(killer == killed)
			deathMessage = textPool.getMultiText("playerKilledSelf", killedStr);
		else
			deathMessage = textPool.getMultiText("playerKilled", killerStr, killedStr);
		Tools.broadcastMessages(deathMessage);
	}
	public TitleTemplate generateWinnerTitle(PlayerSnapshot ps)
	{
		TitleTemplate title = new TitleTemplate();
		title.setTitle(formatUserName(ps.uuid));
		title.setSubTitle(Tools.getHeartString(ps));
		title.setDisplayTimes(WINNER_DISPLAY_TIMES);
		return title;
	}
	public String generateWinnersString(ArrayList<? extends User> winners)
	{
		ArrayList<String> formattedUsernames = new ArrayList<String>();
		for(User u : winners)
			formattedUsernames.add(formatUserName(u));
		return String.join(", ", formattedUsernames);
	}
	public DMapLoader createDMapLoader()
	{
		Game game = this;
		MapPool dMapPool = game.getDMapPool();
		
		DMapInfo mapInfo = (DMapInfo)dMapPool.getRandomMaps(1, 1, game.getLivingPlayerCount(), true).get(0);
		
		DMapLoader mapLoader = new DMapLoader();
		mapLoader.setMapInfo(mapInfo);
		mapLoader.createTasks();
		
		return mapLoader;
	}
	public void cancelLobbyCountdown(Countdown c)
	{
		if(c == null || !c.isRunning())
			return;
		c.cancel();
		Tools.setLevelAndExp(0, 0);
	}
	public void showMidScreenHearts(Player hitter, Player hitted, double damage)
	{
		double finalHealth = hitted.getHealth() - damage;
		double maxHealth = Tools.getMaxHealth(hitted);
		String heartString = Tools.getHeartString(finalHealth, maxHealth);
		
		Tools.sendTitle(heartString, EnumTitleAction.SUBTITLE, ImmutableList.of(hitter), MSH_DISPLAY_TIMES);
	}
	public void updateTablist(User u)
	{
		if(u.isAlive())
			Tools.addToTablist(u.getPlayer());
		else
		{
			ArrayList<Player> receivers;
			receivers = convertToPlayers(getSpectatingPlayers());
			receivers.remove(u.getPlayer());
			Tools.addToTablist(u.getPlayer(), receivers);
			receivers = convertToPlayers(getLivingPlayers());
			receivers.remove(u.getPlayer());
			Tools.removeFromTablist(u.getPlayer(), receivers);
		}
	}
	public boolean isCommandSenderPlayer(CommandSender sender)
	{
		return sender instanceof Player;
	}
	public boolean isCommandSenderAlive(CommandSender sender)
	{
		if(!(sender instanceof Player))
			return false;
		Player p = (Player)sender;
		
		User u = this.getUser(p);
		if(u == null)
			return false;
		
		return u.isAlive();
	}
	public boolean isCommandSenderConsole(CommandSender sender)
	{
		return sender instanceof ConsoleCommandSender;
	}
	public void displayDMCountdownReduceNotice()
	{
		Game game = this;
		TextPool textPool = this.getTextPool();
		
		ArrayList<Player> players = game.convertToPlayers(game.getLivingPlayers());
		
		String message = textPool.getText("dmCountdownReduceNotice");
		
		Tools.sendTitle(message, EnumTitleAction.SUBTITLE, players, DM_COUNTDOWN_REDUCE_NOTICE_TIMES);
	}
	public BukkitTask createMapLoadingWaitingDisplay()
	{
		Game game = this;
		TextPool textPool = game.getTextPool();
		
		String message = textPool.getText("waitMapLoading");
		
		return Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			Tools.sendTitle(message, EnumTitleAction.TITLE, MAP_LOADING_WAITING_TIMES);
		}, 0, MAP_LOADING_WAITING_PERIOD);
	}
	public void setGameMode(GameMode gameMode, Collection<? extends User> users)
	{
		if(users != null)
			users = getUsers();
		for(User u : users)
			u.getPlayer().setGameMode(gameMode);
	}
	public void setLobbyGameRules(World w)
	{
		w.setGameRuleValue("commandBlockOutput", "false");
		w.setGameRuleValue("doEntityDrops", "false");
		w.setGameRuleValue("doFireTicks", "false");
		w.setGameRuleValue("doMobLoot", "false");
		w.setGameRuleValue("doMobSpawning", "false");
		w.setGameRuleValue("doTileDrops", "false");
		w.setGameRuleValue("doWeatherCycle", "false");
	}
	public boolean isAnnoyingBlock(Block b)
	{
		if(b == null)
			return false;
		
		Material m = b.getType();
		switch(m)
		{
			case LEAVES:
			case LEAVES_2:
			case YELLOW_FLOWER:
			case RED_ROSE:
			case GRASS:
			case DEAD_BUSH:
			case DOUBLE_PLANT:
			case BROWN_MUSHROOM:
			case RED_MUSHROOM:
			case WEB:
				return true;
			default:
				break;
		}
		
		return false;
	}
	public Map getCurrentMap()
	{
		GameState state = getCurrentState();
		
		if(state instanceof RunningGameState)
			return ((RunningGameState) state).getMap();
		if(state instanceof HGCountdownState)
			return ((HGCountdownState) state).getMap();
		
		return null;
	}
	public MapInfo getCurrentMapInfo()
	{
		GameState state = getCurrentState();
		
		if(state instanceof RunningGameState)
			return ((RunningGameState) state).getMap().getMapInfo();
		if(state instanceof HGCountdownState)
			return ((HGCountdownState) state).getMap().getMapInfo();
		if(state instanceof LobbyWaitingState)
			return ((LobbyWaitingState) state).getVotedMapInfo();
		
		return null;
	}
	public Integer getCurrentMaxUsers()
	{
		MapInfo mapInfo = getCurrentMapInfo();
		if(mapInfo != null)
			return mapInfo.getMaxPlayers();
		
		GameState state = getCurrentState();
		if(state instanceof LobbyVotingState)
			return ((LobbyVotingState) state).getVoteManager().getLowestMaxPlayerCount();
		
		return null;
	}
	public ReservationManager getCurrentReservationManager()
	{
		GameState state = getCurrentState();
		
		if(state instanceof PreparingState)
			return ((PreparingState) state).getReservationManager();
		
		return null;
	}
	
	//onX -> route events to gamestates
	public void onPlayerDamage(EntityDamageEvent e)
	{
		//Damage to non-player entities is allowed
		if(e.getEntity().getType() != EntityType.PLAYER)
			return;
		
		DamageCause cause = e.getCause();
		Player causer = null;
		
		EntityDamageByEntityEvent ee = null;
		if(e instanceof EntityDamageByEntityEvent)
			ee = (EntityDamageByEntityEvent)e;
		
		//Process damage from another player
		if(cause == DamageCause.ENTITY_ATTACK
			&& ee != null
			&& ee.getDamager().getType() == EntityType.PLAYER)
		{
			causer = (Player)ee.getDamager();
		}
		
		//Process damage from projectiles of other players
		if(cause == DamageCause.PROJECTILE
			&& ee != null
			&& ee.getDamager() instanceof Projectile)
		{
			Projectile p = (Projectile)ee.getDamager();
			ProjectileSource source = p.getShooter();
			if(source instanceof Player)
			{
				causer = (Player)source;
			}
		}
		
		//World-damage
		if(causer == null)
			this.state.onPlayerDamageByWorld(e);
		//Player-damage
		else
			this.state.onPlayerDamageByPlayer(ee, causer);
	}
	public void onLogin(PlayerLoginEvent e)
	{
		this.state.onLogin(e);
	}
	public void onJoin(PlayerJoinEvent e)
	{
		Player p = e.getPlayer();
		
		if(this.currentScoreboard != null)
			p.setScoreboard(this.currentScoreboard.getScoreboard());
		
		this.state.onJoin(e);
	}
	public void onLeave(PlayerQuitEvent e)
	{
		quitUser(getUser(e.getPlayer()));
		this.state.onLeave(e);
	}
	public void onInteract(PlayerInteractEvent e)
	{
		this.state.onInteract(e);
	}
	public void onBlockBreak(BlockBreakEvent e)
	{
		Game game = this;
		Block b = e.getBlock();
		
		boolean annoyingBlock = game.isAnnoyingBlock(b);
		if(annoyingBlock)
			this.state.onAnnoyingBlockBreak(e);
		else
			this.state.onBlockBreak(e);
	}
	public void onFade(BlockFadeEvent e)
	{
		e.setCancelled(true);
	}
	public void onCommand(CommandEvent e) throws CDCException
	{
		this.state.onCommand(e);
	}
	public void onMove(PlayerMoveEvent e)
	{
		this.state.onMove(e);
	}
	public void onDeath(PlayerDeathEvent e)
	{
		this.state.onDeath(e);
	}
	public void onRespawn(PlayerRespawnEvent e)
	{
		this.state.onRespawn(e);
	}
	public void onEntityDamage(EntityDamageEvent e)
	{
		this.state.onEntityDamage(e);
	}
	public void onChat(AsyncPlayerChatEvent e)
	{
		this.state.onChat(e);
	}
	
	//Getters and setters
	public void joinUser(User u)
	{
		this.users.put(u.getPlayer().getUniqueId(), u);
	}
	public User getUser(UUID u)
	{
		return this.users.get(u);
	}
	public User getUser(Player p)
	{
		return this.users.get(p.getUniqueId());
	}
	public void quitUser(User u)
	{
		if(u == null)
			return;
		u.setOnline(false);
	}
	
	public int getUserCount()
	{
		int players = 0;
		for(User u : this.users.values())
			if(u.isOnline())
				++players;
		return players;
	}
	public int getPlayerCount()
	{
		int players = 0;
		for(User u : this.users.values())
			if(u.isOnline() && u instanceof HGPlayer)
				++players;
		return players;
	}
	public int getLivingPlayerCount()
	{
		int players = 0;
		for(User u : this.users.values())
			if(u.isOnline() && u instanceof HGPlayer && ((HGPlayer)u).isAlive())
				++players;
		return players;
	}
	public int getSpectatorCount()
	{
		int spectators = getUserCount() - getPlayerCount();
		return spectators;
	}
	
	public ArrayList<User> getUsers()
	{
		return new ArrayList<User>(this.users.values());
	}
	public ArrayList<HGPlayer> getPlayers()
	{
		ArrayList<HGPlayer> players = new ArrayList<HGPlayer>();
		for(User u : this.users.values())
			if(u.isOnline() && u instanceof HGPlayer)
				players.add((HGPlayer)u);
		return players;
	}
	public ArrayList<Spectator> getSpectators()
	{
		ArrayList<Spectator> spectators = new ArrayList<Spectator>();
		for(User u : this.users.values())
			if(u.isOnline() && u instanceof Spectator)
				spectators.add((Spectator)u);
		return spectators;
	}
	public ArrayList<HGPlayer> getLivingPlayers()	
	{
		ArrayList<HGPlayer> players = getPlayers();
		players.removeIf((hgplayer) ->  !hgplayer.isAlive() || !hgplayer.isOnline());
		return players;
	}
	public ArrayList<User> getSpectatingPlayers()
	{
		ArrayList<User> players = getUsers();
		players.removeIf((user) -> user.isAlive() || !user.isOnline());
		return players;
	}
	public ArrayList<Player> convertToPlayers(Collection<? extends User> users)
	{
		ArrayList<Player> players = new ArrayList<Player>();
		for(User u : users)
			players.add(u.getPlayer());
		return players;
	}
	
	public ISettings getSettings()
	{
		return this.settings;
	}
	public IIOManager getIOManager()
	{
		return this.ioManager;
	}
	
	public TextPool getTextPool()
	{
		return this.textPool;
	}
	public MapPool getHGMapPool()
	{
		return this.hgMapPool;
	}
	public MapPool getDMapPool()
	{
		return this.dMapPool;
	}
	
	public GameState getCurrentState()
	{
		return this.state;
	}
	public void setStateUnsafe(GameState state)
	{
		this.state = state;
		this.communicator.sendGameState();
	}
	
	public CDScoreboard getScoreboard()
	{
		return this.currentScoreboard;
	}
	public void setScoreboard(CDScoreboard s)
	{
		this.currentScoreboard = s;
		s.displayScoreboard();
		for(Player p : Bukkit.getOnlinePlayers())
			p.setScoreboard(s.getScoreboard());
	}
	
	public Countdown getCurrentCountdown()
	{
		return this.currentCountdown;
	}
	public void setCurrentCountdown(Countdown c)
	{
		if(this.currentCountdown == c)
			return;
		if(this.currentCountdown != null && this.currentCountdown.isRunning())
			this.currentCountdown.cancel();
		this.currentCountdown = c;
		if(!c.isRunning())
			c.start();
	}
	
	public synchronized static Game getInstance()
	{
		if(Game.instance == null)
			Game.instance = new Game();
		return Game.instance;
	}
}
