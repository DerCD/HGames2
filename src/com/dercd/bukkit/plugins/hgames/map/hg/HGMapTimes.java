package com.dercd.bukkit.plugins.hgames.map.hg;

import com.dercd.bukkit.plugins.hgames.map.HGMapRefillTimes;

public class HGMapTimes
{
	public int spawnTime;
	public int protectionTime;
	public int playTime;
	public HGMapRefillTimes refillTimes;
}
