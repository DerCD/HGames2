package com.dercd.bukkit.plugins.hgames.map.hg;

import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestMap;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public class HGMap extends Map
{
	protected ItemPool specialItemPool;
	public HGMapTimes mapTimes;
	protected int chestsToPlace;
	protected int minChestDistance;
	protected int maxDMCountdownReducePlayers;
	
	
	public ItemPool getSpecialItemPool()
	{
		return this.specialItemPool;
	}
	public HGMapTimes getMapTimes()
	{
		return this.mapTimes;
	}
	public int getChestsToPlace()
	{
		return this.chestsToPlace;
	}
	public int getMinChestDistance()
	{
		return this.minChestDistance;
	}
	public int getMaxDMCountdownReducePlayers()
	{
		return this.maxDMCountdownReducePlayers;
	}
	public ChestMap getChestMap()
	{
		if(this.chestManager == null)
			return null;
		return this.chestManager.getChestMap();
	}
	
	public void setSpecialItemPool(ItemPool specialItemPool)
	{
		this.specialItemPool = specialItemPool;
	}
	public void setMapTimes(HGMapTimes mapTimes)
	{
		this.mapTimes = mapTimes;
	}
	public void setChestsToPlace(int chestsToPlace)
	{
		this.chestsToPlace = chestsToPlace;
	}
	public void setMinChestDistance(int minChestDistance)
	{
		this.minChestDistance = minChestDistance;
	}
	public void setMaxDMCountdownReducePlayers(int maxDMCountdownReducePlayers)
	{
		this.maxDMCountdownReducePlayers = maxDMCountdownReducePlayers;
	}
}
