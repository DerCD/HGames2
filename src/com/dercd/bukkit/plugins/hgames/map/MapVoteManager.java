package com.dercd.bukkit.plugins.hgames.map;

public class MapVoteManager extends VoteManager<MapInfo>
{
	int lowestMaxPlayerCount = -1;
	
	public void putEntry(int id, MapInfo val)
	{
		if(val != null)
		{
			if(this.lowestMaxPlayerCount == -1 || val.getMaxPlayers() < this.lowestMaxPlayerCount)
				this.lowestMaxPlayerCount = val.getMaxPlayers();
		}
		super.putEntry(id, val);
	}
	
	public int getLowestMaxPlayerCount()
	{
		return this.lowestMaxPlayerCount;
	}
}
