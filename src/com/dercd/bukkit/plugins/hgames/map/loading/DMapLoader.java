package com.dercd.bukkit.plugins.hgames.map.loading;

public class DMapLoader extends MapLoader
{
	public static final int MAP_LOADING_BUFFER = 20;
	
	public void createTasks()
	{
		//Hoping current thread is bukkit main thread
		//If not, we have a problem...
		Thread bukkitThread = Thread.currentThread();
		
		addTask(new DMCopyWorldTask(), bukkitThread);
		addTask(new DMLoadWorldTask(), bukkitThread);
		addTask(new DMLoadMapTask(), bukkitThread);
		addTask(new DMWorldSettingsTask(), bukkitThread);
	}
	public void addTask(MapLoadingTask task, Thread bukkitThread)
	{
		task.setBukkitThread(bukkitThread);
		addTask(task);
	}
}
