package com.dercd.bukkit.plugins.hgames.map.loading;

import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.IIOManager;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;

public class HGLoadWorldTask extends MapLoadingTask
{
	@Override
	public void runTask(MapInfo mapInfo, Map map)
	{
		runSync(() ->
		{
			IIOManager ioManager = Game.getInstance().getIOManager();
			ioManager.loadHGWorld();
			onFinished();
		});
	}
}
