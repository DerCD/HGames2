package com.dercd.bukkit.plugins.hgames.map.loading;

import java.util.function.Consumer;

import org.bukkit.Bukkit;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;

public abstract class MapLoadingTask
{
	protected boolean running;
	protected boolean finished;
	protected Consumer<Map> onFinished;
	protected Thread bukkitThread;
	protected boolean interrupted;
	protected Runnable onInterrupted;

	public abstract void runTask(MapInfo mapInfo, Map map);
	protected void runAsync(Runnable r)
	{
		if(this.bukkitThread == null || Thread.currentThread() == this.bukkitThread)
			new Thread(r).start();
		else
			r.run();
	}
	protected void runSync(Runnable r)
	{
		if(this.bukkitThread != null && Thread.currentThread() == this.bukkitThread)
			r.run();
		else
			Bukkit.getScheduler().runTask(CDAPI.getInstance(), r);
	}
	protected void onFinished()
	{
		onFinished(null);
	}
	protected void onFinished(Map map)
	{
		if(this.onFinished == null)
			return;
		this.onFinished.accept(map);
	}
	public void interrupt()
	{
		this.interrupted = true;
	}
	protected void onInterrupted()
	{
		if(this.onInterrupted == null)
			return;
		this.onInterrupted.run();
	}
	
	public boolean isRunning()
	{
		return this.running;
	}
	public boolean isFinished()
	{
		return this.finished;
	}
	public Consumer<Map> getOnFinished()
	{
		return this.onFinished;
	}
	public Thread getBukkitThread()
	{
		return this.getBukkitThread();
	}
	public boolean isInterrupted()
	{
		return this.interrupted;
	}
	public Runnable getOnInterruped()
	{
		return this.onInterrupted;
	}
	
	public void setOnFinished(Consumer<Map> onFinished)
	{
		this.onFinished = onFinished;
	}
	public void setBukkitThread(Thread bukkitThread)
	{
		this.bukkitThread = bukkitThread;
	}
	public void setOnInterrupted(Runnable onInterrupted)
	{
		this.onInterrupted = onInterrupted;
	}
}
