package com.dercd.bukkit.plugins.hgames.map.loading;

import org.bukkit.Difficulty;
import org.bukkit.World;

import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;

public class DMWorldSettingsTask extends MapLoadingTask
{
	@Override
	public void runTask(MapInfo mapInfo, Map map)
	{
		runSync(() ->
		{
			World w = map.getWorld();
			w.setGameRuleValue("commandBlockOutput", "false");
			w.setGameRuleValue("doTileDrops", "false");
			w.setGameRuleValue("doFireTick", "true");
			w.setGameRuleValue("doMobSpawning", "false");
			w.setDifficulty(Difficulty.HARD);
			onFinished(map);
		});
	}
}
