package com.dercd.bukkit.plugins.hgames.map.loading;

import java.util.ArrayList;
import java.util.function.Consumer;

import org.bukkit.Bukkit;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;

public class MapLoader
{
	protected MapInfo mapInfo;
	protected boolean running;
	protected ArrayList<MapLoadingTask> tasks;
	protected MapLoadingTask currentTask;
	protected Consumer<Map> onFinished;
	protected Runnable onInterrupted = () -> { onInterrupted(); };
	protected Runnable afterInterrupted;
	protected boolean interrupted;
	
	public void startMapLoading()
	{
		if(this.mapInfo == null || this.tasks == null)
			return;
		if(this.tasks.size() == 0)
			callOnFinished(null);
		this.running = true;
		startNextTask(null, 0);
	}
	protected void startNextTask(Map map, int task)
	{
		if(isInterrupted())
		{
			onInterrupted();
			return;
		}
		if(task >= this.tasks.size())
		{
			callOnFinished(map);
			return;
		}
		
		MapLoadingTask currentTask = this.tasks.get(task);
		synchronized(this)
		{
			this.currentTask = currentTask;
			if(isInterrupted())
			{
				onInterrupted();
				return;
			}
		}
		currentTask.setOnFinished((inputMap) ->
		{
			startNextTask(inputMap, task + 1);
		});
		System.out.println("Running task " + currentTask.getClass().getName());
		currentTask.runTask(this.mapInfo, map);
	}
	protected void callOnFinished(Map map)
	{
		System.out.println("Map loaded");
		this.running = false;
		if(this.onFinished == null)
			return;
		this.onFinished.accept(map);
	}

	public synchronized void interrupt()
	{
		this.interrupted = true;
		this.currentTask.interrupt();
	}
	public synchronized void interrupt(boolean wait)
	{
		if(this.isRunning())
			return;
		this.afterInterrupted = () -> { this.notifyAll(); };
		interrupt();
		if(wait)
			try
			{
				this.wait();
			}
			catch(InterruptedException x)
			{ }
	}
	public synchronized void interrupt(Runnable callback)
	{
		this.afterInterrupted = () ->
		{
			Bukkit.getScheduler().runTask(CDAPI.getInstance(), () ->
			{
				if(callback != null)
					callback.run();
			});
		};
		if(!this.isRunning())
		{
			this.afterInterrupted.run();
			return;
		}
		interrupt();
	}
	protected synchronized void onInterrupted()
	{
		this.running = false;
		if(this.afterInterrupted != null)
			this.afterInterrupted.run();
	}
	
	public MapInfo getMapInfo()
	{
		return this.mapInfo;
	}
	public boolean isRunning()
	{
		return this.running;
	}
	public ArrayList<MapLoadingTask> getTasks()
	{
		return this.tasks;
	}
	public MapLoadingTask getCurrentTask()
	{
		return this.currentTask;
	}
	public Consumer<Map> getOnFinished()
	{
		return this.onFinished;
	}
	public boolean isInterrupted()
	{
		return this.interrupted;
	}
	
	public void setMapInfo(MapInfo mapInfo)
	{
		this.mapInfo = mapInfo;
	}
	public void addTask(MapLoadingTask task)
	{
		if(this.tasks == null)
			this.tasks = new ArrayList<MapLoadingTask>();
		if(task == null)
			return;
		task.setOnInterrupted(this.onInterrupted);
		this.tasks.add(task);
	}
	public void setTasks(ArrayList<MapLoadingTask> tasks)
	{
		this.tasks = tasks;
	}
	public void setCurrentTask(MapLoadingTask currentTask)
	{
		this.currentTask = currentTask;
	}
	public void setOnFinished(Consumer<Map> onFinished)
	{
		this.onFinished = onFinished;
	}
}
