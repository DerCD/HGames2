package com.dercd.bukkit.plugins.hgames.map.loading;

import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;
import com.dercd.bukkit.plugins.hgames.map.Map;

public class MapLoadingWaitManager
{
	boolean waiting;
	BukkitTask waitingDisplayTask;
	GameState state;
	
	protected Object mapLoadingLock = new Object();
	
	public MapLoadingWaitManager(GameState state)
	{
		this.state = state;
	}
	
	public GameState onApplyNextState(Map map)
	{
		synchronized(this.mapLoadingLock)
		{
			//Map isn't loaded yet
			//Don't apply next state
			if(map == null)
			{
				this.waiting = true;
				displayWaitMessage();
				return this.state;
			}
		}
		return null;
	}
	public void onMapLoaded(Map map)
	{
		Game game = Game.getInstance();
		
		synchronized(this.mapLoadingLock)
		{
			//Next state should be applied already
			//Applying it again
			if(this.waiting)
			{
				stopWaitMessage();
				if(this.state != null)
					game.moveToNextStateFrom(this.state.getClass());
				else
					game.moveToNextState();
			}
		}
	}
	
	public void displayWaitMessage()
	{
		Game game = Game.getInstance();
		
		if(this.waitingDisplayTask != null)
			this.waitingDisplayTask.cancel();
		this.waitingDisplayTask = game.createMapLoadingWaitingDisplay();
	}
	public void stopWaitMessage()
	{
		if(this.waitingDisplayTask != null)
		{
			this.waitingDisplayTask.cancel();
			this.waitingDisplayTask = null;
		}
	}
}
