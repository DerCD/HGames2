package com.dercd.bukkit.plugins.hgames.map.loading;

import java.util.Iterator;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.ObjHolder;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.BlockPosition;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;
import com.dercd.bukkit.plugins.hgames.map.chest.Chest;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestCollection;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestManager;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestPool;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;

public class HGChestPlaceTask extends MapLoadingTask
{
	public static final double CHEST_PLACE_PERIOD = 0.4; //Seconds
	public static final int CHESTS_PER_ROUND = 10;
	
	@Override
	public void runTask(MapInfo mapInfo, Map map)
	{
		runAsync(() ->
		{
			HGMap hgMap = (HGMap)map;
			ChestManager chestManager = map.getChestManager();
			ChestPool chestPool = chestManager.getChestPool();
			int chestsToPlace = hgMap.getChestsToPlace();
			int minChestDistance = hgMap.getMinChestDistance();
			
			ChestCollection chestLocs = chestPool.getRandomChests(chestsToPlace, minChestDistance, true);
			if(chestLocs == null || chestLocs.isEmpty())
			{
				onFinished(map);
				return;
			}
			ChestCollection staticChestLocs = chestPool.getStaticChests();
			
			if(isInterrupted())
			{
				onInterrupted();
				return;
			}
			
			synchronized(chestManager)
			{
				startBukkitTask(chestManager, chestLocs.iterator(), () ->
				{
					if(staticChestLocs != null && !staticChestLocs.isEmpty())
					{
						startBukkitTask(chestManager, staticChestLocs.iterator(), () ->
						{
							onFinished(map);
						}, true);
					}
					else
					{
						onFinished(map);
					}
				}, false);
			}
		});
	}
	
	protected BukkitTask startBukkitTask(ChestManager chestManager, Iterator<BlockPosition> iterChestLocs, Runnable onFinished, boolean staticChests)
	{
		ObjHolder<BukkitTask> bukkitTask = new ObjHolder<BukkitTask>();
		return bukkitTask.obj = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			if(isInterrupted())
			{
				stopBukkitTask(bukkitTask, chestManager);
				onInterrupted();
				return;
			}
			
			for(int i = 0; i < CHESTS_PER_ROUND && iterChestLocs.hasNext(); ++i)
			{
				Chest chest = chestManager.placeChest(iterChestLocs.next(), false);
				if(staticChests)
				{
					if(chest.isCreepy())
					{
						chest.setCreepy(false);
						chest.init();
					}
				}
			}
			if(!iterChestLocs.hasNext())
			{
				stopBukkitTask(bukkitTask, chestManager);
				onFinished.run();
				return;
			}
			
			if(isInterrupted())
			{
				stopBukkitTask(bukkitTask, chestManager);
				onInterrupted();
				return;
			}
		}, 0, (long)(CHEST_PLACE_PERIOD * Tools.TICKS_PER_SECOND));
	}
	
	private void stopBukkitTask(ObjHolder<BukkitTask> objHolder, Object lock)
	{
		synchronized(lock)
		{
			objHolder.obj.cancel();
		}
	}
}
