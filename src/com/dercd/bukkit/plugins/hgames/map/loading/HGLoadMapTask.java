package com.dercd.bukkit.plugins.hgames.map.loading;

import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.IIOManager;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapInfo;

public class HGLoadMapTask extends MapLoadingTask
{
	@Override
	public void runTask(MapInfo mapInfo, Map map)
	{
		runAsync(() ->
		{
			IIOManager ioManager = Game.getInstance().getIOManager();
			Map outputMap = ioManager.loadHGMap((HGMapInfo)mapInfo, false);
			onFinished(outputMap);
		});
	}
}
