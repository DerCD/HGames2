package com.dercd.bukkit.plugins.hgames.map.loading;

public class HGMapLoader extends MapLoader
{
	public static final int MAP_LOADING_BUFFER = 15;
	
	public void createTasks()
	{
		//Hoping current thread is bukkit main thread
		//If not, we have a problem...
		Thread bukkitThread = Thread.currentThread();
		
		addTask(new HGCopyWorldTask(), bukkitThread);
		addTask(new HGLoadWorldTask(), bukkitThread);
		addTask(new HGLoadMapTask(), bukkitThread);
		addTask(new HGWorldSettingsTask(), bukkitThread);
		addTask(new HGChestPlaceTask(), bukkitThread);
	}
	public void addTask(MapLoadingTask task, Thread bukkitThread)
	{
		task.setBukkitThread(bukkitThread);
		addTask(task);
	}
}
