package com.dercd.bukkit.plugins.hgames.map;

import java.util.ArrayList;
import com.dercd.bukkit.cdapi.tools.Tools;

import org.bukkit.Location;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.BlockPosition;

public class Fountain
{
	Location location;
	int height;
	int marginSleepTime;
	ArrayList<Thread> threads;
	boolean running;
	
	public static final int DEFAULT_MARGIN_SLEEP_TIME = 35;
	public static final int DEFAULT_PERIOD = 650;
	public static final int DEFAULT_SHOTS_COUNT = 7;
	
	public Fountain(Location l)
	{
		this(l, -1);
	}
	public Fountain(Location l, int height)
	{
		this(l, height, DEFAULT_MARGIN_SLEEP_TIME);
	}
	public Fountain(Location l, int height, int marginSleepTime)
	{
		this.location = l;
		this.height = height;
		this.marginSleepTime = marginSleepTime;
	}
	
	public void shootOnce()
	{
		int x = this.location.getBlockX();
		int y = this.location.getBlockY();
		int z = this.location.getBlockZ();
		int marginSleepTime = this.marginSleepTime;
		int height = this.height;
		
		if(height < 0)
			height = this.location.getWorld().getMaxHeight();
		if(height <= y)
			return;
		
		PacketContainer pc = new PacketContainer(PacketType.Play.Server.WORLD_EVENT);
		StructureModifier<Integer> integers = pc.getIntegers();
		StructureModifier<Boolean> booleans = pc.getBooleans();
		StructureModifier<BlockPosition> blockPositions = pc.getBlockPositionModifier();
		
		integers.write(0, 2001); //Magic numbers suck
		integers.write(1, 152);		
		booleans.write(0, false);
		
		try
		{	
			for(int h = 0; h <= height; h++)
			{
				blockPositions.write(0, new BlockPosition(x, h + y, z));
				Tools.send(pc);
				Thread.sleep(10);
				Tools.send(pc);
				Thread.sleep(marginSleepTime);
			}
		}
		catch (InterruptedException ex) { }
		synchronized(this)
		{
			this.threads.remove(Thread.currentThread());
			if(this.threads.isEmpty())
				this.running = false;
		}
	}
	
	public void shoot()
	{
		shoot(DEFAULT_SHOTS_COUNT);
	}
	public void shoot(int times)
	{
		shoot(times, DEFAULT_PERIOD);
	}
	public void shoot(int times, int period)
	{
		new Thread(() ->
		{
			startShootingThreads(times, period);
		}).start();
	}
	public void startShootingThreads(int times, int period)
	{
		synchronized(this)
		{
			if(this.isRunning())
				return;
			this.running = true;
		}
		this.threads = new ArrayList<Thread>();
		for(int i = 0; i < times; ++i)
		{
			Thread t = new Thread(() ->
			{
				shootOnce();
			});
			this.threads.add(t);
		}
		for(Thread t : this.threads)
		{
			t.start();
			try
			{
				Thread.sleep(period);
			}
			catch(InterruptedException e)
			{ }
		}
	}
	
	public boolean isRunning()
	{
		return this.running;
	}
}
