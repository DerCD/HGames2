package com.dercd.bukkit.plugins.hgames.map;

import java.util.ArrayList;
import java.util.Iterator;

import com.dercd.bukkit.cdapi.tools.collection.CircularIterator;

public class HGMapRefillTimes
{
	protected int[] baseData;
	protected Iterator<Integer> timesIterator;
	protected Iterator<Integer> endTimesIterator;
	protected int currentTime = -1;
	
	public int getNextRefillTime()
	{
		if(this.timesIterator == null)
			return -1;
		if(!this.timesIterator.hasNext())
		{
			if(this.endTimesIterator == null || !this.endTimesIterator.hasNext())
				return -1;
			return this.currentTime = this.endTimesIterator.next();
		}
		return this.currentTime = this.timesIterator.next();
	}
	public int getCurrentRefillTime()
	{
		return this.currentTime;
	}
	
	public void generateIterators()
	{
		if(this.baseData == null)
		{
			this.timesIterator = null;
			this.endTimesIterator = null;
			return;
		}
		ArrayList<Integer> regularTimes = new ArrayList<Integer>();
		ArrayList<Integer> endTimes = null;
		int loop = 0;
		for(int time : this.baseData)
		{
			if(time == 0)
				continue;
			if(time > 0)
				regularTimes.add(time);
			else
			{
				loop = Math.abs(time);
				break;
			}
		}
		
		loop = Math.min(loop, regularTimes.size());
		if(loop > 0)
		{
			endTimes = new ArrayList<Integer>();
			for(int i = regularTimes.size() - loop; i < regularTimes.size(); ++i)
				endTimes.add(i);
			
		}
		
		this.timesIterator = regularTimes.iterator();
		if(endTimes != null)
			this.endTimesIterator = new CircularIterator<Integer>(endTimes);
	}
	
	public int[] getBaseData()
	{
		return this.baseData;
	}
	public Iterator<Integer> getTimesIterator()
	{
		return this.timesIterator;
	}
	public Iterator<Integer> getEndTimesIterator()
	{
		return this.endTimesIterator;
	}
	public int getCurrentTime()
	{
		return this.currentTime;
	}
	public void setBaseData(int[] baseData)
	{
		this.baseData = baseData;
		generateIterators();
	}
	public void setTimesIterator(Iterator<Integer> timesIterator)
	{
		this.timesIterator = timesIterator;
	}
	public void setEndTimesIterator(Iterator<Integer> endTimesIterator)
	{
		this.endTimesIterator = endTimesIterator;
	}
	public void setCurrentTime(int currentTime)
	{
		this.currentTime = currentTime;
	}
}
