package com.dercd.bukkit.plugins.hgames.map;

import org.bukkit.World;

import com.dercd.bukkit.plugins.hgames.map.chest.ChestManager;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public abstract class Map
{
	protected MapInfo mapInfo;
	protected World world;
	protected SpawnPool spawnPool;
	protected ItemPool itemPool;
	protected ChestManager chestManager;
	
	public MapInfo getMapInfo()
	{
		return this.mapInfo;
	}
	public World getWorld()
	{
		return this.world;
	}
	public SpawnPool getSpawnPool()
	{
		return this.spawnPool;
	}
	public ItemPool getItemPool()
	{
		return this.itemPool;
	}
	public ChestManager getChestManager()
	{
		return this.chestManager;
	}
	
	public void setMapInfo(MapInfo mapInfo)
	{
		this.mapInfo = mapInfo;
	}
	public void setWorld(World world)
	{
		this.world = world;
	}
	public void setSpawnPool(SpawnPool spawnPool)
	{
		this.spawnPool = spawnPool;
	}
	public void setItemPool(ItemPool itemPool)
	{
		this.itemPool = itemPool;
	}
	public void setChestManager(ChestManager chestManager)
	{
		this.chestManager = chestManager;
	}
}
