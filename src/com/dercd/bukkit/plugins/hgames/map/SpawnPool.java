package com.dercd.bukkit.plugins.hgames.map;

import java.util.ArrayList;

import org.bukkit.Location;

public class SpawnPool
{
	public ArrayList<Location> spawns;
	
	public SpawnPool()
	{
		this.spawns = new ArrayList<Location>();
	}
	
	public ArrayList<Location> getSpawns(int players)
	{
		int spawnCount = getSpawnCount();
		if(players == spawnCount)
			return getAllSpawns();
		if(players > spawnCount)
			return getOverloadedSpawns(players);
		else
			return getDistributedSpawns(players);
	}
	
	public ArrayList<Location> getAllSpawns()
	{
		return (ArrayList<Location>) this.spawns.clone();
	}
	public ArrayList<Location> getDistributedSpawns(int players)
	{
		ArrayList<Location> spawns = new ArrayList<Location>();
		int spawnCount = getSpawnCount();
		double distance = spawnCount / (players * 1D);
		for(int i = 0; i < players; ++i)
		{
			double currentPosition = i * distance;
			//Adding 0.5 to create real rounding
			int spawnId = (int)(currentPosition + 0.5);
			spawns.add(this.spawns.get(spawnId));
		}
		return spawns;
	}
	public ArrayList<Location> getOverloadedSpawns(int players)
	{
		ArrayList<Location> spawns = new ArrayList<Location>();
		int spawnCount = getSpawnCount();
		for(int i = 0; i < players; ++i)
		{
			int spawnId = i % spawnCount;
			spawns.add(this.spawns.get(spawnId));
		}
		return spawns;
	}
	
	public Location getFirstSpawn()
	{
		return this.spawns.get(0);
	}
	
	public int getSpawnCount()
	{
		return this.spawns.size();
	}

	
	public void addSpawn(Location loc)
	{
		if(this.spawns == null)
			this.spawns = new ArrayList<Location>();
		this.spawns.add(loc);
	}
	public ArrayList<Location> getSpawns()
	{
		return this.spawns;
	}

	public void setSpawns(ArrayList<Location> spawns)
	{
		this.spawns = spawns;
	}
}
