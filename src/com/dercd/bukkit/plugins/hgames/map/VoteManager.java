package com.dercd.bukkit.plugins.hgames.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.bukkit.entity.Player;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;

public class VoteManager<T>
{
	HashMap<Integer, T> maps;
	HashMap<Player, Integer> votes;
	protected boolean parallelVoteCounting;
	protected Thread lastVoteCountingThread;
	protected T computedResult;
	
	public VoteManager()
	{
		this.maps = new HashMap<Integer, T>();
		this.votes = new HashMap<Player, Integer>();
	}
	
	public void putEntry(int id, T val)
	{
		this.maps.put(id, val);
	}
	public HashMap<Integer, T> getEntries()
	{
		return new HashMap<Integer, T>(this.maps);
	}
	public ArrayList<VoteEntry<T>> getEntriesSorted()
	{
		ArrayList<Entry<Integer, T>> entryList = new ArrayList<Entry<Integer, T>>(this.maps.entrySet());
		entryList.sort((a, b) -> a.getKey() - b.getKey());
		ArrayList<VoteEntry<T>> sorted = new ArrayList<VoteEntry<T>>();
		for(Entry<Integer, T> entry : entryList)
			sorted.add(new VoteEntry<T>(entry.getKey(), entry.getValue()));
		return sorted;
	}
	
	
	public void vote(Player p, int map)
	{
		this.votes.put(p, map);
		
		synchronized(this)
		{
			if(this.parallelVoteCounting)
				startParallelVotingCountThread();
		}
	}
	
	protected void startParallelVotingCountThread()
	{
		Thread t = new Thread(() -> { preComputeResult(); });
		this.lastVoteCountingThread = t;
		t.start();
	}
	protected void preComputeResult()
	{
		T computedResult = computeVoteResult();
		synchronized(this)
		{
			if(this.lastVoteCountingThread == Thread.currentThread())
			{
				this.computedResult = computedResult;
				this.lastVoteCountingThread = null;
				this.notifyAll();
			}
		}
	}
	protected T computeVoteResult()
	{
		//Key: VoteID, Value: votes
		HashMap<Player, Integer> votes = new HashMap<Player, Integer>(this.votes);
		HashMap<Integer, Integer> voteEvaluation = new HashMap<Integer, Integer>();
		for(Integer voteId : this.maps.keySet())
			voteEvaluation.put(voteId, 0);
		for(Entry<Player, Integer> entry : votes.entrySet())
		{
			Integer votesCnt = voteEvaluation.get(entry.getValue());
			voteEvaluation.put(entry.getValue(), votesCnt + 1);
		}
		int max = -1;
		for(Entry<Integer, Integer> e : voteEvaluation.entrySet())
			if(max == -1 || e.getValue() > max)
				max = e.getValue();
		
		ArrayList<T> highestVoted = new ArrayList<T>();
		for(Entry<Integer, Integer> e : voteEvaluation.entrySet())
		{
			if(e.getValue() == max)
			{
				T val = this.maps.get(e.getKey());
				highestVoted.add(val);
			}
		}
		if(highestVoted.size() == 1)
			return highestVoted.get(0);
		int rnd = Tools.newPositiveRandomInt() % highestVoted.size();
		return highestVoted.get(rnd);
	}
	public T getVoteResult()
	{
		synchronized(this)
		{
			if(this.parallelVoteCounting)
			{
				if(this.lastVoteCountingThread != null)
				{
					try
					{
						this.wait();
					}
					catch(InterruptedException x)
					{
						CDAPI.getInstance().getHandler().getCLog().printException(x);
					}
				}
				return this.computedResult;
			}
		}
		return computeVoteResult();
	}
	
	public int getMapCount()
	{
		return this.maps.size();
	}
	
	public Integer getVoteOf(Player p)
	{
		return this.votes.get(p);
	}
	
	public void enableParallelVoteCounting()
	{
		synchronized(this)
		{
			this.parallelVoteCounting = true;
			startParallelVotingCountThread();
		}
	}
	public void disableParallelVoteCounting()
	{
		synchronized(this)
		{
			this.parallelVoteCounting = false;
		}
	}
}
