package com.dercd.bukkit.plugins.hgames.map;

public abstract class MapInfo
{
	protected String name;
	protected String path;
	protected int minPlayers;
	protected int maxPlayers;
	protected String creator;
	
	
	public String getName()
	{
		return this.name;
	}
	public String getPath()
	{
		return this.path;
	}
	public int getMinPlayers()
	{
		return this.minPlayers;
	}
	public int getMaxPlayers()
	{
		return this.maxPlayers;
	}
	public String getCreator()
	{
		return this.creator;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	public void setPath(String path)
	{
		this.path = path;
	}
	public void setMinPlayers(int minPlayers)
	{
		this.minPlayers = minPlayers;
	}
	public void setMaxPlayers(int maxPlayers)
	{
		this.maxPlayers = maxPlayers;
	}
	public void setCreator(String creator)
	{
		this.creator = creator;
	}
}
