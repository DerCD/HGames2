package com.dercd.bukkit.plugins.hgames.map;

public class VoteEntry<T>
{
	protected int voteId;
	protected T voteValue;
	
	public VoteEntry()
	{ }
	public VoteEntry(int voteId, T voteValue)
	{
		this.voteId = voteId;
		this.voteValue = voteValue;
	}
	
	public int getVoteId()
	{
		return this.voteId;
	}
	public T getVoteValue()
	{
		return this.voteValue;
	}
	public void setVoteId(int voteId)
	{
		this.voteId = voteId;
	}
	public void setVoteValue(T voteValue)
	{
		this.voteValue = voteValue;
	}
}
