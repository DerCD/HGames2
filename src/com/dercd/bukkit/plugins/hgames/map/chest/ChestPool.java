package com.dercd.bukkit.plugins.hgames.map.chest;

import java.util.ArrayList;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.BlockPosition;

public class ChestPool
{
	protected ChestCollection chests;
	protected ChestCollection staticChests;
	
	public ChestPool()
	{
		this.chests = new ChestCollection();
	}
	
	public ChestCollection getRandomChests(int count, int minDistance)
	{
		return getRandomChests(count, minDistance, false);
	}
	public ChestCollection getRandomChests(int count, int minDistance, boolean allowDistanceBoundsBreaking)
	{
		if(this.chests == null)
			return null;
		count = Math.min(count, this.chests.size());
		
		ChestCollection pool = new ChestCollection(this.chests);
		ChestCollection toNear = new ChestCollection();
		
		ChestCollection selected = new ChestCollection();
		
		int i = 0;
		while(i < count && !pool.isEmpty())
		{
			int rnd = Tools.newPositiveRandomInt() % pool.size();
			BlockPosition loc = pool.remove(rnd);
			if(checkDistance(loc, selected, minDistance))
			{
				selected.add(loc);
				++i;
			}
			else
			{
				toNear.add(loc);
			}
		}
		
		//If we reached the requested amount
		//or if we didn't reached it but aren't allowed to break
		//the distance bounds, return the result
		if(i >= count || !allowDistanceBoundsBreaking)
			return selected;
		
		//If we reach this code, we didn't manage to generated the requested amount of chests
		//and satisfying the minDistance
		//So we have to take the rest out of the toNear-pool
		//We simply take the elements as they appear in the list as they are already randomized
		//Also we don't have to check whether toNear has enough elements
		//because count <= pool, toNear = pool - selected and count <= pool - selected
		for(; i < count; ++i)
			selected.add(toNear.remove(0));
		
		return selected;
	}
	
	protected boolean checkDistance(BlockPosition loc, ChestCollection selected, int minDistance)
	{
		for(BlockPosition l : selected)
			if((loc.distance(l) + 0.5) < minDistance)
				return false;
		return true;
	}

	
	public void addChest(BlockPosition loc)
	{
		if(this.chests == null)
			this.chests = new ChestCollection();
		this.chests.add(loc);
	}
	public void addStaticChest(BlockPosition loc)
	{
		if(this.staticChests == null)
			this.staticChests = new ChestCollection();
		this.staticChests.add(loc);
	}
	
	public ChestCollection getChests()
	{
		return this.chests;
	}
	public ChestCollection getStaticChests()
	{
		return this.staticChests;
	}

	public void setChests(ArrayList<BlockPosition> chests)
	{
		this.chests = new ChestCollection(chests);
	}
	public void setChests(ChestCollection chests)
	{
		this.chests = chests;
	}
	public void setStaticChests(ArrayList<BlockPosition> chests)
	{
		this.chests = new ChestCollection(chests);
	}
	public void setStaticChests(ChestCollection staticChests)
	{
		this.staticChests = staticChests;
	}
}
