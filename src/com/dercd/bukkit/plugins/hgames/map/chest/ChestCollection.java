package com.dercd.bukkit.plugins.hgames.map.chest;

import java.util.ArrayList;
import java.util.Collection;
import com.dercd.bukkit.cdapi.tools.minecraft.BlockPosition;

public class ChestCollection extends ArrayList<BlockPosition>
{
	private static final long serialVersionUID = 7853105388692739503L;
	
	public ChestCollection()
	{ }
	public ChestCollection(Collection<BlockPosition> collection)
	{
		super(collection);
	}
}
