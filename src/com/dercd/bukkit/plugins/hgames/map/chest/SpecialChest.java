package com.dercd.bukkit.plugins.hgames.map.chest;

import org.bukkit.block.Block;

import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public class SpecialChest extends Chest
{
	public SpecialChest(Block location, HGMap map)
	{
		this(location, map.getSpecialItemPool());
	}
	public SpecialChest(Block location, ItemPool itemPool)
	{
		super(location, itemPool);
	}
	public SpecialChest(Block location, ItemPool itemPool, Boolean creepyChest)
	{
		super(location, itemPool);
	}
}
