package com.dercd.bukkit.plugins.hgames.map.chest;

import org.bukkit.block.Block;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public class Chest
{
	protected ItemPool itemPool;
	protected ChestContent content;
	
	protected Block location;
	
	protected boolean isCreepy;
	
	public static final double CREEPY_CHANCE = 0.02;
	
	public Chest(Block location, Map map)
	{
		this(location, map.getItemPool());
	}
	public Chest(Block location, ItemPool itemPool)
	{
		this(location, itemPool, null);
	}
	public Chest(Block location, Map map, Boolean creepyChest)
	{
		this(location, map.getItemPool(), creepyChest);
	}
	public Chest(Block location, ItemPool itemPool, Boolean creepyChest)
	{
		this.location = location;
		this.itemPool = itemPool;
		
		if(creepyChest != null)
			this.isCreepy = creepyChest;
		else
			this.isCreepy = genCreepy();
		
		init();
	}
	
	public void init()
	{		
		//Creepy chests don't need a content, so we can stop init here
		if(this.isCreepy)
			return;
		
		this.content = new ChestContent(this.itemPool);
	}
	
	
	public ItemPool getItemPool()
	{
		return this.itemPool;
	}
	public ChestContent getContent()
	{
		return this.content;
	}
	public Block getLocation()
	{
		return this.location;
	}
	public boolean isCreepy()
	{
		return this.isCreepy;
	}
	public void setItemPool(ItemPool itemPool)
	{
		this.itemPool = itemPool;
	}
	public void setContent(ChestContent chestContent)
	{
		this.content = chestContent;
	}
	public void setLocation(Block location)
	{
		this.location = location;
	}
	public void setCreepy(boolean creepyChest)
	{
		this.isCreepy = creepyChest;
	}
	
	public static boolean genCreepy()
	{
		return (Tools.newPositiveRandomInt() % 100000) / 100000D < CREEPY_CHANCE;
	}
}
