package com.dercd.bukkit.plugins.hgames.map.chest;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.items.ItemPool;

public class ChestContent
{
	public Inventory inventory;
	protected ItemPool itemPool;
	
	protected static ArrayList<Integer> positions;
	protected static int inventorySize;
	
	static
	{
		inventorySize = InventoryType.CHEST.getDefaultSize();
		
		positions = new ArrayList<Integer>();
		for(int i = 0; i < inventorySize; ++i)
			positions.add(i);
	}
	
	public ChestContent(HGMap map)
	{
		this(map.getItemPool());
	}
	public ChestContent(ItemPool itemPool)
	{
		this.itemPool = itemPool;
	}
	
	public void generate()
	{
		Inventory inv = Bukkit.createInventory(null, InventoryType.CHEST);
		int itemCount = generateInventoryItemCount();
		
		ArrayList<Integer> posPool = (ArrayList<Integer>) ChestContent.positions.clone();
		ItemPool itemPool = this.itemPool;
		for(int i = 0; i < itemCount; ++i)
		{
			int rnd = Tools.newPositiveRandomInt() % posPool.size();
			int pos = posPool.remove(rnd);
			ItemStack item = itemPool.nextItem();
			
			inv.setItem(pos, item);
		}
		this.inventory = inv;
	}
	
	public int generateInventoryItemCount()
	{
		int min = this.itemPool.getMinItems();
		int max = this.itemPool.getMaxItems();
		
		int rnd = Tools.newPositiveRandomInt();
		rnd = rnd % (max - min + 1);
		rnd += min;
		return rnd;
	}
	
	
	public Inventory getInventory()
	{
		if(this.inventory == null)
			generate();
		return this.inventory;
	}
	public ItemPool getItemPool()
	{
		return this.itemPool;
	}
	public static ArrayList<Integer> getPositions()
	{
		return positions;
	}
	public static int getInventorySize()
	{
		return inventorySize;
	}
	
	public void setInventory(Inventory inventory)
	{
		this.inventory = inventory;
	}
	public void setItemPool(ItemPool itemPool)
	{
		this.itemPool = itemPool;
	}
	public static void setPositions(ArrayList<Integer> positions)
	{
		ChestContent.positions = positions;
	}
	public static void setInventorySize(int inventorySize)
	{
		ChestContent.inventorySize = inventorySize;
	}
}
