package com.dercd.bukkit.plugins.hgames.map.chest;

import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.BlockPosition;
import com.dercd.bukkit.plugins.hgames.map.Map;

import net.minecraft.server.v1_11_R1.Blocks;

public class ChestManager
{
	protected Map map;
	protected net.minecraft.server.v1_11_R1.World mcWorld;
	ChestMap chestMap;
	protected ChestPool chestPool;
	
	public ChestManager(Map map)
	{
		setMap(map);
	}
	
	public Chest placeChest(BlockPosition location)
	{
		return placeChest(location, false);
	}
	public Chest placeChest(BlockPosition location, boolean generateContent)
	{
		return placeChest(location, generateContent, null);
	}
	public Chest placeChest(Chest chest, boolean generateContent)
	{
		return placeChest(null, generateContent, chest);
	}
	public Chest placeChest(BlockPosition location, boolean generateContent, Chest chest)
	{
		Block block;
		if(chest == null)
		{
			block = location.toBlock();
			chest = new Chest(block, this.map);
		}
		else
		{
			block = chest.getLocation();
			location = BlockPosition.from(block);
		}
		if(generateContent)
			chest.getContent().generate();
		if(this.chestMap == null)
			this.chestMap = new ChestMap();
		this.chestMap.put(block, chest);
		if(this.mcWorld != null)
			Tools.setBlockWithoutUpdate(location.x, location.y, location.z, Blocks.CHEST, this.mcWorld);
		return chest;
	}
	public Chest getChest(Block location)
	{
		if(this.chestMap == null)
			return null;
		return this.chestMap.get(location);
	}
	
	public Map getMap()
	{
		return this.map;
	}
	public ChestMap getChestMap()
	{
		return this.chestMap;
	}
	public ChestPool getChestPool()
	{
		return this.chestPool;
	}
	
	public void setMap(Map map)
	{
		this.map = map;
		if(map != null)
			this.mcWorld = ((CraftWorld)map.getWorld()).getHandle();
	}
	public void setChestMap(ChestMap chestMap)
	{
		this.chestMap = chestMap;
	}
	public void setChestPool(ChestPool chestPool)
	{
		this.chestPool = chestPool;
	}
}
