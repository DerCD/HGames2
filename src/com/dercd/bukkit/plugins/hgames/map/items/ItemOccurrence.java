package com.dercd.bukkit.plugins.hgames.map.items;

import org.bukkit.inventory.ItemStack;

public class ItemOccurrence
{
	protected ItemStack item;
	protected int occurrence;
	
	public ItemOccurrence()
	{ }
	public ItemOccurrence(ItemStack item, int occurrence)
	{
		this.item = item;
		this.occurrence = occurrence;
	}
	
	public ItemStack getItem()
	{
		return this.item;
	}
	public int getOccurrence()
	{
		return this.occurrence;
	}
	
	public void setItem(ItemStack item)
	{
		this.item = item;
	}
	public void setOccurrence(int occurrence)
	{
		this.occurrence = occurrence;
	}
}
