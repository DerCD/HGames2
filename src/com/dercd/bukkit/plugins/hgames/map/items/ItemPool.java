package com.dercd.bukkit.plugins.hgames.map.items;

import java.util.ArrayList;
import org.bukkit.inventory.ItemStack;

import com.dercd.bukkit.cdapi.tools.Tools;

public class ItemPool
{
	protected ArrayList<ItemOccurrence> itemOccurrences;
	protected ArrayList<ItemStack> pool;
	protected int minItems;
	protected int maxItems;
	
	public ItemPool()
	{
		this.itemOccurrences = new ArrayList<ItemOccurrence>();
	}
	
	public void generatePool()
	{
		this.pool = new ArrayList<ItemStack>();
		for(ItemOccurrence occurrence : this.itemOccurrences)
			for(int i = 0; i < occurrence.occurrence; ++i)
				this.pool.add(occurrence.item);
	}
	
	public ItemStack nextItem()
	{
		int rnd = Tools.newPositiveRandomInt() % this.pool.size();
		return this.pool.get(rnd).clone();
	}

	public ArrayList<ItemOccurrence> getItemOccurrences()
	{
		return this.itemOccurrences;
	}
	public ArrayList<ItemStack> getPool()
	{
		return this.pool;
	}
	public int getMinItems()
	{
		return this.minItems;
	}
	public int getMaxItems()
	{
		return this.maxItems;
	}

	public void addItemOccurrence(ItemOccurrence itemOccurrence)
	{
		if(this.itemOccurrences == null)
			this.itemOccurrences = new ArrayList<ItemOccurrence>();
		this.itemOccurrences.add(itemOccurrence);
	}
	public void setItemOccurrences(ArrayList<ItemOccurrence> itemOccurrences)
	{
		this.itemOccurrences = itemOccurrences;
	}
	public void setPool(ArrayList<ItemStack> pool)
	{
		this.pool = pool;
	}
	public void setMinItems(int minItems)
	{
		this.minItems = minItems;
	}
	public void setMaxItems(int maxItems)
	{
		this.maxItems = maxItems;
	}
}
