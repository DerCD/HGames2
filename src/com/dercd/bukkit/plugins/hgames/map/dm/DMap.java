package com.dercd.bukkit.plugins.hgames.map.dm;

import com.dercd.bukkit.plugins.hgames.map.Map;

public class DMap extends Map
{
	protected DMapTimes mapTimes;
	
	
	public DMapTimes getMapTimes()
	{
		return this.mapTimes;
	}

	public void setMapTimes(DMapTimes mapTimes)
	{
		this.mapTimes = mapTimes;
	}
}
