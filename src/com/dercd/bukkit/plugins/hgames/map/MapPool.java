package com.dercd.bukkit.plugins.hgames.map;

import java.util.ArrayList;
import com.dercd.bukkit.cdapi.tools.Tools;

public class MapPool
{
	protected ArrayList<MapInfo> maps;
	
	public MapPool()
	{
		this.maps = new ArrayList<MapInfo>();
	}
	
	public ArrayList<MapInfo> getRandomMaps(int count)
	{
		return getRandomMaps(count, -1, -1, false);
	}
	public ArrayList<MapInfo> getRandomMaps(int count, int minPlayers, int maxPlayers, boolean allowBoundBreaking)
	{
		if(this.maps.isEmpty())
			return new ArrayList<MapInfo>();
		ArrayList<MapInfo> maps;
		maps = filter(minPlayers, maxPlayers);
		if(maps.size() == 0)
			maps = filterClosest(minPlayers, maxPlayers);
		maps = trim(maps, count);
		return maps;
	}
	
	protected ArrayList<MapInfo> filter(int min, int max)
	{
		ArrayList<MapInfo> maps = new ArrayList<MapInfo>();
		for(MapInfo mi : this.maps)
			if((min < 0 || mi.minPlayers <= min)
				&& (max < 0 || mi.maxPlayers >= max))
				maps.add(mi);
		return maps;
	}
	protected ArrayList<MapInfo> filterClosest(int min, int max)
	{
		ArrayList<MapInfo> topMaps = new ArrayList<MapInfo>();
		ArrayList<MapInfo> bottomMaps = new ArrayList<MapInfo>();
		
		int topCnt = getHighestMax(this.maps);
		int bottomCnt = getLowestMin(this.maps);
		
		for(MapInfo mi : this.maps)
		{
			if(mi.minPlayers == bottomCnt)
				bottomMaps.add(mi);
			if(mi.maxPlayers == topCnt)
				topMaps.add(mi);
		}
		
		if(max < 0 || topCnt >= max)
			return bottomMaps;
		if(min < 0 || bottomCnt <= min)
			return topMaps;
		
		//Neither min nor max bound fits
		//Prioritize max bound and taking the maps with the lowest min in topMaps
		bottomCnt = getLowestMin(topMaps);
		topMaps.clear();
		for(MapInfo mi : this.maps)
			if(mi.minPlayers == bottomCnt)
				topMaps.add(mi);
		
		return topMaps;
	}
	protected ArrayList<MapInfo> trim(ArrayList<MapInfo> maps, int count)
	{
		maps = new ArrayList<MapInfo>(maps);
		while(maps.size() > count)
		{
			int rnd = Tools.newPositiveRandomInt() % maps.size();
			maps.remove(rnd);
		}
		return maps;
	}
	
	public static int getHighestMax(ArrayList<MapInfo> maps)
	{
		int max = -1;
		for(MapInfo mi : maps)
			if(max == -1 || mi.maxPlayers > max)
				max = mi.maxPlayers;
		return max;
	}
	public static int getLowestMin(ArrayList<MapInfo> maps)
	{
		int min = -1;
		for(MapInfo mi : maps)
			if(min == -1 || mi.minPlayers < min)
				min = mi.minPlayers;
		return min;
	}
	public static int getLowestMax(ArrayList<MapInfo> maps)
	{
		int min = -1;
		for(MapInfo mi : maps)
			if(min == -1 || mi.maxPlayers < min)
				min = mi.maxPlayers;
		return min;
	}
	public static int getHighestMin(ArrayList<MapInfo> maps)
	{
		int max = -1;
		for(MapInfo mi : maps)
			if(max == -1 || mi.minPlayers > max)
				max = mi.minPlayers;
		return max;
	}
	
	public void addMapInfo(MapInfo mapInfo)
	{
		if(this.maps == null)
			this.maps = new ArrayList<MapInfo>();
		this.maps.add(mapInfo);
	}
	
	public ArrayList<MapInfo> getMaps()
	{
		return this.maps;
	}
	public void setMaps(ArrayList<MapInfo> maps)
	{
		this.maps = maps;
	}
}
