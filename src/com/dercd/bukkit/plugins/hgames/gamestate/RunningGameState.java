package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.SpawnPool;
import com.dercd.bukkit.plugins.hgames.map.chest.Chest;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestMap;
import com.dercd.bukkit.plugins.hgames.scoreboard.HGScoreboard;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;
import com.dercd.bukkit.plugins.hgames.user.Spectator;
import com.dercd.bukkit.plugins.hgames.user.User;

public abstract class RunningGameState extends GameState
{
	public HGScoreboard scoreboard;
	public Countdown countdown;
	
	public RunningGameState(int id)
	{
		super(id);
	}
	public RunningGameState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public abstract Map getMap();
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(HGScoreboard.class, this.scoreboard);
		vars.put(Countdown.class, this.countdown);
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.scoreboard = vars.get(HGScoreboard.class);
	}
	
	public void onLogin(PlayerLoginEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		int externalSpectators = game.getSpectatorCount();
		if(externalSpectators >= Game.MAX_EXTERNAL_SPECTATORS)
			e.disallow(Result.KICK_FULL, textPool.getText("loginDeniedSpectatorsFull"));
		else
			e.allow();
	}
	public void onJoin(PlayerJoinEvent e)
	{
		Game game = this.game;
		Spectator s = new Spectator(e.getPlayer());
		Map map = this.getMap();
		SpawnPool spawnPool = map.getSpawnPool();
		Location firstSpawn = spawnPool.getFirstSpawn();
		
		game.joinUser(s);
		game.prepareSpectator(s, firstSpawn);
	}
	public void onLeave(PlayerQuitEvent e)
	{
		Game game = this.game;
		Player p = e.getPlayer();
		TextPool textPool = game.getTextPool();
		Map map = getMap();
		User u = game.getUser(p);
		
		if(!u.isAlive())
			return;
		
		e.setQuitMessage(null);
		String playerStr = game.formatUserName(p);
		Tools.broadcastMessages(textPool.getMultiText("playerDied", playerStr));
		game.updateScoreboardData(map.getMapInfo(), this.scoreboard, false);
		
		game.dropInventory(p);
		if(game.getLivingPlayerCount() == 1)
		{
			if(this.countdown != null && this.countdown.isRunning())
				this.countdown.cancel();
			game.moveToEndState();
		}
	}
	public void onInteract(PlayerInteractEvent e)
	{
		Game game = this.game;
		Action a = e.getAction();
		ChestMap chestMap = this.getMap().getChestManager().getChestMap();
		
		//Only process right-click-block-actions
		if(a != Action.RIGHT_CLICK_BLOCK)
			return;
		
		Block b = e.getClickedBlock();
		if(b == null)
			return;
		if(!chestMap.containsKey(b))
			return;
		
		e.setCancelled(true);
		Chest c = chestMap.get(b);
		game.openChest(e.getPlayer(), c);
	}
	public void onDeath(PlayerDeathEvent e)
	{
		Game game = this.game;
		Map map = this.getMap();
		
		User u = game.getUser(e.getEntity());
		if(!u.isAlive())
			return;
		if(u != null && u instanceof HGPlayer)
			((HGPlayer) u).setAlive(false);
		game.updateTablist(u);
		
		game.displayDeathMessage(e);
		game.updateScoreboardData(map.getMapInfo(), this.scoreboard, false);
		if(game.getLivingPlayerCount() == 1)
			game.moveToEndState();
	}
	public void onRespawn(PlayerRespawnEvent e)
	{
		Player p = e.getPlayer();
		Map map = getMap();
		Location deathLocation = p.getLocation();
		Location respawnLocation;
		
		if(deathLocation.getWorld() == map.getWorld())
			respawnLocation = deathLocation;
		else
			respawnLocation = map.getSpawnPool().getFirstSpawn();
		
		e.setRespawnLocation(respawnLocation);
	}
	public void onPlayerDamageByPlayer(EntityDamageByEntityEvent e, Player causer)
	{
		Game game = this.game;
		Player hitted = (Player)e.getEntity();
		Entity hitter = e.getDamager();
		double finalDamage = e.getFinalDamage();
		
		//Show MidScreenHearts only on direct hits. Damage via arrows or any other
		//Entities isn't shown
		if(hitter == causer)
			game.showMidScreenHearts(causer, hitted, finalDamage);
		
		onDamage(e);
	}
	public void onPlayerDamageByWorld(EntityDamageEvent e)
	{
		onDamage(e);
	}
	public void onDamage(EntityDamageEvent e)
	{
		double finalDamage = e.getFinalDamage();
		
		if(e.getEntityType() == EntityType.PLAYER)
		{
			Player p = (Player)e.getEntity();
			double finalHealth = p.getHealth() - finalDamage;
			
			if(finalHealth <= 0)
				Bukkit.getScheduler().runTask(CDAPI.getInstance(), () ->
				{
					p.setGameMode(GameMode.SPECTATOR);
				});
		}
	}
	public void onEntityDamage(EntityDamageEvent e)
	{
		EntityType type = e.getEntityType();
		if(type == EntityType.ITEM_FRAME)
		{
			e.setCancelled(true);
			return;
		}
	}
	
	
	public HGScoreboard getScoreboard()
	{
		return this.scoreboard;
	}
	public Countdown getCountdown()
	{
		return this.countdown;
	}
	
	public void setScoreboard(HGScoreboard scoreboard)
	{
		this.scoreboard = scoreboard;
	}
	public void setCountdown(Countdown countdown)
	{
		this.countdown = countdown;
	}
}
