package com.dercd.bukkit.plugins.hgames.gamestate;

import java.util.HashMap;

public class StateTransferVariables extends HashMap<Class, Object>
{
	private static final long serialVersionUID = -1636772411823514966L;
	
	public <T> T get(Class<T> type)
	{
		Object o = super.get(type);
		return (T)o;
	}
}
