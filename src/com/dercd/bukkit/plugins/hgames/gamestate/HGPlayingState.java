package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown.RunnableOption;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.dm.DMap;
import com.dercd.bukkit.plugins.hgames.map.loading.DMapLoader;
import com.dercd.bukkit.plugins.hgames.map.loading.MapLoadingWaitManager;

public class HGPlayingState extends HGRunningGameState
{
	public boolean infinite;
	StateRunningTime previousStateRunningTime;
	public BukkitTask dmCountdownReduceNoticeTask;
	public DMap dmap;
	public MapLoadingWaitManager mapLoadingWaitManager;
	
	public static final int DM_COUNTDOWN_REDUCE_TIME = 30;
	public static final int DM_COUNTDOWN_REDUCE_NOTICE_DELAY = 10 * Tools.TICKS_PER_SECOND;
	public static final int DM_COUNTDOWN_REDUCE_NOTICE_INTERVAL = 120 * Tools.TICKS_PER_SECOND;
	
	public HGPlayingState(int id)
	{
		super(id);
		init();
	}
	public HGPlayingState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
		init();
	}
	
	protected void init()
	{
		this.mapLoadingWaitManager = new MapLoadingWaitManager(this);
	}
	
	protected void setAbilities()
	{
		super.setAbilities();
		setAbility(Ability.PLAYER_DAMAGE);
		setAbility(Ability.INFINITE_MATCH);
	}
	
	public String getName()
	{
		return "HGPlaying";
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		
		Countdown c = game.createHGPlayingCountdown(this.map, this.scoreboard);
		c.addOption(new RunnableOption(() ->
		{
			DMapLoader mapLoader = game.createDMapLoader();
			mapLoader.setOnFinished((dmap) ->
			{
				this.dmap = (DMap)dmap;
				this.mapLoadingWaitManager.onMapLoaded(dmap);
			});
			mapLoader.startMapLoading();
		}, DMapLoader.MAP_LOADING_BUFFER));
		
		this.countdown = c;
		game.setCurrentCountdown(c);
		
		int living = game.getLivingPlayerCount();
		if(living <= this.map.getMaxDMCountdownReducePlayers() && this.dmCountdownReduceNoticeTask == null)
			startDMCountdownReduceNoticeTask();
		
		return this;
	}
	public GameState applyNextState()
	{
		if(this.nextState instanceof EndState)
			return super.applyNextState();
		
		GameState state = this.mapLoadingWaitManager.onApplyNextState(this.dmap);
		if(state != null)
			return state;
		return super.applyNextState();
	}
	
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.previousStateRunningTime = vars.get(StateRunningTime.class);
	}
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(DMap.class, this.dmap);
		return vars;
	}
	
	public void onCommand(CommandEvent e) throws CDCException
	{
		switch(e.getStrCommand())
		{
			case "dm":
				executeDMCommand(e);
				break;
			default:
				super.onCommand(e);
				break;
		}
	}
	
	public void onDeath(PlayerDeathEvent e)
	{
		Game game = this.game;
		
		super.onDeath(e);
		
		int living = game.getLivingPlayerCount();
		
		if(living <= this.map.getMaxDMCountdownReducePlayers() && this.dmCountdownReduceNoticeTask == null)
			startDMCountdownReduceNoticeTask();
	}
	
	public void startDMCountdownReduceNoticeTask()
	{
		Game game = this.game;
		HGPlayingState self = this;
		
		this.dmCountdownReduceNoticeTask = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			
			if(game.getCurrentState() != self || self.countdown == null || self.countdown.getTime() <= DM_COUNTDOWN_REDUCE_TIME || self.infinite)
			{
				self.dmCountdownReduceNoticeTask.cancel();
				return;
			}
			
			game.displayDMCountdownReduceNotice();
		}, DM_COUNTDOWN_REDUCE_NOTICE_DELAY, DM_COUNTDOWN_REDUCE_NOTICE_INTERVAL);
	}
	
	public void executeDMCommand(CommandEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		if(this.countdown == null)
			return;
		
		CommandSender sender = e.getSender();
		
		if(!game.isCommandSenderAlive(sender) && !game.isCommandSenderConsole(sender))
		{
			Tools.broadcastMessages(textPool.getMultiText("dmCommandNotAllowed"), sender);
			return;
		}
		
		if(this.countdown.getTime() <= DM_COUNTDOWN_REDUCE_TIME)
			return;
		
		this.countdown.setTime(DM_COUNTDOWN_REDUCE_TIME + 1);
	}
	
	public boolean isFountainShootingTime()
	{
		if(this.countdown == null || !this.countdown.isRunning())
			return true;
		
		Countdown countdown = this.countdown;
		StateRunningTime previousStateRunningTime = this.previousStateRunningTime;
		
		int fullTime = countdown.getFullTime();
		int currentTime = countdown.getTime();
		int previousStateTime = previousStateRunningTime == null ? 0 : previousStateRunningTime.getRunningTime();
		
		//Including previous state time as it also counts as playing time
		//If the previous state isn't HGProtectionState, no running time would be transfered
		//and the result would be a previousRunningTime of zero
		if(fullTime - (currentTime + previousStateTime) > FOUNTAIN_SILENT_TIME)
			return true;
		return false;
	}
	
	
	public boolean getInfinite()
	{
		return this.infinite;
	}
	public StateRunningTime getPreviousStateRunningTime()
	{
		return this.previousStateRunningTime;
	}
	public BukkitTask getDmCountdownReduceNoticeTask()
	{
		return this.dmCountdownReduceNoticeTask;
	}
	public DMap getDmap()
	{
		return this.dmap;
	}
	public MapLoadingWaitManager getMapLoadingWaitManager()
	{
		return this.mapLoadingWaitManager;
	}
	
	public void setInfinite(boolean infinite)
	{
		this.infinite = infinite;
	}
	public void setPreviousStateRunningTime(StateRunningTime previousStateRunningTime)
	{
		this.previousStateRunningTime = previousStateRunningTime;
	}
	public void setDmCountdownReduceNoticeTask(BukkitTask dmCountdownReduceNoticeTask)
	{
		this.dmCountdownReduceNoticeTask = dmCountdownReduceNoticeTask;
	}
	public void setDmap(DMap dmap)
	{
		this.dmap = dmap;
	}
	public void setMapLoadingWaitManager(MapLoadingWaitManager mapLoadingWaitManager)
	{
		this.mapLoadingWaitManager = mapLoadingWaitManager;
	}
}
