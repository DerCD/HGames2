package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidSenderException;
import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.MapVoteManager;
import com.dercd.bukkit.plugins.hgames.user.ReservationManager;

public class LobbyVotingState extends LobbyState
{
	MapVoteManager voteManager;
	CDScoreboard voteScoreboard;
	Countdown countdown;
	
	public LobbyVotingState(int id)
	{
		super(id);
		init();
	}
	public LobbyVotingState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
		init();
	}
	
	public void init()
	{
		this.reservationManager = new ReservationManager();
	}
	
	public String getName()
	{
		return "LobbyVoting";
	}
	
	public GameState applyState()
	{
		super.applyState();
		Game game = this.game;
		
		MapVoteManager voteManager = this.voteManager = game.createVoteManager();
		CDScoreboard voteScoreboard = this.voteScoreboard = game.createMapVotingScoreboard(voteManager);
		game.setScoreboard(voteScoreboard);
		this.countdown = null;
		
		return this;
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(MapVoteManager.class, this.voteManager);
		vars.put(Countdown.class, this.countdown);
		return vars;
	}

	public DisplayState getDisplayState()
	{
		return DisplayState.VOTING;
	}
	
	public void onLogin(PlayerLoginEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		int currentPlayerCount = game.getPlayerCount();
		if(currentPlayerCount >= this.voteManager.getLowestMaxPlayerCount())
			e.disallow(Result.KICK_FULL, textPool.getText("loginDeniedFull"));
		else
			e.allow();
	}
	public void onJoin(PlayerJoinEvent e)
	{
		super.onJoin(e);
		if(this.countdown == null || !this.countdown.isRunning())
		{
			Game game = Game.getInstance();
			Countdown countdown = game.createLobbyVotingCountdown(this.voteManager);
			
			this.countdown = countdown;
			countdown.start();
		}
	}
	public void onLeave(PlayerQuitEvent e)
	{
		super.onLeave(e);
		Game game = Game.getInstance();
		
		if(game.getPlayerCount() <= 0)
		{
			Countdown countdown = this.countdown;
			if(countdown != null)
			{
				countdown.cancel();
				this.countdown = null;
			}
		}
	}
	
	public void onCommand(CommandEvent e) throws CDInvalidArgsException, CDInvalidSenderException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		String[] args = e.getArgs();
		switch(e.getStrCommand().toLowerCase())
		{
			case "v":
			case "vote":
				e.validateCount(1);
				e.validateInt(0);
				e.validateSenderIsPlayer();
				
				Player p = e.getSenderAsPlayer();
				int vote = Integer.parseInt(args[0]);
				int mapCount = this.voteManager.getMapCount();
				if(vote < 1 || vote > mapCount)
				{
					e.getSender().sendMessage(textPool.getMultiText("voteCmdInvalidNum", String.valueOf(mapCount)));
					return;
				}
				Integer previousVote = this.voteManager.getVoteOf(p);
				if(vote == previousVote)
				{
					p.sendMessage(textPool.getMultiText("voteAlreadyCount"));
					return;
				}
				this.voteManager.vote(p, vote);
				if(previousVote == null)
					p.sendMessage(textPool.getMultiText("voteSuccessfull"));
				else
					p.sendMessage(textPool.getMultiText("voteChanged"));
		}
	}
	
	
	public MapVoteManager getVoteManager()
	{
		return this.voteManager;
	}
	public CDScoreboard getVoteScoreboard()
	{
		return this.voteScoreboard;
	}
	public Countdown getCountdown()
	{
		return this.countdown;
	}
	
	public void setVoteManager(MapVoteManager voteManager)
	{
		this.voteManager = voteManager;
	}
	public void setVoteScoreboard(CDScoreboard voteScoreboard)
	{
		this.voteScoreboard = voteScoreboard;
	}
	public void setCountdown(Countdown countdown)
	{
		this.countdown = countdown;
	}
}
