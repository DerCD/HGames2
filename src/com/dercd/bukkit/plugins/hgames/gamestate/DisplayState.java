package com.dercd.bukkit.plugins.hgames.gamestate;

public enum DisplayState
{
	VOTING,
	WAITING,
	STARTING,
	RUNNING,
	DM,
	END,
	DOWN;
}
