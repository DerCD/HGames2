package com.dercd.bukkit.plugins.hgames.gamestate;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.SpawnPool;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;
import com.dercd.bukkit.plugins.hgames.user.Spectator;
import com.dercd.bukkit.plugins.hgames.user.User;

public class DMCountdownState extends DMRunningGameState implements CountdownState
{
	public static final int RETELEPORT_TIME = 5; //Seconds after first teleport
	public static final int RETELEPORT_BUFFER = 2; //Minimum seconds after reteleport before this state ends
	
	public DMCountdownState(int id)
	{
		super(id);
	}
	public DMCountdownState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public String getName()
	{
		return "DMCountdown";
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		SpawnPool spawnPool = this.map.getSpawnPool();
		ArrayList<HGPlayer> players = game.getPlayers();
		ArrayList<Spectator> spectators = game.getSpectators();
		ArrayList<Location> spawns = spawnPool.getSpawns(players.size());
		ArrayList<User> users = new ArrayList<User>(players);
		users.addAll(spectators);
		
		game.teleportUsers(users, spawns, 1);
		//If we have enough time, teleport the users after RETELEPORT_TIME seconds again
		//to catch minecraft errors where users fall into the ground
		if(this.getDuration() >= RETELEPORT_TIME + RETELEPORT_BUFFER)
			Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
			{
				game.teleportUsers(players, spawns, 0);
			}, RETELEPORT_TIME * Tools.TICKS_PER_SECOND);
		
		Countdown countdown = game.createDMWaitingCountdown(this.map);
		this.countdown = countdown;
		game.setCurrentCountdown(countdown);
		
		this.scoreboard.setTitle(textPool.getText("dmCountdownScoreboardTitle"));
		
		return this;
	}
}
