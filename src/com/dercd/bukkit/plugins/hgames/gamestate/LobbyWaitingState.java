package com.dercd.bukkit.plugins.hgames.gamestate;

import java.io.IOException;

import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.IIOManager;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.MapVoteManager;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapInfo;
import com.dercd.bukkit.plugins.hgames.map.loading.HGMapLoader;
import com.dercd.bukkit.plugins.hgames.map.loading.MapLoader;
import com.dercd.bukkit.plugins.hgames.map.loading.MapLoadingWaitManager;
import com.dercd.bukkit.plugins.hgames.scoreboard.HGScoreboard;

public class LobbyWaitingState extends LobbyState
{
	HGScoreboard scoreboard;
	MapVoteManager voteManager;
	HGMapInfo votedMapInfo;
	HGMap votedMap;
	HGMapLoader maploader;
	Countdown countdown;
	MapLoadingWaitManager mapLoadingWaitManager;
	
	public LobbyWaitingState(int id)
	{
		super(id);
		init();
	}
	public LobbyWaitingState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
		init();
	}
	
	protected void init()
	{
		this.mapLoadingWaitManager = new MapLoadingWaitManager(this);
	}
	
	public String getName()
	{
		return "LobbyWaiting";
	}
	
	public GameState applyPreviousState()
	{
		Game game = this.game;
		IIOManager ioManager = game.getIOManager();
		MapLoader mapLoader = this.maploader;
		
		mapLoader.interrupt(() ->
		{
			ioManager.unloadHGWorld();
			try
			{
				ioManager.deleteTmpMapFolders();
			}
			catch(IOException x)
			{
				game.hgames2.performErrorAction();
			}
		});
		return super.applyPreviousState();
	}
	public GameState applyState()
	{
		super.applyState();
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		HGMapInfo mapInfo = (HGMapInfo) this.voteManager.getVoteResult();
		String[] mapVotingTexts = textPool.getMultiText("mapVotingEnded", mapInfo.getName(), mapInfo.getCreator());
		Tools.broadcastMessages(mapVotingTexts);
		HGScoreboard scoreboard = game.createHGPlayingScoreboard(mapInfo);
		this.votedMapInfo = mapInfo;
		this.scoreboard = scoreboard;
		game.setScoreboard(scoreboard);
		
		this.votedMap = null;
		HGMapLoader mapLoader = new HGMapLoader();
		mapLoader.setMapInfo(mapInfo);
		mapLoader.createTasks();
		mapLoader.setOnFinished((map) ->
		{
			this.votedMap = (HGMap)map;
			this.mapLoadingWaitManager.onMapLoaded(map);
		});
		this.maploader = mapLoader;
		mapLoader.startMapLoading();
		
		if(game.getPlayerCount() < mapInfo.getMinPlayers())
		{
			game.cancelLobbyCountdown(this.countdown);
			this.countdown = null;
		}
		return this;
	}
	public GameState applyNextState()
	{
		GameState state = this.mapLoadingWaitManager.onApplyNextState(this.votedMap);
		if(state != null)
			return state;
		return super.applyNextState();
	}
	
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.voteManager = vars.get(MapVoteManager.class);
		this.countdown = vars.get(Countdown.class);
	}
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(HGMap.class, this.votedMap);
		vars.put(HGScoreboard.class, this.scoreboard);
		return vars;
	}

	public DisplayState getDisplayState()
	{
		return DisplayState.WAITING;
	}
	
	public void onLogin(PlayerLoginEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		int currentPlayers = game.getPlayerCount();
		if(currentPlayers >= this.votedMapInfo.getMaxPlayers())
			e.disallow(Result.KICK_FULL, textPool.getText("loginDeniedFull"));
		else
			e.allow();
	}
	public void onJoin(PlayerJoinEvent e)
	{
		super.onJoin(e);
		
		if(this.votedMapInfo == null || this.scoreboard == null)
			return;
		Game game = Game.getInstance();
		
		game.updateScoreboardData(this.votedMapInfo, this.scoreboard, true);
		
		if(this.countdown == null || !this.countdown.isRunning())
		{
			if(game.getPlayerCount() >= this.votedMapInfo.getMinPlayers())
			{
				Countdown countdown = game.createLobbyWaitingCountdown();
				this.countdown = countdown;
				countdown.start();
			}
		}
			
	}
	public void onLeave(PlayerQuitEvent e)
	{
		super.onLeave(e);
		Game game = Game.getInstance();
		
		
		if(this.votedMapInfo == null || this.scoreboard == null)
			return;
		
		game.updateScoreboardData(this.votedMapInfo, this.scoreboard, true);
		
		int playerCount = game.getPlayerCount();
		if(playerCount < this.votedMapInfo.getMinPlayers())
		{
			if(this.countdown != null && this.countdown.isRunning())
			{
				game.cancelLobbyCountdown(this.countdown);
				this.countdown = null;
			}
		}
		
		if(playerCount <= 0)
		{
			Countdown countdown = this.countdown;
			if(countdown != null)
			{
				game.cancelLobbyCountdown(this.countdown);
				this.countdown = null;
			}
			game.moveToPreviousStateFrom(LobbyWaitingState.class);
		}
	}
	
	public void onCommand(CommandEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		switch(e.getStrCommand())
		{
			case "v":
			case "vote":
				e.getSender().sendMessage(textPool.getMultiText("voteTooLate"));
				return;
		}
	}
	
	
	public HGScoreboard getScoreboard()
	{
		return this.scoreboard;
	}
	public MapVoteManager getVoteManager()
	{
		return this.voteManager;
	}
	public HGMapInfo getVotedMapInfo()
	{
		return this.votedMapInfo;
	}
	public HGMap getVotedMap()
	{
		return this.votedMap;
	}
	public HGMapLoader getMaploader()
	{
		return this.maploader;
	}
	public Countdown getCountdown()
	{
		return this.countdown;
	}
	public MapLoadingWaitManager getMapLoadingWaitManager()
	{
		return this.mapLoadingWaitManager;
	}
	
	public void setScoreboard(HGScoreboard scoreboard)
	{
		this.scoreboard = scoreboard;
	}
	public void setVoteManager(MapVoteManager voteManager)
	{
		this.voteManager = voteManager;
	}
	public void setVotedMapInfo(HGMapInfo votedMapInfo)
	{
		this.votedMapInfo = votedMapInfo;
	}
	public void setVotedMap(HGMap votedMap)
	{
		this.votedMap = votedMap;
	}
	public void setMaploader(HGMapLoader maploader)
	{
		this.maploader = maploader;
	}
	public void setCountdown(Countdown countdown)
	{
		this.countdown = countdown;
	}
	public void setMapLoadingWaitManager(MapLoadingWaitManager mapLoadingWaitManager)
	{
		this.mapLoadingWaitManager = mapLoadingWaitManager;
	}
}
