package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.GameMode;

import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMapTimes;
import com.dercd.bukkit.plugins.hgames.user.User;

public class HGProtectionState extends HGRunningGameState
{
	StateRunningTime stateRunningTime;
	
	public HGProtectionState(int id)
	{
		super(id);
	}
	public HGProtectionState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public String getName()
	{
		return "HGProtection";
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		HGMapTimes mapTimes = this.map.getMapTimes();
		int protectionTime = mapTimes.protectionTime;
		
		//Using state running time to transfer protection time to next state
		//which is currently HGPlayingState
		this.stateRunningTime = new StateRunningTime(protectionTime);
		//If protectionTime is zero, we're skipping this
		//jumping to the next state
		if(protectionTime == 0)
		{
			return applyNextState();
		}
		
		Countdown c = game.createHGProtectionCountdown(this.map, this.scoreboard);
		game.setCurrentCountdown(c);
		
		for(User u : game.getLivingPlayers())
			u.getPlayer().setGameMode(GameMode.SURVIVAL);
		
		return this;
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(StateRunningTime.class, this.stateRunningTime);
		return vars;
	}

	@Override
	public boolean isFountainShootingTime()
	{
		if(this.countdown == null || !this.countdown.isRunning())
			return true;
		
		Countdown countdown = this.countdown;
		
		int fullTime = countdown.getFullTime();
		int currentTime = countdown.getTime();
		
		//Including previous state time as it also counts as playing time
		//If the previous state isn't HGProtectionState, no running time would be transfered
		//and the result would be a previousRunningTime of zero
		if(fullTime - currentTime > FOUNTAIN_SILENT_TIME)
			return true;
		return false;
	}
	
	public StateRunningTime getStateRunningTime()
	{
		return this.stateRunningTime;
	}
	
	public void setStateRunningTime(StateRunningTime stateRunningTime)
	{
		this.stateRunningTime = stateRunningTime;
	}
}
