package com.dercd.bukkit.plugins.hgames.gamestate;

import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;

public class DMPlayingState extends DMRunningGameState
{
	public DMPlayingState(int id)
	{
		super(id);
	}
	public DMPlayingState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	protected void setAbilities()
	{
		super.setAbilities();
		setAbility(Ability.ANNOYING_BLOCK_DESTROY);
		setAbility(Ability.WORLD_DAMAGE);
		setAbility(Ability.INFINITE_MATCH);
		setAbility(Ability.MOVE);
		setAbility(Ability.SPECTATE);
		setAbility(Ability.OPEN_CHEST);
		setAbility(Ability.USE_ITEMS);
	}
	
	public String getName()
	{
		return "DMPlaying";
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		
		Countdown countdown = game.createDMPlayingCountdowdn(this.map, this.scoreboard);
		this.countdown = countdown;
		game.setCurrentCountdown(countdown);
		
		return this;
	}
}
