package com.dercd.bukkit.plugins.hgames.gamestate;

import java.util.HashSet;

import org.bukkit.entity.Player;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCException;
import com.dercd.bukkit.plugins.hgames.Game;

public abstract class GameState
{
	protected int id;
	protected GameState previousState;
	protected GameState nextState;
	protected int duration;
	protected Game game;
	
	HashSet<Ability> abilities = new HashSet<Ability>();
	
	public GameState(int id)
	{
		this(id, null, null);
	}
	public GameState(int id, GameState previousState, GameState nextState)
	{
		this.id = id;
		this.previousState = previousState;
		this.nextState = nextState;
		this.game = Game.getInstance();
		
		setAbilities();
	}
	
	protected void setAbilities()
	{ }
	
	public boolean hasAbility(Ability a)
	{
		return this.abilities.contains(a);
	}
	protected void setAbility(Ability a)
	{
		this.abilities.add(a);
	}
	protected void removeAbility(Ability a)
	{
		this.abilities.remove(a);
	}
	
	public abstract String getName();
	public GameState applyPreviousState()
	{
		if(this.previousState == null)
			return this;
		return this.previousState.revertToState();
	}
	public GameState revertToState()
	{
		return this.applyState();
	}
	public abstract GameState applyState();
	public GameState applyNextState()
	{
		if(this.nextState == null)
			return this;
		StateTransferVariables vars = getTransferVariables();
		this.nextState.importVariables(vars);
		return this.nextState.applyState();
	}
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = new StateTransferVariables();
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{ }
	
	public abstract DisplayState getDisplayState();
	
	public void onLogin(PlayerLoginEvent e)
	{
		if(!hasAbility(Ability.JOIN) && !hasAbility(Ability.SPECTATE))
			this.game.denyOnLogin(e);
	}
	public void onJoin(PlayerJoinEvent e)
	{
		if(!hasAbility(Ability.JOIN) && !hasAbility(Ability.SPECTATE))
			this.game.denyOnJoin(e);
	}
	public void onPlayerDamageByPlayer(EntityDamageByEntityEvent e, Player causer)
	{
		if(!hasAbility(Ability.PLAYER_DAMAGE))
			this.game.denyOnPlayerDamageByPlayer(e, causer);
	}
	public void onPlayerDamageByWorld(EntityDamageEvent e)
	{
		if(!hasAbility(Ability.WORLD_DAMAGE))
			this.game.denyOnPlayerDamageByWorld(e);
	}
	public void onInteract(PlayerInteractEvent e)
	{
		if(!hasAbility(Ability.USE_ITEMS) && !hasAbility(Ability.OPEN_CHEST))
		this.game.denyOnInteract(e);
	}
	public void onLeave(PlayerQuitEvent e)
	{
		this.game.defaultOnLeave(e);
	}
	public void onCommand(CommandEvent e) throws CDCException
	{
		this.game.defaultOnCommand(e);
	}
	public void onBlockBreak(BlockBreakEvent e)
	{
		if(!this.hasAbility(Ability.MAP_DESTROY))
			this.game.denyOnBlockBreak(e);
	}
	public void onAnnoyingBlockBreak(BlockBreakEvent e)
	{
		if(!this.hasAbility(Ability.ANNOYING_BLOCK_DESTROY))
			this.game.denyOnBlockBreak(e);
	}
	public void onMove(PlayerMoveEvent e)
	{
		if(!hasAbility(Ability.MOVE))
			this.game.denyOnMove(e);
	}
	public void onDeath(PlayerDeathEvent e)
	{
		this.game.defaultOnDeath(e);
	}
	public void onRespawn(PlayerRespawnEvent e)
	{
		this.game.defaultOnRespawn(e);
	}
	public void onEntityDamage(EntityDamageEvent e)
	{
		this.game.defaultOnEntityDamage(e);
	}
	public void onChat(AsyncPlayerChatEvent e)
	{
		this.game.defaultOnChat(e);
	}
	
	public int getId()
	{
		return this.id;
	}
	public GameState getPreviousState()
	{
		return this.previousState;
	}
	public GameState getNextState()
	{
		return this.nextState;
	}
	public int getDuration()
	{
		return this.duration;
	}
	
	public void setPreviousState(GameState state)
	{
		this.previousState = state;
	}
	public void setNextState(GameState state)
	{
		this.nextState = state;
	}
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
}
