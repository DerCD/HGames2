package com.dercd.bukkit.plugins.hgames.gamestate;

import com.dercd.bukkit.plugins.hgames.user.ReservationManager;

public abstract class PreparingState extends GameState
{
	protected ReservationManager reservationManager;
	
	public PreparingState(int id)
	{
		super(id);
	}
	public PreparingState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(ReservationManager.class, this.reservationManager);
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.reservationManager = vars.get(ReservationManager.class);
	}
	
	
	public ReservationManager getReservationManager()
	{
		return this.reservationManager;
	}
	public void setReservationManager(ReservationManager reservationManager)
	{
		this.reservationManager = reservationManager;
	}
}
