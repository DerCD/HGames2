package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.Bukkit;
import org.bukkit.World;

import com.dercd.bukkit.plugins.hgames.Game;

public class InitState extends GameState
{
	public static final String LOBBY_WORLD_NAME = "lobby";
	
	public InitState(int id)
	{
		super(id);
	}
	public InitState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	@Override
	public String getName()
	{
		return "Init";
	}
	@Override
	public GameState applyState()
	{
		Game game = this.game;
		
		World w = Bukkit.getWorld(LOBBY_WORLD_NAME);
		game.setLobbyGameRules(w);
		
		return applyNextState();
	}
	
	public DisplayState getDisplayState()
	{
		return DisplayState.DOWN;
	}
}
