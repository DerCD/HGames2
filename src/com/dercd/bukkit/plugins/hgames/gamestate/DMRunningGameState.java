package com.dercd.bukkit.plugins.hgames.gamestate;

import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.dm.DMap;

public abstract class DMRunningGameState extends RunningGameState
{
	public DMap map;
	
	public DMRunningGameState(int id)
	{
		super(id);
	}
	public DMRunningGameState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(DMap.class, this.map);
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.map = vars.get(DMap.class);
	}
	
	public Map getMap()
	{
		return this.map;
	}
	
	public DisplayState getDisplayState()
	{
		return DisplayState.DM;
	}
}
