package com.dercd.bukkit.plugins.hgames.gamestate;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.ISettings;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.map.MapInfo;
import com.dercd.bukkit.plugins.hgames.map.SpawnPool;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.scoreboard.HGScoreboard;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;
import com.dercd.bukkit.plugins.hgames.user.User;

public class HGCountdownState extends PreparingState implements CountdownState
{
	public HGScoreboard scoreboard;
	public HGMap map;
	Countdown countdown;
	
	public static final int RETELEPORT_TIME = 5; //Seconds after first teleport
	public static final int RETELEPORT_BUFFER = 2; //Minimum seconds after reteleport before this state ends
	
	public HGCountdownState(int id)
	{
		super(id);
	}
	public HGCountdownState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	public String getName()
	{
		return "HGCountdown";
	}
	
	public GameState applyPreviousState()
	{
		Game game = this.game;
		ISettings settings = game.getSettings();
		Location lobbySpawn = settings.getLobbySpawn();
		
		game.teleportUsers(null, lobbySpawn, 0.2);
		return super.applyPreviousState();
	}
	public GameState applyState()
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		SpawnPool spawnPool = this.map.getSpawnPool();
		ArrayList<HGPlayer> players = game.getPlayers();
		ArrayList<Location> spawns = spawnPool.getSpawns(players.size());
		ArrayList<User> users = new ArrayList<User>(players);
		
		game.teleportUsers(users, spawns, 0.2, () ->
		{
			game.setGameMode(GameMode.SURVIVAL, game.getPlayers());
		});
		
		//If we have enough time, teleport the users after RETELEPORT_TIME seconds again
		//to catch minecraft errors where users fall into the ground
		if(this.getDuration() >= RETELEPORT_TIME + RETELEPORT_BUFFER)
			Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
			{
				game.teleportUsers(users, spawns, 0, () ->
				{
					game.setGameMode(GameMode.SURVIVAL, game.getPlayers());
				});
			}, RETELEPORT_TIME * Tools.TICKS_PER_SECOND);
		
		Countdown countdown = game.createHGWaitingCountdown(this.map);
		this.countdown = countdown;
		game.setCurrentCountdown(countdown);
		
		this.scoreboard.setTitle(textPool.getText("hgCountdownScoreboardTitle"));
		
		return this;
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(HGMap.class, this.map);
		vars.put(HGScoreboard.class, this.scoreboard);
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.scoreboard = vars.get(HGScoreboard.class);
		this.map = vars.get(HGMap.class);
	}
	
	public DisplayState getDisplayState()
	{
		return DisplayState.STARTING;
	}
	
	public void onLogin(PlayerLoginEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		e.disallow(Result.KICK_OTHER, textPool.getText("loginDeniedCountdown"));
	}
	public void onLeave(PlayerQuitEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		MapInfo mapInfo = this.map.getMapInfo();
		
		game.updateScoreboardData(mapInfo, this.scoreboard, true);
		if(game.getPlayerCount() < mapInfo.getMinPlayers())
		{
			Tools.broadcastMessages(textPool.getMultiText("lobbyWaitingKickback"));
			game.moveToPreviousStateFrom(HGCountdownState.class);
		}
	}
	
	
	public HGScoreboard getScoreboard()
	{
		return this.scoreboard;
	}
	public HGMap getMap()
	{
		return this.map;
	}
	public Countdown getCountdown()
	{
		return this.countdown;
	}
	
	public void setScoreboard(HGScoreboard scoreboard)
	{
		this.scoreboard = scoreboard;
	}
	public void setMap(HGMap map)
	{
		this.map = map;
	}
	public void setCountdown(Countdown countdown)
	{
		this.countdown = countdown;
	}
}
