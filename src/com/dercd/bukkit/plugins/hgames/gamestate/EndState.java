package com.dercd.bukkit.plugins.hgames.gamestate;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.collection.CircularIterator;
import com.dercd.bukkit.cdapi.tools.minecraft.Countdown;
import com.dercd.bukkit.cdapi.tools.minecraft.PlayerSnapshot;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.ISettings;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.TitleTemplate;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;

public class EndState extends GameState
{
	Countdown previousCountdown;
	
	public static final double WINNER_DISPLAY_PERIOD = 3; //Seconds between title displays
	public static final double WINNER_DISPLAY_DELAY = 1; //Seconds between title displays
	
	public EndState(int id)
	{
		super(id);
	}
	public EndState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	protected void setAbilities()
	{
		super.setAbilities();
		setAbility(Ability.MOVE);
	}
	
	public String getName()
	{
		return "End";
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		ISettings settings = game.getSettings();
		
		if(this.previousCountdown != null)
			this.previousCountdown.cancel();
		
		ArrayList<HGPlayer> winners = game.getLivingPlayers();
		ArrayList<PlayerSnapshot> winnerSnapshots = new ArrayList<PlayerSnapshot>();
		ArrayList<TitleTemplate> titles = new ArrayList<TitleTemplate>();
		for(HGPlayer winner : winners)
		{
			PlayerSnapshot snapshot = PlayerSnapshot.fromPlayer(winner.getPlayer());
			winnerSnapshots.add(snapshot);
			TitleTemplate title = game.generateWinnerTitle(snapshot); 
			titles.add(title);
		}
		
		CircularIterator<TitleTemplate> iter = new CircularIterator<TitleTemplate>(titles);
		
		String[] winnersText;
		if(winners.size() > 1)
			winnersText = textPool.getMultiText("finishedMultiWinner", game.generateWinnersString(winners));
		else
			winnersText = textPool.getMultiText("finishedSingleWinner", game.formatUserName(winners.get(0)));
		Tools.broadcastMessages(winnersText);
		
		Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			iter.next().display();
		}, (int)(Tools.TICKS_PER_SECOND * WINNER_DISPLAY_DELAY), (int)(Tools.TICKS_PER_SECOND * WINNER_DISPLAY_PERIOD));
		
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
		{
			Tools.kickAll("Game over");
			Bukkit.shutdown();
		}, Tools.TICKS_PER_SECOND * settings.getEndTime());
		
		return this;
	}
	
	public void importVariables(StateTransferVariables vars)
	{
		this.previousCountdown = vars.get(Countdown.class);
	}
	
	public DisplayState getDisplayState()
	{
		return DisplayState.END;
	}
	
	public void onLogin(PlayerLoginEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		e.disallow(Result.KICK_OTHER, textPool.getText("loginDeniedGameEnd"));
	}
	public void onRespawn(PlayerRespawnEvent e)
	{
		Player p = e.getPlayer();
		Location deathLocation = p.getLocation();
		
		e.setRespawnLocation(deathLocation);
	}
	public void onChat(AsyncPlayerChatEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		Player p = e.getPlayer();
		String message = e.getMessage();
		
		String formattedPlayer = game.formatUserName(p);
		String[] formattedMessage = textPool.getMultiText("chat", formattedPlayer, message);
		
		Tools.sendMessage(formattedMessage);
			
		e.setCancelled(true);
	}
	
	public Countdown getPreviousCountdown()
	{
		return this.previousCountdown;
	}
	
	public void setPreviousCountdown(Countdown previousCountdown)
	{
		this.previousCountdown = previousCountdown;
	}
}
