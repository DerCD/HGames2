package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.ISettings;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.user.HGPlayer;

public abstract class LobbyState extends PreparingState
{
	protected Location spawnLocation;
	
	public LobbyState(int id)
	{
		super(id);
	}
	public LobbyState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	protected void setAbilities()
	{
		setAbility(Ability.MOVE);
		setAbility(Ability.JOIN);
	}
	
	public GameState applyState()
	{
		Game game = this.game;
		ISettings settings = game.getSettings();
		
		this.spawnLocation = settings.getLobbySpawn();
		return this;
	}
	
	public void onJoin(PlayerJoinEvent e)
	{
		Game game = this.game;
		Player p = e.getPlayer();
		HGPlayer player = new HGPlayer(p);
		Location spawn = this.spawnLocation;
		TextPool textPool = game.getTextPool();
		
		game.joinUser(player);
		
		String[] joinMessage = textPool.getMultiText("playerJoin", game.formatUserName(p));
		Tools.broadcastMessages(joinMessage);
		p.teleport(spawn);
		p.setGameMode(GameMode.ADVENTURE);
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
		{
			//Checking if block at head is solid
			//If so, user fell into the ground
			if(p.getLocation().add(0, 1, 0).getBlock().getType().isSolid())
				p.teleport(spawn);
		}, Tools.TICKS_PER_SECOND * 5);
	}
	public void onRespawn(PlayerRespawnEvent e)
	{
		Location respawnLocation = this.spawnLocation;
		
		e.setRespawnLocation(respawnLocation);
	}
	public void onPlayerDamageByWorld(EntityDamageEvent e)
	{
		if(e.getCause() == DamageCause.VOID)
			e.getEntity().teleport(this.spawnLocation);
		e.setCancelled(true);
	}
	
	
	public Location getSpawnLocation()
	{
		return this.spawnLocation;
	}
	
	public void setSpawnLocation(Location spawnLocation)
	{
		this.spawnLocation = spawnLocation;
	}
}