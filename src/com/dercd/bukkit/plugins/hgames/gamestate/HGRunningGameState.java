package com.dercd.bukkit.plugins.hgames.gamestate;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.map.Fountain;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.hg.HGMap;
import com.dercd.bukkit.plugins.hgames.user.User;

public abstract class HGRunningGameState extends RunningGameState
{
	HGMap map;
	
	public static final int FOUNTAIN_SILENT_TIME = 15; //Seconds in which no fountains are shot
	
	public HGRunningGameState(int id)
	{
		super(id);
	}
	public HGRunningGameState(int id, GameState previousState, GameState nextState)
	{
		super(id, previousState, nextState);
	}
	
	protected void setAbilities()
	{
		super.setAbilities();
		setAbility(Ability.ANNOYING_BLOCK_DESTROY);
		setAbility(Ability.WORLD_DAMAGE);
		setAbility(Ability.MOVE);
		setAbility(Ability.SPECTATE);
		setAbility(Ability.OPEN_CHEST);
		setAbility(Ability.USE_ITEMS);
	}
	
	public StateTransferVariables getTransferVariables()
	{
		StateTransferVariables vars = super.getTransferVariables();
		vars.put(HGMap.class, this.map);
		return vars;
	}
	public void importVariables(StateTransferVariables vars)
	{
		super.importVariables(vars);
		this.map = vars.get(HGMap.class);
	}

	public DisplayState getDisplayState()
	{
		return DisplayState.RUNNING;
	}
	
	public void onDeath(PlayerDeathEvent e)
	{
		Game game = this.game;
		Player killed = e.getEntity();
		User u = game.getUser(killed);
		
		if(!u.isAlive())
			return;
		
		super.onDeath(e);
		shootFountain(killed.getLocation());
	}
	public void onLeave(PlayerQuitEvent e)
	{
		super.onLeave(e);
		Game game = this.game;
		Player p = e.getPlayer();
		User u = game.getUser(p);
		
		if(!u.isAlive())
			return;
		
		shootFountain(p.getLocation());
	}
	
	protected void shootFountain(Location l)
	{
		if(isFountainShootingTime())
		{
			Fountain f = new Fountain(l);
			f.shoot();
		}
	}
	public abstract boolean isFountainShootingTime();
	
	public Map getMap()
	{
		return this.map;
	}
	
	public void setMap(HGMap map)
	{
		this.map = map;
	}
}
