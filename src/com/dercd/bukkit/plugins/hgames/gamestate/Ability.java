package com.dercd.bukkit.plugins.hgames.gamestate;

public enum Ability
{
	WORLD_DAMAGE,
	PLAYER_DAMAGE,
	INFINITE_MATCH,
	MAP_DESTROY,
	MOVE,
	SPECTATE,
	OPEN_CHEST,
	USE_ITEMS,
	JOIN,
	ANNOYING_BLOCK_DESTROY
}
