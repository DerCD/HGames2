package com.dercd.bukkit.plugins.hgames.gamestate;

public class StateRunningTime
{
	protected int runningTime;
	
	public StateRunningTime(int runningTime)
	{
		this.runningTime = runningTime;
	}
	
	public int getRunningTime()
	{
		return this.runningTime;
	}
	
	public void setRunningTime(int runningTime)
	{
		this.runningTime = runningTime;
	}
}
