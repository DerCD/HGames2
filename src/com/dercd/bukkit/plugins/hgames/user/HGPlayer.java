package com.dercd.bukkit.plugins.hgames.user;

import org.bukkit.entity.Player;

public class HGPlayer extends User
{
	protected boolean alive;
	
	public HGPlayer(Player bukkitPlayer)
	{
		super(bukkitPlayer);
		this.alive = true;
	}

	public boolean isAlive()
	{
		return this.alive;
	}

	public void setAlive(boolean alive)
	{
		this.alive = alive;
	}
}
