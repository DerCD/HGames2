package com.dercd.bukkit.plugins.hgames.user;

import java.util.UUID;

import org.bukkit.entity.Player;

public abstract class User
{
	public Player bukkitPlayer;
	protected UserGroup userGroup = UserGroup.NORMAL;
	protected String name;
	protected boolean isOnline;
	
	public User(Player bukkitPlayer)
	{
		this.bukkitPlayer = bukkitPlayer;
		if(bukkitPlayer != null)
			this.name = bukkitPlayer.getName();
		this.isOnline = true;
	}
	
	public Player getPlayer()
	{
		return this.bukkitPlayer;
	}
	public UserGroup getUserGroup()
	{
		return this.userGroup;
	}
	public String getName()
	{
		if(this.bukkitPlayer != null)
			this.name = this.bukkitPlayer.getName();
		return this.name;
	}
	public boolean isOnline()
	{
		return this.isOnline;
	}
	public abstract boolean isAlive();
	
	public UUID getUUID()
	{
		Player p = getPlayer();
		if(p == null)
			return null;
		return p.getUniqueId();
	}
	
	public void setUserGroup(UserGroup userGroup)
	{
		this.userGroup = userGroup;
	}
	public void setOnline(boolean isOnline)
	{
		this.isOnline = isOnline;
	}
}
