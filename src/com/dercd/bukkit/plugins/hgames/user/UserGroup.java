package com.dercd.bukkit.plugins.hgames.user;

public enum UserGroup
{
	NORMAL("prefixUser"),
	VIP("prefixVIP"),
	YT("prefixYT"),
	ADMIN("prefixAdmin");
	
	protected String prefixTextId;
	
	private UserGroup(String prefixTextId)
	{
		this.prefixTextId = prefixTextId;
	}
	
	public String getPrefixTextId()
	{
		return this.prefixTextId;
	}
}
