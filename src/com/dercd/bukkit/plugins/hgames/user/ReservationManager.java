package com.dercd.bukkit.plugins.hgames.user;

import java.util.HashSet;
import java.util.UUID;

import com.dercd.bukkit.plugins.hgames.Game;

public class ReservationManager
{
	public HashSet<UUID> reserved = new HashSet<UUID>();
	
	public void addReservation(UUID u)
	{
		this.reserved.add(u);
	}
	public void addReservation(User u)
	{
		if(u == null)
			return;
		
		addReservation(u.getUUID());
	}
	
	public void removeReservation(UUID u)
	{
		this.reserved.remove(u);
	}
	public void removeReservation(User u)
	{
		if(u == null)
			return;
		
		removeReservation(u.getUUID());
	}
	
	public void refreshReservations()
	{
		Game game = Game.getInstance();
		for(User u : game.getUsers())
			if(u.isOnline())
				removeReservation(u);
	}
	
	public boolean hasReservedSlot(UUID u)
	{
		return this.reserved.contains(u);
	}
	public boolean hasReservedSlot(User u)
	{
		if(u == null)
			return false;
		
		return hasReservedSlot(u.getUUID());
	}
}
