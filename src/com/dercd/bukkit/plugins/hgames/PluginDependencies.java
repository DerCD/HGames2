package com.dercd.bukkit.plugins.hgames;

import java.util.HashMap;

public class PluginDependencies extends HashMap<Class, Object>
{
	private static final long serialVersionUID = 5601760445405211958L;

	public <T> T get(Class<T> type)
	{
		Object o = super.get(type);
		return (T)o;
	}
}
