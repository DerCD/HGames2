package com.dercd.bukkit.plugins.hgames.scoreboard;

public enum ScoreboardLineType
{
	PLAYER_COUNT,
	REQUIRED_PLAYER_COUNT,
	TEAMS_ALLOWED,
	CURRENT_MAP
}
