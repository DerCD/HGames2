package com.dercd.bukkit.plugins.hgames.scoreboard;

import java.util.HashMap;

import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard;
import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard.Line;

public class ScoreboardLineMapping extends HashMap<ScoreboardLineType, Line>
{
	private static final long serialVersionUID = -220601989239793933L;
	
	protected CDScoreboard scoreboard;
	
	public ScoreboardLineMapping(CDScoreboard scoreboard)
	{
		this.scoreboard = scoreboard;
	}
	
	
	public CDScoreboard getScoreboard()
	{
		return this.scoreboard;
	}
	public void setScoreboard(CDScoreboard scoreboard)
	{
		this.scoreboard = scoreboard;
	}
}
