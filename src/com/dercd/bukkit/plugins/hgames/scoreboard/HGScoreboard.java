package com.dercd.bukkit.plugins.hgames.scoreboard;

import com.dercd.bukkit.cdapi.tools.minecraft.CDScoreboard;

public class HGScoreboard extends CDScoreboard
{
	protected ScoreboardLineMapping lineMapping;

	public ScoreboardLineMapping getLineMapping()
	{
		return this.lineMapping;
	}
	public void setLineMapping(ScoreboardLineMapping lineMapping)
	{
		this.lineMapping = lineMapping;
	}
}
