package com.dercd.bukkit.plugins.hgames;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.bukkit.ChatColor;

public class TextPool
{
	HashMap<String, TextTemplate> texts;
	
	public static final String DEFAULT_TEXT = "NoText";
	
	public TextPool()
	{
		this.texts = new HashMap<String, TextTemplate>();
	}
	
	public String getText(String id, String... vars)
	{
		TextTemplate textTemplate = this.texts.get(id);
		if(textTemplate == null)
		{
			System.out.println("Warning: Missing text '" + id + "'");
			return DEFAULT_TEXT;
		}
		return textTemplate.getText(vars);
	}
	public String[] getMultiText(String id, String... vars)
	{
		TextTemplate textTemplate = this.texts.get(id);
		if(textTemplate == null)
		{
			System.out.println("Warning: Missing text '" + id + "'");
			return new String[]{ DEFAULT_TEXT };
		}
		return textTemplate.getMultiText(vars);
	}
	
	public String getPrefix()
	{
		return getText("prefix");
	}
	
	
	public void addText(TextTemplate text)
	{
		if(text == null)
			return;
		this.texts.put(text.id, text);
	}
	public void addTexts(TextPool textPool)
	{
		if(textPool == null)
			return;
		if(textPool.texts == null)
			return;
		for(TextTemplate text : textPool.texts.values())
		{
			TextTemplate ourText = text.clone(this);
			addText(ourText);
		}
	}
	
	public static String replaceColorCodes(String text)
	{
		ChatColor[] colors = ChatColor.values();
		for(ChatColor color : colors)
			text = text.replace("§" + color.name() + "§", color.toString());
		return text;
	}
	
	public class TextTemplate
	{
		protected String id;
		protected ArrayList<String> texts;
		protected boolean usePrefix;
		
		public TextTemplate()
		{ }
		public TextTemplate(String textId)
		{
			this.id = textId;
		}
		public TextTemplate(String textId, String textValue)
		{
			this.id = textId;
			addText(textValue);
		}
		
		public void addText(String text)
		{
			text = replaceColorCodes(text);
			if(this.texts == null)
				this.texts = new ArrayList<String>();
			this.texts.add(text);
		}
		public void addText(Collection<? extends String> text)
		{
			for(String line : text)
				addText(line);
		}
		
		public void clear()
		{
			this.texts.clear();
			this.usePrefix = false;
		}
		
		public String getText(String... variables)
		{
			String[] filled = fill(variables);
			return filled[0];
		}
		public String[] getMultiText(String... variables)
		{
			String[] filled = fill(variables);
			return filled;
		}
		
		public String[] fill(String... variables)
		{
			int textCount = this.texts.size();
			String[] filled = new String[textCount];
			for(int i = 0; i < textCount; ++i)
				filled[i] = this.texts.get(i);
			for(int i = 0; i < variables.length; ++i)
			{
				//Use i + 1 because first placeholder is &1
				String search = "&" + (i + 1);
				if(i + 1 >= 10)
					search += "&";
				String replacement = variables[i];
				for(int j = 0; j < filled.length; ++j)
					filled[j] = filled[j].replace(search, replacement);
			}
			for(int j = 0; j < filled.length; ++j)
				filled[j] = filled[j].replaceAll("&\\d{2,}&", "").replaceAll("&\\d", "");
			if(this.usePrefix)
			{
				String prefix = TextPool.this.getPrefix();
				for(int j = 0; j < filled.length; ++j)
					filled[j] = prefix + filled[j];
			}
			return filled;
		}
		
		public String getId()
		{
			return this.id;
		}
		public ArrayList<String> getTexts()
		{
			return this.texts;
		}
		public boolean isUsingPrefix()
		{
			return this.usePrefix;
		}
		public void setId(String id)
		{
			this.id = id;
		}
		public void setTexts(ArrayList<String> texts)
		{
			this.texts = texts;
		}
		public void setUsingPrefix(boolean usePrefix)
		{
			this.usePrefix = usePrefix;
		}	
	
		//Shallow clone
		public TextTemplate clone(TextPool textPool)
		{
			TextTemplate template = textPool.new TextTemplate();
			template.id = this.id;
			template.texts = this.texts;
			template.usePrefix = this.usePrefix;
			
			return template;
		}
	}
}
