package com.dercd.bukkit.plugins;

import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.StructureModifier;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.annotations.CDPluginPacket;
import com.dercd.bukkit.cdapi.tools.Tools.JsonParser;

import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class NoMessage extends CDPlugin
{
	public NoMessage(PluginHandler handler)
	{
		super(handler);
	}
	
	@Override
	public String getVersion()
	{
		return "0.1";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@CDPluginPacket(types = {"schat"})
	public void onEmptyMessage(PacketEvent e)
	{
		PacketContainer packet = e.getPacket();
		StructureModifier<WrappedChatComponent> chat = packet.getChatComponents();
		
		WrappedChatComponent chatComponent = chat.read(0);
		String chatStr = JsonParser.parse(chatComponent.getJson());
		System.out.println("DEBUG: ChatStr: '" + chatStr + "'");
		if(chatStr.replaceAll("§.", "").replace("§",  "").replace(" ", "").isEmpty())
			e.setCancelled(true);
	}
	
	@CDPluginEvent
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		e.setJoinMessage(null);
	}

	@CDPluginEvent
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		e.setQuitMessage(null);
	}

	@CDPluginEvent
	public void onPlayerDeath(PlayerDeathEvent e)
	{
		e.setDeathMessage(null);
	}

	@CDPluginEvent
	public boolean onPlayerKick(PlayerKickEvent e)
	{
		e.setLeaveMessage(null);
		return false;
	}
}