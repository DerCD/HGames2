package com.dercd.bukkit.plugins;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.util.Vector;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDPlayerNotFoundException;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.plugins.Man.ManEntry;

public class BetterJump extends CDPlugin
{
	Log clog;
	String mbeg = ChatColor.LIGHT_PURPLE + "[BetterJump] " + ChatColor.WHITE;
	Map<Runnable, UUID> protect = new HashMap<Runnable, UUID>();

	public BetterJump(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
	}
	
	@Override
	public String getVersion()
	{
		return "0.1a";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@Override
	public Permission[] getPermissions()
	{
		return new Permission[]
		{
			new Permission("cd.bj", PermissionDefault.OP)
		};
	}
	
	@Override
	public ManEntry[] man()
	{
		ManEntry man = new ManEntry("bj", "BetterJump bj");
		man.addEntry("all", "[player] [<width> <height>] [data]");
		return new ManEntry[] { man };
	}
	
	@CDPluginCommand(commands = { "bj cd.bj 1" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException, CDPlayerNotFoundException
	{
		e.validateCount(-1, 4);
		String[] args = e.getArgs();
		Player p = getPlayer(args, e.getSender());
		if (p == null)
			return;
		Double[] data = getData(args);
		if (containsProtectArg(args))
			putRunnable(p);
		doJump(p, data);
	}

	@CDPluginEvent
	public void onEntityDamage(EntityDamageByBlockEvent e)
	{
		if (e.getCause() != DamageCause.FALL)
			return;
		if (!Tools.isPlayer(e.getEntity()))
			return;
		protectUser(e);
	}
	@CDPluginEvent
	public void onEntityDamage(EntityDamageEvent e)
	{
		if (e.getCause() != DamageCause.FALL)
			return;
		if (!Tools.isPlayer(e.getEntity()))
			return;
		protectUser(e);
	}

	private void protectUser(EntityDamageEvent e)
	{
		if (!Tools.isPlayer(e))
			return;
		Player p = (Player) e.getEntity();
		if (this.protect.containsValue(p.getUniqueId()))
		{
			this.clog.log("Cancelling FallDamage for " + p.getName(), this);
			removeRunnable(p);
			e.setCancelled(true);
			this.clog.log("FallDamage for " + p.getName() + " canceled", this);
		}
	}

	protected void putRunnable(Player p)
	{
		this.clog.log("Putting Runnable for " + p.getName(), this);
		Runnable r = getRunnable(p);
		this.protect.put(r, p.getUniqueId());
		this.clog.log("Starting Runnable for " + p.getName(), this);
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this.handler.getMain(), r, 200L);
		this.clog.log("Runnable for " + p.getName() + " started", this);
	}

	private Runnable getRunnable(Player p)
	{
		removeRunnable(p);
		return getRunnable();
	}
	private Runnable getRunnable()
	{
		return new Runnable()
		{
			@Override
			public void run()
			{
				if (!BetterJump.this.protect.containsKey(this))
					return;
				BetterJump.this.clog.log("Fallprotection for " + Bukkit.getOfflinePlayer(BetterJump.this.protect.get(this)).getName() + " timed out", this);
				BetterJump.this.protect.remove(this);
			}
		};
	}

	private void removeRunnable(Player p)
	{
		for (Runnable r : new HashMap<Runnable, UUID>(this.protect).keySet())
			if (this.protect.get(r).equals(p.getUniqueId()))
			{
				this.clog.log("Removing " + p.getName() + " of the Protect list", this);
				this.protect.remove(r);
			}
	}

	public Double[] getDefaultData()
	{
		return new Double[] { 4.375, 0.2 };
	}

	private Player getPlayer(String[] args, CommandSender sender) throws CDPlayerNotFoundException
	{
		if (args.length == 0 || Tools.isDouble(args[0]) || args[0].equals("true"))
			return (Player) sender;
		Player p = Bukkit.getServer().getPlayer(args[0]);
		if (p == null)
			throw new CDPlayerNotFoundException(args[0]);
		return p;
	}

	private boolean containsProtectArg(String[] args)
	{
		for (int i = 0; i < args.length; i++)
			if(!Tools.isDouble(args[i]) && Tools.convertToBool(args[i]) == true)
				return true;
		return false;
	}

	private Double[] getData(String[] args)
	{
		for (int i = 0; i < args.length - 1; i++)
			if (Tools.isDouble(args[i]) && Tools.isDouble(args[i + 1]))
				return getData(args, i);
		return getDefaultData();
	}
	private Double[] getData(String[] args, int start)
	{
		Double[] d;
		if ((d = getDoubles(args, start)) == null)
			return getDefaultData();
		return d;
	}
	private Double[] getDoubles(String[] args, int start)
	{
		if (!Tools.isDouble(args[start]) || !Tools.isDouble(args[start + 1]))
			return null;
		return new Double[] { Double.valueOf(args[start]), Double.valueOf(args[start + 1]) };
	}

	public void doJump(Player e, Double[] d)
	{
		this.clog.log("Jump " + e.getName() + " with h = " + d[1] + ", l = " + d[0], this);
		Location direction = e.getLocation().clone();
		direction.setPitch(-20F);
		doJump(e, d, direction.getDirection());
	}
	public void doJump(Player p, Double[] d, Vector v)
	{
		p.setVelocity(v.setY(d[1]).multiply(d[0]));
	}
}
