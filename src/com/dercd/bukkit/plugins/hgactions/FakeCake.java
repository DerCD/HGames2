package com.dercd.bukkit.plugins.hgactions;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.ObjHolder;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

import net.minecraft.server.v1_11_R1.EntityArrow;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitTask;

public class FakeCake extends HGAction
{
	Map<Location, Player> places = new HashMap<Location, Player>();
	
	@Override
	public void onEnable()
	{
		this.hga.events.get(PlayerInteractEvent.class).add((e) -> { onInteract((PlayerInteractEvent) e); });
//		Boom-Bed
		this.hga.events.get(ProjectileHitEvent.class).add((e) -> { onProjectileHit((ProjectileHitEvent) e); });
//		End Boom-Bed
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.ALLOW;
		Block b = e.getClickedBlock().getRelative(e.getBlockFace());
		this.places.put(b.getLocation(), p);
		b.setType(Material.CAKE_BLOCK);
		b.setData((byte) 0);
		return ItemAction.REMOVE_CANCEL;
	}
	
	public void onInteract(PlayerInteractEvent e)
	{
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;		
		
		Block block = e.getClickedBlock();
		if(block.getType() != Material.CAKE_BLOCK)
			return;
		
		Location loc = block.getLocation();
		if(!this.places.containsKey(loc))
			return;
		
		e.setCancelled(true);
		Player p = e.getPlayer();
		if(this.places.get(loc) == p && !p.isSneaking())
		{
			eatCake(block, p);
		}
		else
		{
			this.places.remove(loc);
			block.setType(Material.AIR);
			
			boolean worldDamage = shouldMakeWorldDamage();
			loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), 3, true, worldDamage);
		}
	}
	
	@SuppressWarnings("deprecation")
	public void eatCake(Block b, Player p)
	{
		Tools.Food.CAKE_PIECE.applyToPlayer(p);
		byte tileId = b.getData();
		if(tileId >= 6)
			b.setType(Material.AIR);
		else
			b.setData((byte) (tileId + 1));
	}
	
//	Boom-Bed
	public void onProjectileHit(ProjectileHitEvent e)
	{
		if(!(e.getEntity() instanceof Arrow)) return;
		CraftArrow a = (CraftArrow) e.getEntity();
		EntityArrow ea = a.getHandle();
		try
		{
			Field f = ea.getClass().getDeclaredField("d");
			f.setAccessible(true);
			long start = Tools.getTimestamp();
			ObjHolder<BukkitTask> bukkitTaskHolder = new ObjHolder<BukkitTask>();
			bukkitTaskHolder.obj = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () -> {
				if(Tools.getTimestamp() - start >= 2)
					bukkitTaskHolder.obj.cancel();
				try
				{
					if(!ea.isAlive())
					{
						bukkitTaskHolder.obj.cancel();
					}
					else if(f.get(ea) != null)
					{
						Location l = new Location(a.getLocation().getWorld(), -1, -1, -1);
						l.setX((int) f.get(ea));
						
						Field fl = ea.getClass().getDeclaredField("e");
						fl.setAccessible(true);
						l.setY((int) fl.get(ea));
						
						fl = ea.getClass().getDeclaredField("f");
						fl.setAccessible(true);
						l.setZ((int) fl.get(ea));
						
						if(this.places.remove(l) != null)
						{
							l.getBlock().setType(Material.AIR);
							
							boolean worldDamage = shouldMakeWorldDamage();
							l.getWorld().createExplosion(l.getX(), l.getY(), l.getZ(), 3, true, worldDamage);
						}
						bukkitTaskHolder.obj.cancel();
					}
				}
				catch (Exception x)
				{
					bukkitTaskHolder.obj.cancel();
				}
			}, 0, 5);
		}
		catch(Exception x)
		{
		}
	}
//	End Boom-Bed
	
	protected boolean shouldMakeWorldDamage()
	{
		Game game = this.game;
		GameState state = game.getCurrentState();
		
		return state instanceof DMPlayingState && !(state instanceof CountdownState);
	}
}
