package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public class IceBall extends HGAction
{
	@Override
	public void onEnable()
	{
		this.hga.events.get(EntityDamageByEntityEvent.class).add((e) -> { onDamage((EntityDamageByEntityEvent) e); });
	}
	
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Snowball s = p.launchProjectile(Snowball.class);
		s.setMetadata("IceBall", new FixedMetadataValue(CDAPI.getInstance(), "CDAPI"));
		return ItemAction.REMOVE_CANCEL;
	}
	
	
	public void onDamage(EntityDamageByEntityEvent e)
	{
		if(e.getEntity() instanceof LivingEntity && e.getDamager().hasMetadata("IceBall"))
		{
			LivingEntity livingEntity = ((LivingEntity)e.getEntity());
			PotionEffect potionEffect = new PotionEffect(PotionEffectType.SLOW, 200, 1, false);
			livingEntity.addPotionEffect(potionEffect, true);
		}
	}
}
