package com.dercd.bukkit.plugins.hgactions;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;
import com.dercd.bukkit.plugins.hgames.gamestate.HGPlayingState;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class Fireball extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = Game.getInstance();
		GameState state = game.getCurrentState();
		
		if(state instanceof CountdownState)
			return ItemAction.CANCEL;
		
		Location l = p.getLocation();
		org.bukkit.entity.Fireball f = p.launchProjectile(org.bukkit.entity.Fireball.class);
		if(state instanceof HGPlayingState)
			f.setMetadata("playerTnt", new FixedMetadataValue(CDAPI.getInstance(), "CDAPI"));
		f.setDirection(l.getDirection());
		f.setVelocity(l.getDirection());
		f.setIsIncendiary(false);
		f.setYield(2.5F);
		f.setBounce(false);
		return ItemAction.REMOVE_CANCEL;
	}
}
