package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.user.User;

public class Speed extends HGAction
{
	@Override
	public ItemAction run(final Player p, PlayerInteractEvent e)
	{
		Game game = Game.getInstance();
		
		PotionEffect potionEffect = new PotionEffect(PotionEffectType.SPEED, 1800, 1, false); 
		p.addPotionEffect(potionEffect, true);
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), new Runnable()
		{
			@Override
			public void run()
			{
				User u = game.getUser(p);
				if(!u.isAlive())
					return;
				
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 200, 0, true), true);
				p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 200, 0, true), true);
				p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 100, 4, true), true);
			}
		}, 1800);
		return ItemAction.REMOVE_CANCEL;
	}
}
