package com.dercd.bukkit.plugins.hgactions;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.BetterJump;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

import org.bukkit.craftbukkit.v1_11_R1.entity.CraftEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

public class Charge extends HGAction
{
	BetterJump bj;

	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = this.game;
		GameState state = game.getCurrentState();
		
		if(state instanceof CountdownState)
			return ItemAction.CANCEL;
		
		if(this.bj == null)
			this.bj = CDAPI.getInstance().getHandler().getPlugin(BetterJump.class);
		
		CraftEntity craftPlayer = ((CraftEntity) p);
		craftPlayer.getHandle().fallDistance = 0F;
		
		this.bj.doJump(p, new Double[] { 3D, 0.16 });
		
		return ItemAction.REMOVE_CANCEL;
	}
}
