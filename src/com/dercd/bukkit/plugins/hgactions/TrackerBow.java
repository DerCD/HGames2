package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.Effect;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;

public class TrackerBow extends HGAction
{
//	SpawnWolf sw;
	
	@Override
	public void onEnable()
	{
//		this.sw = (SpawnWolf) this.hga.actions.get(SpawnWolf.class);
		this.hga.events.get(EntityShootBowEvent.class).add((e) ->
		{
			onShoot((EntityShootBowEvent) e);
		});
	}
	
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		return ItemAction.ALLOW;
	}

	public void onShoot(EntityShootBowEvent e)
	{
		Game game = Game.getInstance();
		
		if(!(e.getEntity() instanceof Player) || !(e.getProjectile() instanceof Arrow))
			return;
		Player p = (Player) e.getEntity();
		if(this.trigger == null) return;
		ItemStack cloned = this.trigger.clone();
		ItemStack used = e.getBow();
		cloned.setDurability(used.getDurability());
		if(!used.equals(cloned)) return;
		double minAngle = 6.283185307179586D;
		Entity minEntity = null;
		for(Entity en : p.getNearbyEntities(64.0D, 64.0D, 64.0D))
		{
			if(
				(
					en.getType() == EntityType.PLAYER
//					|| en.getType() == EntityType.WOLF
				)
				&& !en.isDead()
//				&&
//				(
//					!(en instanceof Wolf)
//					|| !this.sw.wolves.get(p.getUniqueId()).contains(en)
//				)
				&&
				(
					!(en instanceof Player)
					|| game.getUser((Player)en).isAlive()
				)
				)
			{
				Vector toTarget = en.getLocation().toVector().clone().subtract(p.getLocation().toVector());
				double angle = e.getProjectile().getVelocity().angle(toTarget);
				if (angle < minAngle)
				{
					minAngle = angle;
					minEntity = en;
				}
			}
		}
		if(minEntity != null)
			new Tracker((Arrow) e.getProjectile(), (LivingEntity) minEntity, CDAPI.getInstance());
	}
}

class Tracker extends BukkitRunnable
{
	Arrow arrow;
	LivingEntity target;

	public Tracker(Arrow arrow, LivingEntity target, Plugin plugin)
	{
		this.arrow = arrow;
		this.target = target;
		runTaskTimer(plugin, 1L, 1L);
	}

	@Override
	public void run()
	{
		try
		{
			double speed = this.arrow.getVelocity().length();
			if ((this.arrow.isOnGround()) || (this.arrow.isDead()) || (this.target.isDead()))
			{
				cancel();
				return;
			}
			Vector toTarget = this.target.getLocation().clone().add(new Vector(0.0D, 0.5D, 0.0D)).subtract(this.arrow.getLocation()).toVector();
			Vector dirVelocity = this.arrow.getVelocity().clone().normalize();
			Vector dirToTarget = toTarget.clone().normalize();
			double angle = dirVelocity.angle(dirToTarget);
			double newSpeed = 0.9D * speed + 0.14D;
			if (((this.target instanceof Player)) && (this.arrow.getLocation().distance(this.target.getLocation()) < 8.0D))
			{
				Player player = (Player) this.target;
				if (player.isBlocking()) newSpeed = speed * 0.6D;
			}
			Vector newVelocity;
			if (angle < 0.12D)
			{
				newVelocity = dirVelocity.clone().multiply(newSpeed);
			}
			else
			{
				Vector newDir = dirVelocity.clone().multiply((angle - 0.12D) / angle).add(dirToTarget.clone().multiply(0.12D / angle));
				newDir.normalize();
				newVelocity = newDir.clone().multiply(newSpeed);
			}
			this.arrow.setVelocity(newVelocity.add(new Vector(0.0D, 0.03D, 0.0D)));
			this.arrow.getWorld().playEffect(this.arrow.getLocation(), Effect.SMOKE, 0);
		}
		catch (Exception x)
		{
			this.cancel();
		}
	}
}
