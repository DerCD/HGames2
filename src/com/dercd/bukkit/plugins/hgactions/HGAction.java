package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;

public abstract class HGAction
{
	ItemStack trigger;
	Game game;
	HGActions hga;
	
	public HGAction()
	{
		this.game = Game.getInstance();
		this.hga = HGActions.getInstance();
	}
	
	public void onEnable()
	{
	}
	
	public boolean isNonItemAction()
	{
		return false;
	}
	
	
	public abstract ItemAction run(Player p, PlayerInteractEvent e);
	
	public void setItem(ItemStack item)
	{
		this.trigger = item;
	}
}
