package com.dercd.bukkit.plugins.hgactions;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public class SandSurprise extends HGAction
{
	@SuppressWarnings("deprecation")
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Block b = p.getLocation().clone().add(0, 4, 0).getBlock();
		Set<Block> blocks = new HashSet<Block>();
		blocks.add(b);
		blocks.addAll(Tools.getAdjacentBlocks(b, 1, 2));
		
		for(Block current : new HashSet<Block>(blocks))
		{
			Location loc = current.getLocation();
			int blockX = loc.getBlockX();
			int blockZ = loc.getBlockZ();
			
			//By using XOR we create a checkered pattern
			boolean pattern = blockX % 2 == 0 ^ blockZ % 2 == 0;
			
			if(pattern)
				blocks.add(current.getRelative(BlockFace.UP));
		}
		for(Block current : blocks)
		{
			if(current.getType().isSolid())
				continue;
			current.setData((byte) 0);
			current.setType(Material.SAND);
		}
		return ItemAction.REMOVE_CANCEL;
	}
}
