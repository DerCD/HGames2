package com.dercd.bukkit.plugins.hgactions;

import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;
import com.dercd.bukkit.plugins.hgames.gamestate.RunningGameState;
import com.dercd.bukkit.plugins.hgames.map.Map;
import com.dercd.bukkit.plugins.hgames.map.chest.Chest;
import com.dercd.bukkit.plugins.hgames.map.chest.ChestManager;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SpawnChest extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		GameState currentState = game.getCurrentState();
		
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.ALLOW;
		
		if(!(currentState instanceof RunningGameState))
		{
			p.sendMessage(textPool.getMultiText("hgaspDeniedGamestate"));
			return ItemAction.CANCEL;
		}
		
		Block b = e.getClickedBlock();
		if(!b.getType().isSolid())
		{
			p.sendMessage(textPool.getMultiText("hgaspDeniedNonSolid"));
			return ItemAction.CANCEL;
		}
		if(b.getType() == Material.CHEST)
		{
			p.sendMessage(textPool.getMultiText("hgaspDeniedBlockIsChest"));
			return ItemAction.CANCEL;
		}
		
		
		RunningGameState runningGameState = (RunningGameState)currentState;
		Map map = runningGameState.getMap();
		ChestManager chestManager = map.getChestManager();
		Chest chest = new Chest(e.getClickedBlock(), map, false);
		chestManager.placeChest(chest, true);
		return ItemAction.REMOVE_CANCEL;
	}
}
