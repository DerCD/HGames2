package com.dercd.bukkit.plugins.hgactions;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDInternPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCMessage;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.tools.DynamicClassLoader;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.collection.PresetMap;
import com.dercd.bukkit.cdapi.tools.collection.SyncedMap;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.plugins.BetterJump;
import com.dercd.bukkit.plugins.Man.ManEntry;
import com.dercd.bukkit.plugins.RemoveItemInHand;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.HGames2;
import com.dercd.bukkit.plugins.hgames.IOManager;
import com.dercd.bukkit.plugins.hgames.TextPool;

import net.minecraft.server.v1_11_R1.NBTTagCompound;

@CDInternPluginDepend(depends = { RemoveItemInHand.class, BetterJump.class, HGames2.class }, softdepends = {})
public class HGActions extends CDPlugin
{
	public SyncedMap<ItemStack, HGAction> actions;
	public Set<HGAction> nonItemActions;
	public Map<Class<? extends HGAction>, HGAction> loadedActions;
	public Map<Class<? extends Event>, Set<EventReceiver>> events = new PresetMap<Class<? extends Event>, Set<EventReceiver>>(() -> { return new HashSet<EventReceiver>(); });
	
	private RemoveItemInHand riih;
	protected Game game;
	
	protected String nestedMainDirectory;
	
	private static HGActions instance;
	
	private Log clog;

	public HGActions(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
		HGActions.instance = this;
	}

	public enum ItemAction
	{
		CANCEL,
		REMOVE,
		ALLOW,
		REMOVE_CANCEL;
	}
	
	@Override
	public String getVersion()
	{
		return "0.2";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@Override
	public ManEntry[] man()
	{
		ManEntry man = new ManEntry("hga", "HGActions");
		man.addEntry("save", "save");
		man.addEntry("load", "load");
		man.addEntry("set", "set <action>");
		man.addEntry("actions", "actions");
		man.addEntry("get", "get <action>");
		return new ManEntry[] { man };
	}
	
	@CDPluginEvent(priority = 9000)
	public void onEnable(CDPluginEnableEvent e) throws CDPluginNotFoundException, IOException
	{
		this.game = Game.getInstance();
		
		HGames2 hg2 = this.game.hgames2;
		this.nestedMainDirectory = Paths.get(hg2.getAbsoluteDirectories()[0], "hgactions").toString();
		
		loadTexts();
		
		this.clog.log("Getting 'RemoveItemInHand'", this);
		this.riih = getDepend(RemoveItemInHand.class);
		this.clog.log("Success", this);
		this.clog.log("Getting 'HGames'", this);
		this.game = Game.getInstance();
		this.clog.log("Success", this);
		this.clog.log("Loading Items", this);
		load();
		this.clog.log("Success", this);
		this.clog.log("Calling enable-method of loaded HGActions", this);
		for (HGAction hga : this.loadedActions.values())
			hga.onEnable();
		this.clog.log("Done", this);
	}

	@CDPluginCommand(commands = { "hga cd.hg.hga 1" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException, CDNoPermissionException, CDCMessage
	{
		String[] args = e.getArgs();
		e.validateCount(1, 2);
		switch (args[0].toLowerCase())
		{
			case "save":
				e.validateCount(1);
				saveData(e.getSender());
				return;
			case "load":
				e.validateCount(1);
				loadData(e.getSender());
				return;
			case "set":
				e.validateCount(2);
				setActionItem((Player) e.getSender(), args[1]);
				return;
			case "actions":
				e.validateCount(1);
				showActions(e.getSender());
				return;
			case "get":
				e.validateCount(2);
				processGetCmd(e);
				return;
		}
		throw new CDInvalidArgsException();
	}
	
	
	public HGAction getAction(String name)
	{
		for (HGAction action : this.loadedActions.values())
			if (action.getClass().getSimpleName().equalsIgnoreCase(name))
				return action;
		return null;
	}
	
	public void processGetCmd(CommandEvent e) throws CDCMessage
	{
		Game game = Game.getInstance();
		TextPool textPool = game.getTextPool();
		
		String name = e.getArgs()[1];
		HGAction hga = getAction(name);
		if(hga == null)
			throw new CDCMessage(textPool.getText("hgaNameNotFound"));
		
		ItemStack i = this.actions.getOther(hga);
		if(i == null)
			throw new CDCMessage(textPool.getText("hgaNoItemFound"));
		
		Player p = (Player) e.getSender();
		if(p.getInventory().firstEmpty() == -1)
			throw new CDCMessage(textPool.getText("hgaInventoryFull"));
		
		p.getInventory().addItem(i);
	}
	
	public boolean hasAction(String name)
	{
		for(Class<? extends HGAction> clazz : this.loadedActions.keySet())
			if(clazz.getSimpleName().equals(name)) return true;
		return false;
	}
	
	@CDPluginEvent(priority = 8000)
	public void onInteractLate(PlayerInteractEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onMove(PlayerMoveEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onDamage(EntityDamageByEntityEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onDamage(EntityDamageByBlockEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onDeath(PlayerDeathEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onPickup(PlayerPickupItemEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onShoot(EntityShootBowEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onLaunch(ProjectileLaunchEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onProjectileHit(ProjectileHitEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onJoin(PlayerJoinEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onQuit(PlayerQuitEvent e)
	{
		onEvent(e);
	}
	@CDPluginEvent
	public void onExplode(EntityExplodeEvent e)
	{
		if(e.getEntity().hasMetadata("playerTnt"))
			e.blockList().clear();
	}
//	@CDPluginEvent
//	public void onUDSave(HGUserdataSaveEvent e)
//	{
//		onEvent(e);
//	}
//	@CDPluginEvent
//	public void onUDLoad(HGUserdataLoadEvent e)
//	{
//		onEvent(e);
//	}
//	@CDPluginEvent
//	public void onTeleport(HGTeleportEvent e)
//	{
//		onEvent(e);
//	}
	
	public void onEvent(Event e)
	{
		if(!this.events.containsKey(e.getClass())) return;
		for(EventReceiver er : this.events.get(e.getClass()))
			er.onEvent(e);
	}
	
	public void showActions(CommandSender sender)
	{
		StringBuilder sb = new StringBuilder();
		for (HGAction hga : this.loadedActions.values())
		{
			if (sb.length() != 0) sb.append(", ");
			sb.append((this.actions.containsValue(hga)
					? ChatColor.GREEN
					: this.nonItemActions.contains(hga)
						? ChatColor.GOLD
						: ChatColor.RED)
					+ hga.getClass().getSimpleName() + ChatColor.WHITE);
		}
		sender.sendMessage(Tools.getExclamation(ChatColor.GREEN) + sb.toString());
	}

	public void setActionItem(Player p, String name) throws CDCMessage, CDNoPermissionException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		ItemStack i = p.getInventory().getItemInMainHand();
		
		if(i == null || i.getType() == Material.AIR)
			throw new CDCMessage(textPool.getText("hgaMissingItemInHand"));
		
		HGAction hga = getAction(name);
		if(hga == null)
			throw new CDCMessage(textPool.getText("hgaNameNotFound"));
		
		removeAction(hga);
		this.actions.put(i.clone(), hga);
		
		p.sendMessage(textPool.getMultiText("hgaItemSet"));
		saveData(p);
	}

	public void removeAction(HGAction hga)
	{
		for(ItemStack item : new HashSet<ItemStack>(this.actions.keySet()))
			if(this.actions.get(item) == hga)
				this.actions.remove(item);
	}

	private void loadClasses()
	{
		DynamicClassLoader<HGAction> classLoader = new DynamicClassLoader<HGAction>(this.clog);
		this.loadedActions = new HashMap<Class<? extends HGAction>, HGAction>();
		for(HGAction hga : classLoader.loadDir(
				this.handler.getClassLoader().getFileOfPlugin(this.getClass()).getParentFile(),
				HGAction.class,
				new Class<?>[0]
				))
			this.loadedActions.put(hga.getClass(), hga);
		this.clog.log(this.loadedActions.size() + " HGActions found", this);
	}
	
	protected void loadTexts() throws IOException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		IOManager ioManager = (IOManager)game.getIOManager();
		
		File textFile = Paths.get(this.nestedMainDirectory, "hga_lang.json").toFile();
		String textPath = textFile.toString();
		if(!textFile.exists())
			ioManager.extractResource("/hga_lang.json", textPath);
		
		TextPool hgaTexts = ioManager.loadTextPool(textPath);
		textPool.addTexts(hgaTexts);
	}

	@CDPluginEvent(priority = 9000, ignoreCancelled = true)
	public void onInteract(PlayerInteractEvent e)
	{
		Action a = e.getAction();
		if(a != Action.RIGHT_CLICK_BLOCK && a != Action.RIGHT_CLICK_AIR)
			return;
		if(e.isCancelled() && a != Action.RIGHT_CLICK_AIR)
			return;
		executeAction(e.getPlayer(), e, a);
	}

	public void executeAction(Player p, PlayerInteractEvent e, Action a)
	{
		ItemStack i = e.getItem();
		if(i == null || i.getType() == Material.AIR)
			return;
		
		i = i.clone();
		i.setAmount(1);
		
		if(!this.actions.containsKey(i))
			return;
		
		HGAction hga = this.actions.get(i);
		if(hga == null)
			return;
		
		switch (hga.run(p, e))
		{
			case ALLOW:
				break;
			case REMOVE_CANCEL:
				e.setCancelled(true);
			case REMOVE:
				this.riih.removeItem(p, e);
				break;
			case CANCEL:
				e.setCancelled(true);
				break;
		}
	}

	public void loadData(CommandSender sender) throws CDNoPermissionException
	{
		Game game = Game.getInstance();
		TextPool textPool = game.getTextPool();
		
		if(!sender.hasPermission("cd.hga2.io"))
			throw new CDNoPermissionException(true);
		try
		{
			load();
			sender.sendMessage(textPool.getText("hgbDataLoaded"));
		}
		catch (Exception x)
		{
			sender.sendMessage(textPool.getText("hgbDataLoadError"));
		}
	}
	public void load() throws IOException
	{
		this.clog.log("Loading classes", this);
		loadClasses();
		this.clog.log("Loading items", this);
		this.actions = new SyncedMap<ItemStack, HGAction>();
		this.nonItemActions = new HashSet<HGAction>();
		
		String path = Paths.get(this.nestedMainDirectory, "hgactions.dat").toString();
		NBTTagCompound base = Tools.load(path, this);
		if(base == null)
			return;
		
		NBTTagCompound item;
		ItemStack i;
		String name;
		for (HGAction hga : this.loadedActions.values())
		{
			name = hga.getClass().getSimpleName();
			if(hga.isNonItemAction())
			{
				this.clog.log("HGAction '" + name + "' is a nonItemAction", this);
				this.nonItemActions.add(hga);
				continue;
			}
			this.clog.log("Loading item '" + name + "'", this);
			if (!base.hasKey(name))
			{
				this.clog.log("Item with name '" + name + "' not found", this);
				continue;
			}
			item = base.getCompound(name);
			if ((i = Tools.readItem(item)) != null)
			{
				this.actions.put(i, hga);
				hga.setItem(i);
			}
		}
	}
	public void saveData(CommandSender sender) throws CDNoPermissionException
	{
		Game game = Game.getInstance();
		TextPool textPool = game.getTextPool();
		
		if(!sender.hasPermission("cd.hga2.io"))
			throw new CDNoPermissionException(true);
		
		try
		{
			save();
			sender.sendMessage(textPool.getText("hgaDataSaved"));
		}
		catch (Exception x)
		{
			sender.sendMessage(textPool.getText("hgaDataSaveError"));
		}
	}
	public void save() throws IOException
	{
		NBTTagCompound base = new NBTTagCompound();
		for (ItemStack i : this.actions.keySet())
			base.set(this.actions.get(i).getClass().getSimpleName(), Tools.writeItem(i));
		
		String path = Paths.get(this.nestedMainDirectory, "hgactions.dat").toString();
		Tools.save(base, path, this);
	}

	public static HGActions getInstance()
	{
		return HGActions.instance;
	}
}
