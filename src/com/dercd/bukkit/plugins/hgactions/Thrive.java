package com.dercd.bukkit.plugins.hgactions;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.ObjHolder;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

public abstract class Thrive extends HGAction
{
	private static Map<Player, Long> thrown = new HashMap<Player, Long>();
	private int multiplier;

	public Thrive(int multiplier)
	{
		this.multiplier = multiplier;
	}

	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = Game.getInstance();
		GameState state = game.getCurrentState();
		TextPool textPool = game.getTextPool();
		
		if(state instanceof CountdownState)
			return ItemAction.CANCEL;
		
		long time = Tools.getTimestamp();
		Long l = Thrive.thrown.get(p);
		if(l != null && time - l < 2)
		{
			p.sendMessage(textPool.getMultiText("hgathrivecooldown"));
			return ItemAction.CANCEL;
		}
		
		Thrive.thrown.put(p, time);
		
		ItemMeta meta;
		ItemStack item = e.getItem();
		if(item.hasItemMeta())
			meta = item.getItemMeta().clone();
		else
			meta = null;
		
		Material m = e.getItem().getType();
		Arrow a = p.launchProjectile(Arrow.class, p.getLocation().clone().getDirection().normalize().multiply(this.multiplier));
		((CraftArrow) a).getHandle().c(1.2D);
		a.setBounce(false);
		
		ObjHolder<BukkitTask> bukkitTaskHolder = new ObjHolder<BukkitTask>();
		bukkitTaskHolder.obj = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
		{
			if(a.isValid() && a.isOnGround())
			{
				bukkitTaskHolder.obj.cancel();
				
				a.remove();
				
				ItemStack i = new ItemStack(m);
				i.setItemMeta(meta);
				p.getWorld().dropItem(a.getLocation(), i);
			}
		}, 10, 5);
		return ItemAction.REMOVE_CANCEL;
	}
}
