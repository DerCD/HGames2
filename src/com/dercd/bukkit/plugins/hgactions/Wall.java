package com.dercd.bukkit.plugins.hgactions;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.minecraft.CDBlock;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public class Wall extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.CANCEL;
		
		Block b = e.getClickedBlock().getRelative(e.getBlockFace());
		float yaw = p.getLocation().getYaw();
		if(yaw < -180)
			yaw += 360;
		float f = Math.abs(yaw) / 90F;
		
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
		{
			Block rel;
			Set<CDBlock> setBlocks = new HashSet<CDBlock>();
			if(f > 0.5 && f < 1.5)
			{
				for (int y = 0; y <= 2; y++)
					for (int z = -2; z <= 2; z++)
					{
						rel = b.getRelative(0, y, z);
						if(rel.getType() == Material.CHEST)
							continue;
						
						setBlocks.add(CDBlock.fromBlock(rel));
						rel.setType(Material.OBSIDIAN);
					}
			}
			else
			{
				for(int y = 0; y <= 2; y++)
					for(int x = -2; x <= 2; x++)
					{
						rel = b.getRelative(x, y, 0);
						if(rel.getType() == Material.CHEST)
							continue;
						
						setBlocks.add(CDBlock.fromBlock(rel));
						rel.setType(Material.OBSIDIAN);
					}
			}
			Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
			{
				for(CDBlock cdb : setBlocks)
					cdb.place();
			}, 200);
		}, 20);
		return ItemAction.REMOVE_CANCEL;
	}
}
