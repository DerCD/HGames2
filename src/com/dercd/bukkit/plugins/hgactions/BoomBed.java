package com.dercd.bukkit.plugins.hgactions;

import java.lang.reflect.Field;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftArrow;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitTask;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.ObjHolder;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.minecraft.CDBlock;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;
import com.dercd.bukkit.plugins.hgblocks.HGBlocks;

import net.minecraft.server.v1_11_R1.EntityArrow;

public class BoomBed extends HGAction
{
	Set<CDBlock> triggers;
	
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		return ItemAction.ALLOW;
	}
	
	@Override
	public boolean isNonItemAction()
	{
		return true;
	}
	
	@Override
	public void onEnable()
	{
		this.hga.events.get(ProjectileHitEvent.class).add((e) -> { onProjectileHit((ProjectileHitEvent) e); });
	}
	
	@SuppressWarnings("deprecation")
	public void onProjectileHit(ProjectileHitEvent e)
	{
		Game game = Game.getInstance();
		
		if(this.triggers == null)
			this.triggers = CDAPI.getInstance().getHandler().getPlugin(HGBlocks.class).getHGBlock("BoomBed").getTriggers();
		
		if(!(e.getEntity() instanceof Arrow))
			return;
		
		CraftArrow a = (CraftArrow) e.getEntity();
		EntityArrow entityArrow = a.getHandle();
		try
		{
			//X field
			Field field = EntityArrow.class.getDeclaredField("h");
			field.setAccessible(true);
			long start = Tools.getTimestamp();
			
			ObjHolder<BukkitTask> bukkitTaskHolder = new ObjHolder<BukkitTask>();
			bukkitTaskHolder.obj = Bukkit.getScheduler().runTaskTimer(CDAPI.getInstance(), () ->
			{
				if(Tools.getTimestamp() - start >= 3)
					bukkitTaskHolder.obj.cancel();
				try
				{
					if(!entityArrow.isAlive())
					{
						bukkitTaskHolder.obj.cancel();
					}
					else if(field.get(entityArrow) != null)
					{
						Location loc = new Location(a.getLocation().getWorld(), -1, -1, -1);
						loc.setX((int)field.get(entityArrow));
						
						//Y field
						Field fld = EntityArrow.class.getDeclaredField("at");
						fld.setAccessible(true);
						loc.setY((int)fld.get(entityArrow));
						
						//Z field
						fld = EntityArrow.class.getDeclaredField("au");
						fld.setAccessible(true);
						loc.setZ((int)fld.get(entityArrow));
						
						if(this.triggers.contains(CDBlock.fromMaterial(loc.getBlock().getType(), loc.getBlock().getData())))
						{
							boolean doTileDrops = loc.getWorld().getGameRuleValue("doTileDrops").equals("true");
							if(doTileDrops)
								loc.getWorld().setGameRuleValue("doTileDrops", "false");
							loc.getBlock().setType(Material.AIR);
							if(doTileDrops)
								loc.getWorld().setGameRuleValue("doTileDrops", "true");
							
							GameState currentState = game.getCurrentState();
							boolean worldDamage = currentState instanceof DMPlayingState && !(currentState instanceof CountdownState);
							loc.getWorld().createExplosion(loc.getX(), loc.getY(), loc.getZ(), 3, true, worldDamage);
						}
						bukkitTaskHolder.obj.cancel();
					}
				}
				catch (Exception x)
				{
					bukkitTaskHolder.obj.cancel();
					CDAPI.getInstance().getHandler().getCLog().error(x.toString(), this);
				}
			}, 0, 5);
		}
		catch(Exception x)
		{ }
	}
}
