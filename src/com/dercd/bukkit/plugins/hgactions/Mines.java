/*package com.dercd.bukkit.plugins.hgactions;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dercd.bukkit.cdapi.tools.Tools;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;

public class Mines //extends HGAction
{
	Set<Location> mines = new HashSet<Location>();
	private static PacketContainer particles1 = new PacketContainer(PacketType.Play.Server.WORLD_PARTICLES);
	private static PacketContainer particles2 = new PacketContainer(PacketType.Play.Server.WORLD_PARTICLES);
	Lock l = new ReentrantLock();
	static
	{
		particles1.getStrings().write(0, "fireworksSpark");
		particles1.getIntegers().write(0, 1);
		particles1.getFloat().write(3, 0.2F);
		particles1.getFloat().write(4, 0F);
		particles1.getFloat().write(5, 0.15F);
		particles2.getStrings().write(0, "footstep");
		particles2.getIntegers().write(0, 1);
		particles2.getFloat().write(3, 0.23F);
		particles2.getFloat().write(4, 0F);
		particles2.getFloat().write(5, 0.23F);
	}
	
	//
	HGActions hga;
	//

	public Mines()
	{
		Bukkit.getScheduler().runTaskTimerAsynchronously(hg.getHandler().getMain(), () -> { update(); }, 70, 15);
	}
	
//	@Override
	public void onEnable()
	{
		this.hga.events.get(PlayerMoveEvent.class).add((e) -> { onMove((PlayerMoveEvent) e); });
	}
	
//	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.CANCEL;
		Block b = e.getClickedBlock();
		final Location l = b.getLocation().clone().add(0, 1, 0);
		if (!b.getType().isSolid())
		{
			p.sendMessage(HGames.mbeg + ChatColor.RED + "You can place mines only on solid blocks");
			return ItemAction.CANCEL;
		}
		if (e.getBlockFace() != BlockFace.UP)
		{
			p.sendMessage(HGames.mbeg + ChatColor.RED + "You have to place the mine on top of the block");
			return ItemAction.CANCEL;
		}
		if (this.mines.contains(l))
		{
			p.sendMessage(HGames.mbeg + ChatColor.RED + "There is already a mine");
			return ItemAction.CANCEL;
		}
		Bukkit.getScheduler().runTaskLater(this.hg.getHandler().getMain(), () ->
		{
			l.getBlock().setType(Material.AIR);
			Mines.this.l.lock();
			try
			{
				Mines.this.mines.add(l);
			}
			finally
			{
				Mines.this.l.unlock();
			}
		}, 35);
		ItemStack i = e.getItem();
		if (i.getType().isBlock()) l.getBlock().setType(i.getType());
		else l.getBlock().setType(Material.STONE_PLATE);
		return ItemAction.REMOVE_CANCEL;
	}

	public void onMove(PlayerMoveEvent e)
	{
		Location block = Tools.getBlockLoc(e.getTo());
		if (this.mines.contains(block))
		{
			if (!this.hg.dm.living.contains(e.getPlayer())) return;
			block.getWorld().createExplosion(block.getX() + 0.5, block.getY() + 0.1, block.getZ() + 0.5, 2, false, false);
			this.l.lock();
			try
			{
				this.mines.remove(block);
			}
			finally
			{
				this.l.unlock();
			}
		}
	}

	public void update()
	{
		StructureModifier<Float> sm1 = particles1.getFloat();
		StructureModifier<Float> sm2 = particles2.getFloat();
		for (Location l : this.mines)
		{
			sm1.write(0, (float) (l.getBlockX() + 0.5));
			sm1.write(1, (float) (l.getBlockY() + 1));
			sm1.write(2, (float) (l.getBlockZ() + 0.5));
			sm2.write(0, (float) (l.getBlockX() + 0.5));
			sm2.write(1, (float) (l.getBlockY() + 0.1));
			sm2.write(2, (float) (l.getBlockZ() + 0.5));
			Tools.sendPacketToAll(particles1);
			Tools.sendPacketToAll(particles2);
		}
	}
}
*/