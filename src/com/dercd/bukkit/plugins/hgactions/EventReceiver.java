package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.event.Event;

public interface EventReceiver
{
	public void onEvent(Event e);
}
