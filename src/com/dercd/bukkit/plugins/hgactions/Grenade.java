package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

public class Grenade extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = Game.getInstance();
		GameState state = game.getCurrentState();
		
		if(state instanceof CountdownState)
			return ItemAction.CANCEL;
		
		Item i = p.getWorld().dropItem(p.getLocation().clone().add(0, 1.62, 0), new ItemStack(e.getItem().getType()));
		i.setPickupDelay(100000000);
		i.setVelocity(p.getLocation().getDirection().normalize().multiply(2));
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
		{
			Location l = i.getLocation();
			
			boolean worldDamage = state instanceof DMPlayingState;
			i.getWorld().createExplosion(l.getX(), l.getY(), l.getZ(), 2.5F, false, worldDamage);
			i.remove();
		}, 50);
		return ItemAction.REMOVE_CANCEL;
	}
}
