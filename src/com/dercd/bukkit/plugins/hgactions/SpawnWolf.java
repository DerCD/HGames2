/*package com.dercd.bukkit.plugins.hgactions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.Tools.Attribute;
import com.dercd.bukkit.plugins.hgames.HGTeleportEvent;
import com.dercd.bukkit.plugins.hgames.HGames;
import com.dercd.bukkit.plugins.hgames.actions.HGActions.ItemAction;

import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Wolf;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class SpawnWolf extends HGAction
{
	Map<UUID, List<Wolf>> wolves = new HashMap<UUID, List<Wolf>>();
	
	@Override
	public void onEnable()
	{
		this.hga.events.get(PlayerDeathEvent.class).add((e) -> { onDeath((PlayerDeathEvent) e); });
		this.hga.events.get(HGTeleportEvent.class).add((e) -> { onTeleport((HGTeleportEvent) e); });
	}
	
	
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		if (e.getAction() != Action.RIGHT_CLICK_BLOCK) return ItemAction.CANCEL;
		List<Wolf> list;
		if((list = this.wolves.get(p.getUniqueId())) == null)
		{
			list = new ArrayList<Wolf>();
			this.wolves.put(p.getUniqueId(), list);
		}
		list.add(spawn(p, e.getClickedBlock().getRelative(e.getBlockFace()).getLocation()));
		return ItemAction.REMOVE_CANCEL;
	}
	
	private Wolf spawn(Player p, Location l)
	{
		Wolf w = (Wolf) p.getWorld().spawnEntity(Tools.getMiddle(l), EntityType.WOLF);
		w.setOwner(p);
		w.setAngry(true);
		w.setCollarColor(DyeColor.PURPLE);
		Tools.setAttribute(w, 3, Attribute.ATTACK_DAMAGE);
		return w;
	}
	
	public void onDeath(PlayerDeathEvent e)
	{
		Player p = e.getEntity();
		List<Wolf> list = this.wolves.get(p.getUniqueId());
		if(list == null) return;
		Iterator<Wolf> i = list.iterator();
		Wolf w;
		while(i.hasNext())
		{
			w = i.next();
			try
			{
				w.setHealth(0.0);
				w.remove();
			}
			catch (Exception x) {}
			i.remove();
		}
	}
	
	public void onTeleport(HGTeleportEvent e)
	{
		if(!e.isDmTeleport() || e.getRepeat() != 1) return;
		List<Wolf> playerWolves;
		for(Player p : e.getPlayers())
		{
			playerWolves = this.wolves.get(p.getUniqueId());
			if(playerWolves == null) return;
			Location l = p.getLocation();
			for(Wolf w : playerWolves)
				if(!w.isDead())
					spawn(p, l);
		}
	}
}
*/