package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public class Melons extends HGAction
{
	ItemStack i = new ItemStack(Material.MELON);

	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.CANCEL;
		
		Block b = e.getClickedBlock().getRelative(e.getBlockFace());
		ItemStack item = this.i.clone();
		
		//Random number between 3 and 7
		int itemCount = Tools.newPositiveRandomInt() % 5 + 3;
		for (int i = 0; i < itemCount; ++i)
			b.getWorld().dropItemNaturally(b.getLocation(), item.clone());
		
		return ItemAction.REMOVE_CANCEL;
	}
}
