package com.dercd.bukkit.plugins.hgactions;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

public class DoubleHealth extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		double h = p.getHealth() * 2;
		h = Math.min(Tools.getMaxHealth(p), h);
		p.setHealth(h);
		return ItemAction.REMOVE_CANCEL;
	}
}
