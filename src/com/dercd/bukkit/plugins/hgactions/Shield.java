package com.dercd.bukkit.plugins.hgactions;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.BetterJump;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.TextPool;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.reflect.StructureModifier;

public class Shield extends HGAction
{
	Set<Location> prot = new HashSet<Location>();
	private static PacketContainer pc;
	BetterJump bj;
	private Lock protLock = new ReentrantLock();
	static
	{
		pc = new PacketContainer(PacketType.Play.Server.WORLD_EVENT);
		pc.getIntegers().write(0, 2003);
		pc.getIntegers().write(1, 1000);
		pc.getBooleans().write(0, false);
	}

	public Shield()
	{
		Bukkit.getScheduler().runTaskTimerAsynchronously(CDAPI.getInstance(), () ->
		{
			update();
		}, 0, 5);
		this.bj = CDAPI.getInstance().getHandler().getPlugin(BetterJump.class);
	}

	@SuppressWarnings("deprecation")
//	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.ALLOW;
		
		ItemStack i = e.getItem();
		
		final Block b1 = e.getClickedBlock().getRelative(e.getBlockFace());
		final Block b2 = b1.getRelative(BlockFace.UP);
		
		if(b1 == null || b2 == null || b1.getType() != Material.AIR || b2.getType() != Material.AIR)
		{
			p.sendMessage(textPool.getMultiText("hgashildWrongPlace"));
			return ItemAction.CANCEL;
		}
		if(i.getType().isBlock())
		{
			Material mat = i.getType();
			byte data = i.getData().getData();
			
			b1.setType(mat);
			b1.setData(data);
			b2.setData(data);
			b2.setType(mat);
		}
		else
		{
			b1.setType(Material.BREWING_STAND);
			b2.setType(Material.BREWING_STAND);
		}
		final Location l = b2.getLocation();
		Runnable r = new Runnable()
		{
			@Override
			public void run()
			{
				b1.setType(Material.AIR);
				b2.setType(Material.AIR);
				Shield.this.prot.remove(l);
			}
		};
		this.protLock.lock();
		this.prot.add(l);
		this.protLock.unlock();
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), r, 300);
		return ItemAction.REMOVE_CANCEL;
	}

	public void update()
	{
		StructureModifier<Integer> smi;
		this.protLock.lock();
		for (Location l : this.prot)
		{
			smi = pc.getIntegers();
			smi.write(2, l.getBlockX());
			smi.write(3, l.getBlockY());
			smi.write(4, l.getBlockZ());
			Tools.sendPacketToAll(pc);
		}
		this.protLock.unlock();
	}

//	@Override
	public void onMove(PlayerMoveEvent e)
	{
		if (this.prot.isEmpty()) return;
		Player moved = e.getPlayer();
		Location pLoc = moved.getLocation();
		Location l = null;
		double distance;
		this.protLock.lock();
		for (Location loc : this.prot)
			if(pLoc.getWorld() == loc.getWorld())
				if ((distance = pLoc.distance(loc)) > 3 && distance <= 5)
				{
					l = loc;
					break;
				}
		this.protLock.unlock();
		if (l == null) return;
		this.bj.doJump(moved, new Double[] { 0.3, 0.1 }, new Vector(pLoc.getX() - l.getX(), pLoc.getY() - l.getY(), pLoc.getZ() - l.getZ()));
	}

//	@Override
	public void onDamage(EntityDamageEvent e)
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		if(!(e instanceof EntityDamageByEntityEvent))
			return;
		
		EntityDamageByEntityEvent ee = (EntityDamageByEntityEvent) e;
		Player shooter = null;
		Location lShot = null, lShooter = null;
		if(ee.getEntity() instanceof Player)
			lShot = ((Player) ee.getEntity()).getLocation();
		if(ee.getDamager() instanceof Player)
		{
			shooter = (Player) ee.getDamager();
			lShooter = shooter.getLocation();
		}
		else if(ee.getDamager() instanceof Projectile)
		{
			Projectile p = (Projectile) ee.getDamager();
			if(p.getShooter() instanceof Player)
			{
				shooter = (Player) p.getShooter();
				lShooter = shooter.getLocation();
			}
		}
		if(lShooter == null && lShot == null)
			return;
		
		World w;
		if(lShooter != null)
			w = shooter.getWorld();
		else
			w = lShot.getWorld();
		
		for(Location loc : this.prot)
			if(loc.getWorld() == w)
			{
				if(lShot != null && loc.distance(lShot) <= 3)
				{
					e.setCancelled(true);
					return;
				}
				if(lShooter != null && loc.distance(lShooter) <= 3)
				{
					e.setCancelled(true);
					shooter.sendMessage(textPool.getMultiText("hgashildInnerHitForbidden"));
					return;
				}
			}
	}
}
