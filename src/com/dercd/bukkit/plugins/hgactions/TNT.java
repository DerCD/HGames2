package com.dercd.bukkit.plugins.hgactions;

import com.dercd.bukkit.cdapi.CDAPI;
import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;

public class TNT extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		Game game = this.game;
		GameState state = game.getCurrentState();
		
		if(state instanceof CountdownState)
			return ItemAction.CANCEL;
		
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return ItemAction.CANCEL;
		
		Location l = e.getClickedBlock().getRelative(e.getBlockFace()).getLocation().clone().add(0.5, 0.5, 0.5);
		l.getBlock().setType(Material.TNT);
		Bukkit.getScheduler().runTaskLater(CDAPI.getInstance(), () ->
		{
			l.getBlock().setType(Material.AIR);
			Entity tnt = l.getWorld().spawnEntity(l, EntityType.PRIMED_TNT);
			
			boolean worldDamage = state instanceof DMPlayingState;
			if(!worldDamage)
				tnt.setMetadata("playerTnt", new FixedMetadataValue(CDAPI.getInstance(), "CDAPI"));
			((TNTPrimed) tnt).setFuseTicks(40);
		}, 10);
		return ItemAction.REMOVE_CANCEL;
	}
}
