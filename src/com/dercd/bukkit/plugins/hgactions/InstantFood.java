package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public abstract class InstantFood extends HGAction
{
	int foodAmount;
	float saturation;
	
	public InstantFood(int foodAmount, float saturation)
	{
		this.foodAmount = foodAmount;
		this.saturation = saturation;
	}

	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		int hunger = p.getFoodLevel();
		if(hunger >= 20)
			return ItemAction.CANCEL;
		
		p.setFoodLevel(Math.min(hunger += this.foodAmount, 20));
		
		float saturation = p.getSaturation() + this.saturation;
		p.setSaturation(Math.min(saturation, hunger));
		
		return ItemAction.REMOVE_CANCEL;
	}
}
