package com.dercd.bukkit.plugins.hgactions;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.dercd.bukkit.plugins.hgactions.HGActions.ItemAction;

public class Extrahearts extends HGAction
{
	@Override
	public ItemAction run(Player p, PlayerInteractEvent e)
	{
		int amplifier = -1;
		for(PotionEffect pe : p.getActivePotionEffects())
			if(pe.getType().equals(PotionEffectType.ABSORPTION))
			{
				amplifier = pe.getAmplifier();
				break;
			}
		amplifier += 2;
		p.removePotionEffect(PotionEffectType.ABSORPTION);
		p.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, 40000000, amplifier, false), true);
		return ItemAction.REMOVE_CANCEL;
	}
}
