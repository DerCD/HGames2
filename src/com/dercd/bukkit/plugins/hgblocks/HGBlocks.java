package com.dercd.bukkit.plugins.hgblocks;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

import com.dercd.bukkit.cdapi.CDPlugin;
import com.dercd.bukkit.cdapi.PluginHandler;
import com.dercd.bukkit.cdapi.annotations.CDInternPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginCommand;
import com.dercd.bukkit.cdapi.annotations.CDPluginDepend;
import com.dercd.bukkit.cdapi.annotations.CDPluginEvent;
import com.dercd.bukkit.cdapi.events.CDPluginEnableEvent;
import com.dercd.bukkit.cdapi.events.CommandEvent;
import com.dercd.bukkit.cdapi.exceptions.CDCMessage;
import com.dercd.bukkit.cdapi.exceptions.CDInvalidArgsException;
import com.dercd.bukkit.cdapi.exceptions.CDNoPermissionException;
import com.dercd.bukkit.cdapi.exceptions.CDNullSelectionException;
import com.dercd.bukkit.cdapi.exceptions.CDPluginNotFoundException;
import com.dercd.bukkit.cdapi.exceptions.CDSelectionException;
import com.dercd.bukkit.cdapi.exceptions.CDTooManyBlocksException;
import com.dercd.bukkit.cdapi.tools.DynamicClassLoader;
import com.dercd.bukkit.cdapi.tools.Log;
import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.cdapi.tools.collection.SyncedSetMap;
import com.dercd.bukkit.cdapi.tools.minecraft.CDBlock;
import com.dercd.bukkit.plugins.Man.ManEntry;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.HGames2;
import com.dercd.bukkit.plugins.hgames.IOManager;
import com.dercd.bukkit.plugins.hgames.TextPool;

import net.minecraft.server.v1_11_R1.NBTTagCompound;
import net.minecraft.server.v1_11_R1.NBTTagList;

@CDPluginDepend(depends = { }, softdepends = { "WorldEdit" })
@CDInternPluginDepend(depends = { HGames2.class }, softdepends = {})
public class HGBlocks extends CDPlugin
{
	public SyncedSetMap<CDBlock, HGBlock> blocks;
	public Map<Class<? extends HGBlock>, HGBlock> loadedBlocks;
	private DynamicClassLoader<HGBlock> classLoader;
	
	protected Plugin wep;
	private Log clog;
	protected Game game;
	
	protected String nestedMainDirectory;
	
	private static HGBlocks instance;
	
	public HGBlocks(PluginHandler handler)
	{
		super(handler);
		this.clog = handler.getCLog();
		HGBlocks.instance = this;
	}
	
	@Override
	public String getVersion()
	{
		return "0.2";
	}
	
	@Override
	public final String getAuthor()
	{
		return "CD";
	}
	
	@Override
	public ManEntry[] man()
	{
		ManEntry back = new ManEntry("hgb", "HGBlocks");
		back.addEntry("save", "save");
		back.addEntry("load", "load");
		back.addEntry("set", "set <action>");
		back.addEntry("blocks", "blocks");
		back.addEntry("get", "get <action>");
		back.addEntry("remove", "remove <block <action> | block | <action>>");
		return new ManEntry[] { back };
	}
	
	@CDPluginEvent(priority = 9000)
	public void onEnable(CDPluginEnableEvent e) throws CDPluginNotFoundException, IOException
	{
		this.game = Game.getInstance();
		
		HGames2 hg2 = this.game.hgames2;
		this.nestedMainDirectory = Paths.get(hg2.getAbsoluteDirectories()[0], "hgblocks").toString();
		
		loadTexts();
		
		this.clog.log("Try getting WorldEdit", this);
		this.wep = this.getSoftDepend("WorldEdit");
		if(this.wep != null)
			this.clog.log("Success", this);
		else
			this.clog.log("No success", this);
		
		this.clog.log("Loading Items", this);
		loadItems();
		this.clog.log("Success", this);
		
		this.clog.log("Calling enable-method of loaded HGBlocks", this);
		for (HGBlock hgb : this.loadedBlocks.values())
			hgb.onEnable();
		this.clog.log("Done", this);
	}

	@CDPluginCommand(commands = { "hgb cd.hgb2 1" })
	public void onCommand(CommandEvent e) throws CDInvalidArgsException, CDNoPermissionException, CDCMessage, CDPluginNotFoundException, CDSelectionException
	{
		String[] args = e.getArgs();
		e.validateCount(1, 2);
		switch(args[0].toLowerCase())
		{
			case "save":
				e.validateCount(1);
				save(e.getSender());
				return;
			case "load":
				e.validateCount(1);
				load(e.getSender());
				return;
			case "set":
				e.validateCount(2);
				setBlock((Player) e.getSender(), args[1]);
				return;
			case "blocks":
				e.validateCount(1);
				showBlocks(e.getSender());
				return;
			case "get":
				e.validateCount(2);
				procGetCmd(e);
				return;
			case "remove":
				e.validateCount(2, 3);
				removeBlock(e.getSender(), args);
				return;
		}
		throw new CDInvalidArgsException();
	}
	
	@CDPluginEvent
	public void onInteract(PlayerInteractEvent e)
	{
		if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
			return;
		doBlock(e.getPlayer(), e);
	}
	
	
	public HGBlock getHGBlock(String name)
	{
		for(HGBlock act : this.loadedBlocks.values())
			if(act.getClass().getSimpleName().equalsIgnoreCase(name))
				return act;
		return null;
	}
	
	public void procGetCmd(CommandEvent e) throws CDCMessage
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		String name = e.getArgs()[1];
		HGBlock hgb = getHGBlock(name);
		
		if(hgb == null)
			throw new CDCMessage(textPool.getText("hgbNameNotFound"));
		
		if(!this.blocks.containsVal(hgb))
			throw new CDCMessage(textPool.getText("hgbNoBlockFound"));
		
		Player p = (Player) e.getSender();
		for(CDBlock cdb : this.blocks.getByVal(hgb))
		{
			if(p.getInventory().firstEmpty() == -1)
				throw new CDCMessage(textPool.getText("hgbInventoryFull"));
			p.getInventory().addItem(cdb.toBukkitItem());
		}
	}
	
	public boolean hasBlock(String name)
	{
		for(Class<? extends HGBlock> clazz : this.loadedBlocks.keySet())
			if(clazz.getSimpleName().equals(name))
				return true;
		return false;
	}
	
	public void showBlocks(CommandSender sender)
	{
		StringBuilder sb = new StringBuilder();
		for (HGBlock hgb : this.loadedBlocks.values())
		{
			if(sb.length() != 0)
				sb.append(", ");
			sb.append((this.blocks.containsVal(hgb) ? ChatColor.GREEN : ChatColor.RED) + hgb.getClass().getSimpleName() + ChatColor.WHITE);
		}
		sender.sendMessage(Tools.getExclamation(ChatColor.GREEN) + sb.toString());
	}

	@SuppressWarnings("deprecation")
	public void setBlock(Player p, String name) throws CDCMessage, CDNoPermissionException, CDPluginNotFoundException, CDSelectionException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		HGBlock hgb = getHGBlock(name);
		if(hgb == null)
			throw new CDCMessage(textPool.getText("hgbNameNotFound"));
		
		Block b = getSelection(p);
		this.blocks.add(CDBlock.fromMaterial(b.getType(), b.getData()), hgb);
		p.sendMessage(textPool.getMultiText("hgbBlockAdded"));
		save(p);
	}
	
	@SuppressWarnings("deprecation")
	public void removeBlock(CommandSender sender, String[] args) throws CDInvalidArgsException, CDPluginNotFoundException, CDSelectionException, CDCMessage, CDNoPermissionException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		String name = args[1];
		boolean block = name.equals("block");
		if(args.length == 3)
			if(block)
				name = args[2];
			else
				throw new CDInvalidArgsException();
		
		else if(block)
			name = null;
		CDBlock cdb = null;
		if(block)
		{
			Block b = getSelection((Player) sender);
			cdb = CDBlock.fromMaterial(b.getType(), b.getData());
		}
		HGBlock hgb = null;
		if(name != null)
		{
			hgb = getHGBlock(name);
			if(hgb == null)
				throw new CDCMessage(textPool.getText("hgbNameNotFound"));
		}
		
		if(hgb != null)
			if(cdb != null)
				this.blocks.removeByKey(hgb, cdb);
			else
				this.blocks.removeVal(hgb);
		else
			this.blocks.removeKey(cdb);
		
		sender.sendMessage(textPool.getText("hgbBlockRemoved"));
		save(sender);
	}
	
	
	
	private Block getSelection(Player p) throws CDPluginNotFoundException, CDSelectionException
	{
		if(this.wep == null)
			throw new CDPluginNotFoundException("WorldEdit");
		
		Selection s = ((WorldEditPlugin) this.wep).getSelection(p);
		if(s == null)
			throw new CDNullSelectionException();
		if(s.getArea() != 1)
			throw new CDTooManyBlocksException(1);
		
		return s.getMaximumPoint().getBlock();
	}
	
	private void loadClasses()
	{
		this.classLoader = new DynamicClassLoader<HGBlock>(this.clog);
		this.loadedBlocks = new HashMap<Class<? extends HGBlock>, HGBlock>();
		for(HGBlock hgb : this.classLoader.loadDir(
				this.handler
					.getClassLoader()
					.getFileOfPlugin(this.getClass())
					.getParentFile(),
				HGBlock.class,
				new Class<?>[0]
				))
			this.loadedBlocks.put(hgb.getClass(), hgb);
		
		this.clog.log(this.loadedBlocks.size() + " HGBlocks found", this);
	}
	protected void loadTexts() throws IOException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		IOManager ioManager = (IOManager)game.getIOManager();
		
		File textFile = Paths.get(this.nestedMainDirectory, "hgb_lang.json").toFile();
		String textPath = textFile.toString();
		if(!textFile.exists())
			ioManager.extractResource("/hgb_lang.json", textPath);
		
		
		TextPool hgbTexts = ioManager.loadTextPool(textPath);
		textPool.addTexts(hgbTexts);
	}
	
	@SuppressWarnings("deprecation")
	public void doBlock(Player p, PlayerInteractEvent e)
	{
		Block b = e.getClickedBlock();
		CDBlock cdb = CDBlock.fromMaterial(b.getType(), b.getData());
		if(!this.blocks.containsKey(cdb))
			return;
		for(HGBlock hgb : this.blocks.getByKey(cdb))
			e.setCancelled(hgb.run(p, e));
	}

	public void load(CommandSender sender) throws CDNoPermissionException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		if(!sender.hasPermission("cd.hgb2.io"))
			throw new CDNoPermissionException(true, textPool.getText("hgbNoIOPermission"));
		
		try
		{
			loadItems();
			sender.sendMessage(textPool.getMultiText("hgbDataLoaded"));
		}
		catch (Exception x)
		{
			sender.sendMessage(textPool.getMultiText("hgbDataLoadError"));
		}
	}
	public void loadItems() throws IOException
	{
		this.clog.log("Loading classes", this);
		loadClasses();
		
		this.clog.log("Loading blocks", this);
		this.blocks = new SyncedSetMap<CDBlock, HGBlock>();
		
		String blocksPath = Paths.get(this.nestedMainDirectory, "hgblocks.dat").toString();
		NBTTagCompound base = Tools.load(blocksPath, this);
		if(base == null)
			return;
		
		NBTTagList blocks;
		CDBlock cdb;
		String name;
		for (HGBlock hgb : this.loadedBlocks.values())
		{
			name = hgb.getClass().getSimpleName();
			this.clog.log("Loading item '" + name + "'", this);
			if(!base.hasKey(name))
			{
				this.clog.log("Block with name '" + name + "' not found", this);
				continue;
			}
			blocks = (NBTTagList) base.get(name);
			for(int i = 0; i < blocks.size(); ++i)
			{
				this.blocks.add(cdb = CDBlock.fromNBT(blocks.get(i)), hgb);
				hgb.addTrigger(cdb);
			}
		}
	}
	public void save(CommandSender sender) throws CDNoPermissionException
	{
		Game game = this.game;
		TextPool textPool = game.getTextPool();
		
		if(!sender.hasPermission("cd.hgb2.io"))
			throw new CDNoPermissionException(true, textPool.getText("hgbNoIOPermission"));
		try
		{
			saveItems();
			sender.sendMessage(textPool.getMultiText("hgbDataSaved"));
		}
		catch (Exception x)
		{
			sender.sendMessage(textPool.getMultiText("hgbDataSaveError"));
		}
	}
	public void saveItems() throws IOException
	{
		NBTTagCompound base = new NBTTagCompound();
		NBTTagList list;
		Map<HGBlock, Set<CDBlock>> toSave = this.blocks.getMap2();
		for(Entry<HGBlock, Set<CDBlock>> e : toSave.entrySet())
		{
			list = new NBTTagList();
			for(CDBlock cdb : e.getValue())
				list.add(cdb.toNBT());
			base.set(e.getKey().getClass().getSimpleName(), list);
		}
		Tools.save(base, Paths.get(this.nestedMainDirectory, "hgblocks.dat").toString(), this);
	}
	
	
	public static HGBlocks getInstance()
	{
		return HGBlocks.instance;
	}
}
