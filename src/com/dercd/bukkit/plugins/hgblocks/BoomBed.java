package com.dercd.bukkit.plugins.hgblocks;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_11_R1.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.dercd.bukkit.cdapi.tools.Tools;
import com.dercd.bukkit.plugins.hgames.Game;
import com.dercd.bukkit.plugins.hgames.gamestate.CountdownState;
import com.dercd.bukkit.plugins.hgames.gamestate.DMPlayingState;
import com.dercd.bukkit.plugins.hgames.gamestate.GameState;

import net.minecraft.server.v1_11_R1.Blocks;

public class BoomBed extends HGBlock
{
	@SuppressWarnings("deprecation")
	@Override
	public boolean run(Player p, PlayerInteractEvent e)
	{
		Game game = this.game;
		GameState state = game.getCurrentState();
		
		Location l = e.getClickedBlock().getLocation();
		Block b, b2 = null;
		byte tileId = (b = l.getBlock()).getData();
		switch(tileId < 8 ? tileId % 4 : (tileId % 4 + 2) % 4)
		{
			case 0:
				b2 = b.getRelative(BlockFace.SOUTH);
				break;
			case 1:
				b2 = b.getRelative(BlockFace.WEST);
				break;
			case 2:
				b2 = b.getRelative(BlockFace.NORTH);
				break;
			case 3:
				b2 = b.getRelative(BlockFace.EAST);
				break;
		}
		Tools.setBlockWithoutUpdate(l, Blocks.AIR, ((CraftWorld) l.getWorld()).getHandle());
		l = b2.getLocation();
		if(b2.getType() == Material.BED_BLOCK && b2.getData() % 4 == tileId % 4)
			Tools.setBlockWithoutUpdate(l, Blocks.AIR, ((CraftWorld) l.getWorld()).getHandle());
		
		if(Tools.newPositiveRandomInt() % 100 < 25)
		{
			p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 200, 2, true), true);
		}
		else
		{
			boolean worldDamage = state instanceof DMPlayingState && !(state instanceof CountdownState);
			l.getWorld().createExplosion(l.getX(), l.getY(), l.getZ(), 3, true, worldDamage);
		}
		return true;
	}
}
