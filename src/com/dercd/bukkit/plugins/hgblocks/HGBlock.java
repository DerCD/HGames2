package com.dercd.bukkit.plugins.hgblocks;

import java.util.HashSet;
import java.util.Set;

import com.dercd.bukkit.cdapi.tools.minecraft.CDBlock;
import com.dercd.bukkit.plugins.hgames.Game;

import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class HGBlock
{
	Game game;
	HGBlocks hgb;
	Set<CDBlock> triggers = new HashSet<CDBlock>();
	CDBlock trigger;
	
	public HGBlock()
	{
		this.game = Game.getInstance();
		this.hgb = HGBlocks.getInstance();
	}

	public void onEnable()
	{
	}

	public abstract boolean run(Player p, PlayerInteractEvent e);
	
	
	public void addTrigger(CDBlock trigger)
	{
		this.triggers.add(trigger);
	}
	public Set<CDBlock> getTriggers()
	{
		return this.triggers;
	}
	public void clearTriggers()
	{
		this.triggers.clear();
	}
	
}
